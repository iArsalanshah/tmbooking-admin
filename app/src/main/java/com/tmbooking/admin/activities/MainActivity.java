package com.tmbooking.admin.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.tmbooking.admin.R;
import com.tmbooking.admin.fragments.HomeFragment;
import com.tmbooking.admin.fragments.LoginFragment;
import com.tmbooking.admin.fragments.NotificationFragment;
import com.tmbooking.admin.fragments.SideMenuFragment;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.AppConstants;
import com.tmbooking.admin.global.SideMenuChooser;
import com.tmbooking.admin.global.SideMenuDirection;
import com.tmbooking.admin.helpers.ScreenHelper;
import com.tmbooking.admin.helpers.UIHelper;
import com.tmbooking.admin.residemenu.ResideMenu;
import com.tmbooking.admin.retrofit.WebService;
import com.tmbooking.admin.ui.views.TitleBar;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends DockActivity implements OnClickListener,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    public TitleBar titleBar;
    @BindView(R.id.sideMneuFragmentContainer)
    public FrameLayout sideMneuFragmentContainer;
    @BindView(R.id.header_main)
    TitleBar header_main;
    @BindView(R.id.mainFrameLayout)
    FrameLayout mainFrameLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private MainActivity mContext;
    private boolean loading;

    private ResideMenu resideMenu;

    private float lastTranslate = 0.0f;

    private String sideMenuType;
    private String sideMenuDirection;

    //Location Task
    private static final float MIN_DISTANCE = 1;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;

    //Location Broadcast
    private BroadcastReceiver locationBroadCast;
    private boolean isLocationServiceConnected;
    private WebService webService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dock);
        ButterKnife.bind(this);
        titleBar = header_main;
        // setBehindContentView(R.layout.fragment_frame);
        mContext = this;
        Log.i("Screen Density", ScreenHelper.getDensity(this) + "");

        sideMenuType = SideMenuChooser.DRAWER.getValue();
        sideMenuDirection = SideMenuDirection.LEFT.getValue();

        settingSideMenu(sideMenuType, sideMenuDirection);

        titleBar.setMenuButtonListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (sideMenuType.equals(SideMenuChooser.DRAWER.getValue()) && getDrawerLayout() != null) {
                    if (sideMenuDirection.equals(SideMenuDirection.LEFT.getValue())) {
                        drawerLayout.openDrawer(Gravity.LEFT);
                    } else {
                        drawerLayout.openDrawer(Gravity.RIGHT);
                    }
                } else {
                    resideMenu.openMenu(sideMenuDirection);
                }

            }
        });

        titleBar.setBackButtonListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                if (!prefHelper.isInProgress()) {
                if (loading) {
                    UIHelper.showLongToastInCenter(getApplicationContext(),
                            R.string.message_wait);
                } else {
                    popFragment();
                    UIHelper.hideSoftKeyboard(getApplicationContext(),
                            titleBar);
                }
//                }

              /*  if (!prefHelper.isInProgress())
                    if (getSupportFragmentManager().getBackStackEntryCount() > 1)
                        popFragment();
                    else
                        DialogFactory.createQuitDialog(MainActivity.this,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        MainActivity.this.finish();

                                    }
                                }, R.string.message_quit).show();*/
            }
        });

      /*  titleBar.setHomeButtonListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!prefHelper.isInProgress()) {
                    if (loading) {
                        UIHelper.showLongToastInCenter(getApplicationContext(),
                                R.string.message_wait);
                    } else {
                        popBackStackTillEntry(1);
                        UIHelper.hideSoftKeyboard(getApplicationContext(),
                                titleBar);
                    }
                }

//                replaceDockableFragment(HomeFragment.newInstance(), "HomeFragment");
            }
        });

        titleBar.setCancelButtonListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo cancel api will be call
                if (prefHelper.getRide() != null && (
                        Util.getParsedInteger(prefHelper.getRide().getStatus()) == AppConstants.ASSIGNED
                                || Util.getParsedInteger(prefHelper.getRide().getStatus()) == AppConstants.ARRIVED)) {
                    alertDialog("Are you sure you want to cancel this ride?");
//                    prefHelper.setRideStatus(false);
//                    popBackStackTillEntry(0);
//                    replaceDockableFragment(new HomeFragment());
                } else
                    UIHelper.showShortToastInCenter(MainActivity.this, "You can't cancel this ride");
            }
        });
        */

        titleBar.setNotificationButton(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loading) {
                    UIHelper.showLongToastInCenter(getApplicationContext(),
                            R.string.message_wait);
                } else {
                    replaceDockableFragment(new NotificationFragment());
                }

            }
        });


        if (savedInstanceState == null)
            initFragment();

        initLocationServices();
        setLocationBroadCast();
    }

    public View getDrawerView() {
        return getLayoutInflater().inflate(getSideMenuFrameLayoutId(), null);
    }

    private void settingSideMenu(String type, String direction) {

        if (type.equals(SideMenuChooser.DRAWER.getValue())) {


            DrawerLayout.LayoutParams params = new DrawerLayout.LayoutParams((int) getResources().getDimension(R.dimen._260sdp), (int) DrawerLayout.LayoutParams.MATCH_PARENT);


            if (direction.equals(SideMenuDirection.LEFT.getValue())) {
                params.gravity = Gravity.LEFT;
                sideMneuFragmentContainer.setLayoutParams(params);
            } else {
                params.gravity = Gravity.RIGHT;
                sideMneuFragmentContainer.setLayoutParams(params);
            }
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

            sideMenuFragment = SideMenuFragment.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            transaction.replace(getSideMenuFrameLayoutId(), sideMenuFragment).commit();

            drawerLayout.closeDrawers();
        } else {
            resideMenu = new ResideMenu(this);
            resideMenu.attachToActivity(this);
            resideMenu.setMenuListener(getMenuListener());
            resideMenu.setScaleValue(0.52f);

            setMenuItemDirection(direction);
        }
    }

    private void setMenuItemDirection(String direction) {

        if (direction.equals(SideMenuDirection.LEFT.getValue())) {

            SideMenuFragment leftSideMenuFragment = SideMenuFragment.newInstance();
            resideMenu.addMenuItem(leftSideMenuFragment, "LeftSideMenuFragment", direction);

        } else if (direction.equals(SideMenuDirection.RIGHT.getValue())) {

            SideMenuFragment rightSideMenuFragment = SideMenuFragment.newInstance();
            resideMenu.addMenuItem(rightSideMenuFragment, "RightSideMenuFragment", direction);

        }

    }

    private int getSideMenuFrameLayoutId() {
        return R.id.sideMneuFragmentContainer;

    }

    public void initFragment() {

        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
        if (prefHelper.isLogin()) {
            replaceDockableFragment(HomeFragment.newInstance(), "HomeFragment");
        } else {
            replaceDockableFragment(LoginFragment.newInstance(), "LoginFragment");
        }
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {

             /*   if (prefHelper.getRide() != null && (
                        Util.getParsedInteger(prefHelper.getRide().getStatus()) == AppConstants.ARRIVED ||
                                Util.getParsedInteger(prefHelper.getRide().getStatus()) == AppConstants.START_RIDE
                )) {
                    switch (Util.getParsedInteger(prefHelper.getRide().getStatus())) {
                        case AppConstants.ARRIVED:
                            alertDialog("Do you want to cancel this ride?");
                            return;
                        case AppConstants.START_RIDE:
                            alertDialog("Do you want to end this ride?");
                            return;
                    }
                } else {*/
                FragmentManager manager = getSupportFragmentManager();

                if (manager != null) {
                    BaseFragment currFrag = (BaseFragment) manager.findFragmentById(getDockFrameLayoutId());
                    if (currFrag != null) {
                        currFrag.fragmentResume();
                    }
                }
//                }
            }
        };

        return result;
    }

    private void alertDialog(String msg) {
        final NormalDialog dialog = new NormalDialog(mContext);
        dialog.content(msg)//
                .style(NormalDialog.STYLE_TWO)//
                .titleTextSize(23)//
                .title("Alert")
//                .showAnim(mBasIn)//
//                .dismissAnim(mBasOut)//
                .show();
        dialog.btnText("Yes", "No");

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
//                        T.showShort(mContext, "left");
//                        UIHelper.showShortToastInCenter(MainActivity.this, "Will be implemented in beta!");
                        dialog.dismiss();
//                        ride after cancel
                        callCancelService();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
//                        T.showShort(mContext, "right");
                        dialog.dismiss();
                    }
                });

    }

    private void callCancelService() {
/*        ActiveRideDetailsFragment fragment = (ActiveRideDetailsFragment)
                getSupportFragmentManager().findFragmentByTag(ActiveRideDetailsFragment.class.getSimpleName());
        fragment.updateStatusService(Util.getParsedInteger(prefHelper.getRide().getId()),AppConstants.CANCEL_BY_DRIVER);

        this.onLoadingStarted();

        webService = WebServiceFactory.getWebServiceInstanceWithCustomInterceptor(MainActivity.this, WebServiceConstants.BASE_URL);
        webService.updateridestatus(Util.getParsedInteger(prefHelper.getRide().getId()),AppConstants.CANCEL_BY_DRIVER);*/
//
//        Fragment frag = getSupportFragmentManager().findFragmentById(getDockFrameLayoutId());
//        if (frag instanceof ActiveRideDetailsFragment) {
//            ActiveRideDetailsFragment activeRideDetailsFragment = (ActiveRideDetailsFragment) frag;
//            activeRideDetailsFragment.updateStatusService(Util.getParsedInteger(prefHelper.getRide().getId()),
//                    AppConstants.CANCEL_BY_DRIVER);
//        }

    }

    @Override
    public void onLoadingStarted() {

        if (mainFrameLayout != null) {
            mainFrameLayout.setVisibility(View.VISIBLE);
            if (progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
            }
            loading = true;
        }
    }

    @Override
    public void onLoadingFinished() {
        mainFrameLayout.setVisibility(View.VISIBLE);

        if (progressBar != null) {
            progressBar.setVisibility(View.INVISIBLE);
        }
        loading = false;

    }

    @Override
    public void onProgressUpdated(int percentLoaded) {

    }

    @Override
    public int getDockFrameLayoutId() {
        return R.id.mainFrameLayout;
    }

    @Override
    public void onMenuItemActionCalled(int actionId, String data) {

    }

    @Override
    public void setSubHeading(String subHeadText) {

    }

    @Override
    public boolean isLoggedIn() {
        return false;
    }

    @Override
    public void hideHeaderButtons(boolean leftBtn, boolean rightBtn) {
    }

    @Override
    public void onBackPressed() {
        if (loading) {
            UIHelper.showLongToastInCenter(getApplicationContext(),
                    R.string.message_wait);
        } else
            super.onBackPressed();

    }

    @Override
    public void onClick(View view) {

    }

    private void notImplemented() {
        UIHelper.showLongToastInCenter(this, "Coming Soon");
    }

    //Get locations

    private void initLocationServices() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        locationRequest = new LocationRequest();
        locationRequest.setFastestInterval(TimeUnit.SECONDS.toMillis(1));
        locationRequest.setInterval(TimeUnit.SECONDS.toMillis(1));
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(MIN_DISTANCE);
        locationRequest.setMaxWaitTime(TimeUnit.SECONDS.toMillis(1));
    }


    private void locationListeners() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "locationListeners: *** Permissions Not Given ***");
            return;
        }
        if (mGoogleApiClient != null && locationRequest != null && mGoogleApiClient.isConnected()
                && !isLocationServiceConnected) {
            isLocationServiceConnected = true;
            Log.e(TAG, "locationListeners: *** LocationServices INIT ***");
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "onLocationChanged: \nLatLng: " + location.getLatitude() + "," + location.getLongitude());
        Intent intent = new Intent();
        intent.setAction(AppConstants.RECEIVE_LOCATION_BROADCAST);
        intent.putExtra(AppConstants.LAT, location.getLatitude());
        intent.putExtra(AppConstants.LNG, location.getLongitude());
        MainActivity.this.sendBroadcast(intent);
    }

    private void setLocationBroadCast() {
        locationBroadCast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "locationListeners: *** Location Broadcast Call ***");
                locationListeners();
            }
        };
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected: ");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            this.registerReceiver(locationBroadCast, new IntentFilter(AppConstants.SEND_LOCATION_BROADCAST));
        } catch (Exception ex) {
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isLocationServiceConnected = false;
        stopLocationUpdates();
        try {
            this.unregisterReceiver(locationBroadCast);
        } catch (Exception ex) {
        }
    }

}
