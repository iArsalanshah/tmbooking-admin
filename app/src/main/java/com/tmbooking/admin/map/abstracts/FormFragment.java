package com.tmbooking.admin.map.abstracts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;
import com.tmbooking.admin.entities.DriverEnt;
import com.tmbooking.admin.fragments.BookingListFragment;
import com.tmbooking.admin.fragments.DriverDetailFormFragment;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.AppConstants;
import com.tmbooking.admin.helpers.TextViewHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FormFragment extends BaseFragment {


    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvCar)
    TextView tvCar;
    @BindView(R.id.tvLicenseNumber)
    TextView tvLicenseNumber;
    @BindView(R.id.tvEmergencyContact)
    TextView tvEmergencyContact;
    Unbinder unbinder;

    DriverEnt driverEnt;

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.map_popup, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

      /*  final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Well I don't have any...", Toast.LENGTH_SHORT).show();
            }
        };*/


        Button btnShowProfile = view.findViewById(R.id.btnShowProfile);
        Button btnAssignBooking = view.findViewById(R.id.btnAssignBooking);

        btnShowProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DriverDetailFormFragment fragment = new DriverDetailFormFragment();
                fragment.setObjectData(driverEnt);
//                ((DockActivity) getContext()).replaceDockableFragment(fragment, DriverDetailFormFragment.class.getSimpleName());
                ((DockActivity) getContext()).addDockableFragment(fragment, DriverDetailFormFragment.class.getSimpleName());

            }
        });
        btnAssignBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookingListFragment fragment0 = new BookingListFragment();
                fragment0.whichFragment(AppConstants.PENDING_TEXT);
//                ((DockActivity) getContext()).replaceDockableFragment(fragment0, BookingListFragment.class.getSimpleName());
                ((DockActivity) getContext()).addDockableFragment(fragment0, BookingListFragment.class.getSimpleName());

            }
        });

        if (driverEnt != null) {
            TextViewHelper.setText(tvName, driverEnt.getName());
            TextViewHelper.setText(tvCar, driverEnt.getAssignedCar());
            TextViewHelper.setText(tvLicenseNumber, driverEnt.getLicenceNo());
            TextViewHelper.setText(tvEmergencyContact, driverEnt.getEmergencyContact());
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public DriverEnt getDriverEnt() {
        return driverEnt;
    }

    public void setDriverEnt(DriverEnt driverEnt) {
        this.driverEnt = driverEnt;
    }
}