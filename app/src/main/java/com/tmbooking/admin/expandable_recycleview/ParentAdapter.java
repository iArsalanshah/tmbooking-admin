package com.tmbooking.admin.expandable_recycleview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;

import java.util.List;

public class ParentAdapter extends ExpandableRecyclerViewAdapter<ParentViewHolder, SideMenuChildViewHolder> {
    DockActivity dockActivity;
    List<Parent> mList;

    public ParentAdapter(DockActivity dockActivity, List<? extends ExpandableGroup> groups) {
        super(groups);
        mList = (List<Parent>) groups;
        this.dockActivity = dockActivity;
    }

    @Override
    public ParentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sidemenu, parent, false);
        return new ParentViewHolder(view);
    }

    @Override
    public SideMenuChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sidemenu, parent, false);
        return new SideMenuChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(SideMenuChildViewHolder holder, final int flatPosition, ExpandableGroup group,
                                      final int childIndex) {
        final SideMenuChild menuChild = ((Parent) group).getItems().get(childIndex);
        holder.setChildName(menuChild.getName());
        final int mPos = holder.getAdapterPosition();
        final String parentName = ((Parent) group).getTitle();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*previous work list*/
               /* switch (parentName) {
                    case "Drivers":
                        switch (childIndex) {
                            case 0:
                                dockActivity.replaceDockableFragment(new DriverListFragment());
                                break;
                            case 1:
                                dockActivity.replaceDockableFragment(new DriverMapFragment());
                                break;
                            case 2:
                                DriverDetailFormFragment fragment = new DriverDetailFormFragment();
                                fragment.setIsRegistration(true);
                                dockActivity.replaceDockableFragment(fragment);
                                break;

                        }
                        break;
                    case "Bookings":
                        switch (childIndex) {
                            case 0:
                                BookingListFragment fragment0 = new BookingListFragment();
                                fragment0.whichFragment(AppConstants.PENDING_TEXT);
                                dockActivity.replaceDockableFragment(fragment0);
                                break;
                            case 1:
                                BookingListFragment fragment1 = new BookingListFragment();
                                fragment1.whichFragment(AppConstants.ACTIVE_TEXT);
                                dockActivity.replaceDockableFragment(fragment1);
                                break;
                            case 2:
                                BookingListFragment fragment2 = new BookingListFragment();
                                fragment2.whichFragment(AppConstants.CANCEL_TEXT);
                                dockActivity.replaceDockableFragment(fragment2);
                                break;
                            case 3:
                                BookingListFragment fragment3 = new BookingListFragment();
                                fragment3.whichFragment(AppConstants.HISTORY_TEXT);
                                dockActivity.replaceDockableFragment(fragment3);
                                break;
                        }
                        break;
                }*/
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(ParentViewHolder holder, final int flatPosition,
                                      ExpandableGroup group) {
        holder.setTitle(group);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mList.get(flatPosition).getMfragment() != null)
                    dockActivity.replaceDockableFragment(mList.get(flatPosition).getMfragment());
                if (mList.get(flatPosition).getDialog() != null)
                    mList.get(flatPosition).getDialog().show();
            }
        });

/* previous list work
        if (flatPosition == 0 || flatPosition == 1 || flatPosition == 4) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (flatPosition) {
                        case 0:
                            VisaTypesFragment fragment = new VisaTypesFragment();
                            fragment.setVisaTypes(true);
                            dockActivity.replaceDockableFragment(fragment);
                            break;
                        case 1:
                            VisaTypesFragment fragment2 = new VisaTypesFragment();
                            fragment2.setVisaTypes(false);
                            dockActivity.replaceDockableFragment(fragment2);
                            break;
                        case 4:
                            BookingsCalendarFragment fragment4 = new BookingsCalendarFragment();
                            dockActivity.replaceDockableFragment(fragment4);
                            break;
                    }
                }
            });
        }*/
    }

/*    private NormalDialog showLogoutDialog() {
        final NormalDialog dialog = new NormalDialog(dockActivity);
        dialog.title(dockActivity.getString(R.string.logout))
                .style(NormalDialog.STYLE_TWO)
                .contentGravity(Gravity.CENTER)
                .content(dockActivity.getString(R.string.logout_msg))
                .btnText(dockActivity.getString(R.string.yes),
                        dockActivity.getString(R.string.no))
                .showAnim(new BounceEnter());
//                .show();

        dialog.setOnBtnClickL(new OnBtnClickL() {
            @Override
            public void onBtnClick() {
                dialog.dismiss();
                //on Logout click
                logout();
            }
        }, new OnBtnClickL() {
            @Override
            public void onBtnClick() {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    private void logout() {
        dockActivity.prefHelper.setLoginStatus(false);
        dockActivity.prefHelper.setLoginData(null);
        dockActivity.popBackStackTillEntry(0);
        dockActivity.replaceDockableFragment(LoginFragment.newInstance(),
                LoginFragment.class.getSimpleName());
    }*/
}