package com.tmbooking.admin.expandable_recycleview;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.tmbooking.admin.R;

public class SideMenuChildViewHolder extends ChildViewHolder {

  private TextView tvName;

  public SideMenuChildViewHolder(View itemView) {
    super(itemView);
    tvName = itemView.findViewById(R.id.tvSidemenu);
  }

  public void setChildName(String name) {
    tvName.setText(name);
  }
}