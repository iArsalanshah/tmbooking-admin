package com.tmbooking.admin.expandable_recycleview;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.tmbooking.admin.R;

public class ParentViewHolder extends GroupViewHolder {

  private TextView tvParentTitle;

  public ParentViewHolder(View itemView) {
    super(itemView);
    tvParentTitle = itemView.findViewById(R.id.tvSidemenu);
  }

  public void setTitle(ExpandableGroup group) {
    tvParentTitle.setText(group.getTitle());
  }


}