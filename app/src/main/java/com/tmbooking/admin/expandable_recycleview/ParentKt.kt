package com.tmbooking.admin.expandable_recycleview

import android.os.Parcel
import android.os.Parcelable
import android.support.v4.app.Fragment
import com.flyco.dialog.widget.NormalDialog
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.tmbooking.admin.fragments.abstracts.BaseFragment

class Parent : ExpandableGroup<SideMenuChild> {
    internal var title: String
    internal var items: List<SideMenuChild>?
    val mfragment: BaseFragment?
    val dialog: NormalDialog?

    constructor(title: String, items: List<SideMenuChild>?, mfragment: BaseFragment?, dialog: NormalDialog?) : super(title, items) {
        this.title = title
        this.items = items
        this.mfragment = mfragment
        this.dialog = dialog
    }

}

