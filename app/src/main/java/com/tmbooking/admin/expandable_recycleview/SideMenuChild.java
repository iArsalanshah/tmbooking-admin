package com.tmbooking.admin.expandable_recycleview;

import android.os.Parcel;
import android.os.Parcelable;

public class SideMenuChild implements Parcelable{
    protected SideMenuChild(Parcel in) {
        name = in.readString();
    }

    public static final Creator<SideMenuChild> CREATOR = new Creator<SideMenuChild>() {
        @Override
        public SideMenuChild createFromParcel(Parcel in) {
            return new SideMenuChild(in);
        }

        @Override
        public SideMenuChild[] newArray(int size) {
            return new SideMenuChild[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }

    public SideMenuChild(String name) {
        this.name = name;
    }
}
