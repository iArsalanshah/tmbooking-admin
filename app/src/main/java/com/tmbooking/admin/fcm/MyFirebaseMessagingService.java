package com.tmbooking.admin.fcm;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        NotificationHelper helper = new NotificationHelper(this);

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> msgData = remoteMessage.getData();
            String msg = msgData.get("message");
            String title = msgData.get("title");
            Log.e(TAG, "onMessageReceived: msg->" + msg + " title->" + title);
            helper.createNotification(msg);
        } else if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            String msg = remoteMessage.getNotification().getBody();
            String title = remoteMessage.getNotification().getTitle();
            helper.createNotification(msg);
        }//Check if message contains a notification payload.
    }

}