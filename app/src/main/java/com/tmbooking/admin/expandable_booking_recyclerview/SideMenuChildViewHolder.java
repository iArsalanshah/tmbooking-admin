package com.tmbooking.admin.expandable_booking_recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;
import com.tmbooking.admin.entities.ChildBookingItems;
import com.tmbooking.admin.global.AppConstants;
import com.tmbooking.admin.helpers.TextViewHelper;

class SideMenuChildViewHolder extends ChildViewHolder {

    private TextView tvTime, tvName, tvStatus;
    private ImageView img;
    private DockActivity dockActivity;

    SideMenuChildViewHolder(View itemView, DockActivity dockActivity) {
        super(itemView);
        tvTime = itemView.findViewById(R.id.tvTime);
        img = itemView.findViewById(R.id.imageView);
        tvName = itemView.findViewById(R.id.tvName);
        tvStatus = itemView.findViewById(R.id.tvStatus);
        this.dockActivity = dockActivity;
    }
/*

    public void setChildData(String time, Drawable image, String name) {
        TextViewHelper.setText(tvTime, time);
        TextViewHelper.setText(tvName, name);

        img.setImageDrawable(image);

    }
*/

    void setChildData(ChildBookingItems childData) {
        if (childData.getType() == AppConstants.HOTEL) {
            TextViewHelper.setText(tvTime, childData.getHotel().getTime());
            TextViewHelper.setText(tvName, childData.getHotel().getName());
            TextViewHelper.setText(tvStatus, childData.getHotel().getStatus());
            img.setImageDrawable(dockActivity.getResources().getDrawable(R.drawable.ic_hotel));
        } else if (childData.getType() == AppConstants.DELIVERY) {
            TextViewHelper.setText(tvTime, childData.getDelivery().getTime());
            TextViewHelper.setText(tvName, childData.getDelivery().getName());
            TextViewHelper.setText(tvStatus, childData.getDelivery().getStatus());
            img.setImageDrawable(dockActivity.getResources().getDrawable(R.drawable.ic_gift));
        } else if (childData.getType() == AppConstants.TRANSPORT) {
            TextViewHelper.setText(tvTime, childData.getTransport().getTime());
            TextViewHelper.setText(tvName, childData.getTransport().getName());
            TextViewHelper.setText(tvStatus, childData.getTransport().getStatus());
            img.setImageDrawable(dockActivity.getResources().getDrawable(R.drawable.ic_car));
        }

    }
}