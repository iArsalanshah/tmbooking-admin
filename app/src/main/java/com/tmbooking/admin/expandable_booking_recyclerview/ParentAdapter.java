package com.tmbooking.admin.expandable_booking_recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;
import com.tmbooking.admin.entities.ChildBookingItems;
import com.tmbooking.admin.fragments.phase_4.EditBookingFragment;
import com.tmbooking.admin.fragments.phase_4.GiftsFragment;
import com.tmbooking.admin.global.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class ParentAdapter extends ExpandableRecyclerViewAdapter<ParentViewHolder, SideMenuChildViewHolder> {
    private DockActivity dockActivity;
//    private ArrayList<Integer> parentPosList = new ArrayList<>();

    public ParentAdapter(DockActivity dockActivity, List<? extends ExpandableGroup> groups) {
        super(groups);
        this.dockActivity = dockActivity;
    }

    @Override
    public ParentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_phase4, parent, false);
        return new ParentViewHolder(view);
    }

    @Override
    public SideMenuChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child_list_bookings, parent, false);
        return new SideMenuChildViewHolder(view, dockActivity);
    }

    @Override
    public void onBindChildViewHolder(SideMenuChildViewHolder holder, final int flatPosition, ExpandableGroup group,
                                      final int childIndex) {
        final ChildBookingItems menuChild = ((Parent) group).getItems().get(childIndex);
        holder.setChildData(menuChild);
        final int mPos = holder.getAdapterPosition();
        final String childType = menuChild.getType();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (childType) {
                    case AppConstants.DELIVERY:
                        GiftsFragment mfragment0 = new GiftsFragment();
                        mfragment0.deliveryObj = menuChild.getDelivery();
                        dockActivity.replaceDockableFragment(mfragment0);
                        break;
                    case AppConstants.TRANSPORT:
                        EditBookingFragment fragment = new EditBookingFragment();
                        fragment.setHotelBooking(false);
                        fragment.transportObj = menuChild.getTransport();
                        dockActivity.replaceDockableFragment(fragment);
                        break;
                    case AppConstants.HOTEL:
                        EditBookingFragment fragment2 = new EditBookingFragment();
                        fragment2.setHotelBooking(true);
                        fragment2.setHotelObj(menuChild.getHotel());
                        dockActivity.replaceDockableFragment(fragment2);
                        break;
                }
            }
        });
    }

    @Override
    public void onBindGroupViewHolder(ParentViewHolder holder, final int flatPosition,
                                      ExpandableGroup group) {
        holder.setTitle(group);
//        parentPosList.add(flatPosition);
    }

}