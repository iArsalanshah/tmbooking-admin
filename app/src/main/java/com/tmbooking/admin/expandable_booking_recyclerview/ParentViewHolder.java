package com.tmbooking.admin.expandable_booking_recyclerview;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.tmbooking.admin.R;

public class ParentViewHolder extends GroupViewHolder {

    private TextView tvDate;

    ParentViewHolder(View itemView) {
        super(itemView);
        tvDate = itemView.findViewById(R.id.tvDate);
    }

    public void setTitle(ExpandableGroup group) {
        tvDate.setText(group.getTitle());
    }
}