package com.tmbooking.admin.expandable_booking_recyclerview;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.tmbooking.admin.entities.ChildBookingItems;
import com.tmbooking.admin.entities.phase4.Hotel;

public class SideMenuChild implements Parcelable {
    //    private String time;
    private Drawable image;
//    private String name;
//    private String type;

    private ChildBookingItems childBookingItems;

    public SideMenuChild(Drawable image, ChildBookingItems childBookingItems) {
//        this.time = time;
        this.image = image;
//        this.name = name;
//        this.type = type;
        this.childBookingItems = childBookingItems;
    }

    protected SideMenuChild(Parcel in) {
//        time = in.readString();
//        name = in.readString();
//        type = in.readString();
    }

    public static final Creator<SideMenuChild> CREATOR = new Creator<SideMenuChild>() {
        @Override
        public SideMenuChild createFromParcel(Parcel in) {
            return new SideMenuChild(in);
        }

        @Override
        public SideMenuChild[] newArray(int size) {
            return new SideMenuChild[size];
        }
    };

    /*  protected SideMenuChild(Parcel in) {
            time = in.readString();
            image = in.readString();
            name = in.readString();
            type = in.readString();
        }

        public static final Creator<SideMenuChild> CREATOR = new Creator<SideMenuChild>() {
            @Override
            public SideMenuChild createFromParcel(Parcel in) {
                return new SideMenuChild(in);
            }

            @Override
            public SideMenuChild[] newArray(int size) {
                return new SideMenuChild[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(time);
            dest.writeString(image);
            dest.writeString(name);
            dest.writeString(type);
        }
    */
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }


    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

 /*   public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }*/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(time);
//        dest.writeString(name);
//        dest.writeString(type);
    }


    /*@Override
    public int compareTo(@NonNull SideMenuChild comparechild) {

        int compareage=((SideMenuChild)comparechild).getStudentage();
        *//* For Ascending order*//*
        return this.studentage-comparechild;
    }*/
}
