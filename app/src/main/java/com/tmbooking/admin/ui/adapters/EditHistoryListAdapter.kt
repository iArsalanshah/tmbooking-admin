package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.EditHistory
import com.tmbooking.admin.entities.phase4.GuestDetails
import com.tmbooking.admin.fragments.phase_4.EditHistoryFragment
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class EditHistoryListAdapter(context: Context) : RecyclerViewListAdapter<EditHistory>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_edit_history, viewGroup, false)
    }

    override fun bindView(item: EditHistory?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val tvChanges = viewHolder.getView(R.id.tvChanges) as TextView
            val tvTime = viewHolder.getView(R.id.tvTime) as TextView
            val tvName = viewHolder.getView(R.id.tvName) as TextView

            TextViewHelper.setText(tvChanges, "Old value : " + item.old_value +
                    "\nNew value : " + item.new_value)
            TextViewHelper.setText(tvTime, item.date_time)
            TextViewHelper.setText(tvName, "Updated by : "+item.user)
        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}