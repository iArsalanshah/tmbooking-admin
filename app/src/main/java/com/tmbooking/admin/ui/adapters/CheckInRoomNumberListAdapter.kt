package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter
import com.tmbooking.admin.ui.views.AnyEditTextView

class CheckInRoomNumberListAdapter(context: Context) : RecyclerViewListAdapter<String>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_check_in_rooms, viewGroup, false)
    }

    override fun bindView(item: String?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val tvRoomTitle = viewHolder.getView(R.id.tvRoomTitle) as TextView
            val etRoomNumber = viewHolder.getView(R.id.etRoomNumber) as AnyEditTextView

            TextViewHelper.setText(tvRoomTitle, item)

        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}