package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.DriverLogList
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class DriverLogListAdapter(context: Context) : RecyclerViewListAdapter<DriverLogList>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_driver_log_list, viewGroup, false)
    }

    override fun bindView(item: DriverLogList?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val tvName = viewHolder.getView(R.id.tvName) as TextView
            val tvDetails = viewHolder.getView(R.id.tvDetails) as TextView

            TextViewHelper.setText(tvName, item.name)
            TextViewHelper.setText(tvDetails, "Status: ${item.name}\nUpdated at:${item.updated_at}")


        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}