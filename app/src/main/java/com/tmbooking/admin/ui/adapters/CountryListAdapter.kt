package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.entities.phase4.CountryListEnt
import com.tmbooking.admin.fragments.phase_4.visa_management.CountryListFragment
import com.tmbooking.admin.fragments.phase_4.visa_management.CountryVisaFragment
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class CountryListAdapter(context: Context, var instance: CountryListFragment) : RecyclerViewListAdapter<CountryListEnt>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_country, viewGroup, false)
    }

    override fun bindView(item: CountryListEnt?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val tvCountryName = viewHolder.getView(R.id.tvCountryName) as TextView

            val tvEdit = viewHolder.getView(R.id.tvEdit) as TextView
            val tvDlt = viewHolder.getView(R.id.tvDlt) as TextView

            TextViewHelper.setText(tvCountryName, item.cname)

            tvCountryName.setOnClickListener() {
                var fragment = CountryVisaFragment()
                fragment.countryObj = item
                (context as DockActivity).replaceDockableFragment(fragment)
            }

            tvDlt.setOnClickListener() {
                instance.onSwipeDelete(viewHolder.adapterPosition)
            }
            tvEdit.setOnClickListener() {
                instance.onSwipeEdit(item)
            }

        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}