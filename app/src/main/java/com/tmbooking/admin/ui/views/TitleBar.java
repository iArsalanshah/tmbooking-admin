package com.tmbooking.admin.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tmbooking.admin.R;

public class TitleBar extends RelativeLayout {

    private TextView txtTitle;
    private TextView btnRight;
    private ImageView btnLeft;
    private ImageView btnNotification;
    private ImageView btnAdd;

    private View.OnClickListener menuButtonListener;
    private OnClickListener backButtonListener;
    private OnClickListener notificationButtonListener;
    private OnClickListener addButtonListener; // add and filter button
    private OnClickListener cancelButtonListener;

    private Context context;


    public TitleBar(Context context) {
        super(context);
        this.context = context;
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    private void initAttrs(Context context, AttributeSet attrs) {
    }

    private void bindViews() {
        txtTitle = this.findViewById(R.id.txt_subHead);
        btnRight = this.findViewById(R.id.btnRight);
        btnLeft = this.findViewById(R.id.btnLeft);
        btnNotification = this.findViewById(R.id.btnNotification);
        btnAdd = this.findViewById(R.id.btnAdd);
    }

    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.header_main, this);
        bindViews();
    }

    public void hideButtons() {
        btnLeft.setVisibility(View.INVISIBLE);
        btnRight.setVisibility(View.INVISIBLE);
        btnNotification.setVisibility(View.INVISIBLE);
        btnAdd.setVisibility(View.INVISIBLE);
    }

    public void showBackButton() {
        btnLeft.setImageResource(R.drawable.ic_arrow_back_24_white);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(backButtonListener);
    }

    public void showNotificationButton() {
        btnNotification.setVisibility(View.VISIBLE);
        btnNotification.setOnClickListener(notificationButtonListener);
    }

    public void showAddButton() {
        btnAdd.setVisibility(View.VISIBLE);
        btnAdd.setImageResource(R.drawable.ic_add_24dp);
        btnAdd.setOnClickListener(addButtonListener);
    }

    public void showFilterButton() {
        btnAdd.setVisibility(View.VISIBLE);
        btnAdd.setImageResource(R.drawable.ic_filter);
        btnAdd.setOnClickListener(addButtonListener);
    }

    public void showRideCancelButton() {
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setOnClickListener(cancelButtonListener);
    }

    public void showMenuButton() {
        btnLeft.setImageResource(R.drawable.ic_menu);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(menuButtonListener);
    }

    public void setSubHeading(String heading) {
        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(heading);
    }

    public String getSubHeading() {
        return txtTitle.getText().toString();
    }

    public void setRightBtnHeading(String heading) {
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setText(heading);
    }

    public void setRightBtnListener(OnClickListener rightBtnListener) {
        btnRight.setOnClickListener(rightBtnListener);
    }

    public String getRightButtonText() {
        return btnRight.getText().toString();
    }

    public void showTitleBar() {
        this.setVisibility(View.VISIBLE);
    }

    public void hideTitleBar() {
        this.setVisibility(View.GONE);
    }

    public void setMenuButtonListener(View.OnClickListener listener) {
        menuButtonListener = listener;
    }

    public void setBackButtonListener(View.OnClickListener listener) {
        backButtonListener = listener;
    }

    public void setNotificationButton(View.OnClickListener listener) {
        notificationButtonListener = listener;
    }

    public void setAddButton(View.OnClickListener listener) {
        addButtonListener = listener;
    }

}
