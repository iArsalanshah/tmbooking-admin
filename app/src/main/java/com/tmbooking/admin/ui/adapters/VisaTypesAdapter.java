package com.tmbooking.admin.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;
import com.tmbooking.admin.entities.VisaApplicationEnt;
import com.tmbooking.admin.entities.VisaTypesEnt;
import com.tmbooking.admin.fragments.VisaApplicationFormFragment;
import com.tmbooking.admin.fragments.phase_4.visa_management.VisaTypeForm;
import com.tmbooking.admin.global.AppConstants;
import com.tmbooking.admin.helpers.DateHelper;
import com.tmbooking.admin.helpers.TextViewHelper;
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.tmbooking.admin.ui.views.Util;

//Only for use case of Generic @RecyclerViewListAdapter
public class VisaTypesAdapter extends RecyclerViewListAdapter<Object> {
    private Context context;
    private String[] visaType = {"Title:", "Visa Type:", "Price:"};
    private String[] visaApplication = {"Full Name:", "Date:", "Status:"};
    private String[] visaConstantList = {"Pending", "Approved", "Rejected"};

    private boolean isVisaType, isMember;

    public VisaTypesAdapter(Context context, boolean isVisaType, boolean isMember) {
        super(context);
        this.context = context;
        this.isVisaType = isVisaType;
        this.isMember = isMember;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_visa_type, viewGroup, false);
    }

    @Override
    protected void bindView(final Object item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            final int position = viewHolder.getAdapterPosition();
            TextView tvTitleText = (TextView) viewHolder.getView(R.id.tvTitleText);
            TextView tvVisaTypeText = (TextView) viewHolder.getView(R.id.tvVisaTypeText);
            TextView tvPriceText = (TextView) viewHolder.getView(R.id.tvPriceText);

            TextView tvTitle = (TextView) viewHolder.getView(R.id.tvTitle);
            TextView tvVisaType = (TextView) viewHolder.getView(R.id.tvVisaType);
            TextView tvPrice = (TextView) viewHolder.getView(R.id.tvPrice);

            if (isVisaType) {
                VisaTypesEnt obj = (VisaTypesEnt) item;
                TextViewHelper.setText(tvTitleText, visaType[0]);
                TextViewHelper.setText(tvVisaTypeText, visaType[1]);
                TextViewHelper.setText(tvPriceText, visaType[2]);

                TextViewHelper.setText(tvTitle, obj.getTitle());
                TextViewHelper.setText(tvVisaType, obj.getVisatype());
                TextViewHelper.setText(tvPrice, obj.getPrice());
            } else {
                VisaApplicationEnt visaAppObj = (VisaApplicationEnt) item;
                TextViewHelper.setText(tvTitleText, visaApplication[0]);
                TextViewHelper.setText(tvVisaTypeText, visaApplication[1]);
                TextViewHelper.setText(tvPriceText, visaApplication[2]);

                TextViewHelper.setText(tvTitle, visaAppObj.getFullname());
                if (isMember) /*For member*/
                    TextViewHelper.setText(tvVisaType,
                            DateHelper.getlocalDate(visaAppObj.getDatetime(), DateHelper.DATE_FORMAT2, DateHelper.DATE_TIME_FORMAT));

                TextViewHelper.setText(tvVisaType, visaAppObj.getDate());


                if (AppConstants.PENDING == Util.getParsedInteger(visaAppObj.getStatus()))
                    TextViewHelper.setText(tvPrice, visaConstantList[0]);
                else if (AppConstants.APPROVED == Util.getParsedInteger(visaAppObj.getStatus()))
                    TextViewHelper.setText(tvPrice, visaConstantList[1]);
                else if (AppConstants.REJECTED == Util.getParsedInteger(visaAppObj.getStatus()))
                    TextViewHelper.setText(tvPrice, visaConstantList[2]);
            }

            viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isVisaType) {
                        VisaTypeForm fragment = new VisaTypeForm();
//                        fragment.setmVisaTypeObj((VisaTypesEnt) item); /*updated*/

                        ((DockActivity) context).replaceDockableFragment(fragment
                                , VisaTypeForm.class.getSimpleName());
                    } else {
                        VisaApplicationFormFragment fragment = new VisaApplicationFormFragment();
                        fragment.setObjectData((VisaApplicationEnt) item);

                        if (isMember) /* only when member */
                            fragment.setMember(true);

                        ((DockActivity) context).replaceDockableFragment(fragment
                                , VisaApplicationFormFragment.class.getSimpleName());
                    }
                }
            });
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
