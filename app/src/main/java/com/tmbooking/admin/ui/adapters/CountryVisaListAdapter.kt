package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.entities.phase4.CountryVisaTypeEnt
import com.tmbooking.admin.fragments.phase_4.visa_management.CountryVisaFragment
import com.tmbooking.admin.fragments.phase_4.visa_management.VisaTypeForm
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class CountryVisaListAdapter(context: Context, var instance: CountryVisaFragment) : RecyclerViewListAdapter<CountryVisaTypeEnt>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_country_visa_name, viewGroup, false)
    }

    override fun bindView(item: CountryVisaTypeEnt?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val clSurface = viewHolder.getView(R.id.clSurface) as ConstraintLayout
            val tvDlt = viewHolder.getView(R.id.tvDlt) as TextView
            val tvCountryName = viewHolder.getView(R.id.tvCountryName) as TextView
            val tvType = viewHolder.getView(R.id.tvType) as TextView
            val tvPrice = viewHolder.getView(R.id.tvPrice) as TextView

            TextViewHelper.setText(tvCountryName, item.title)
            TextViewHelper.setHtmlText(tvType, "Type: ", item.visatype)
            TextViewHelper.setHtmlText(tvPrice, "Price: ", item.price)

            /*Bottom */
            tvDlt.setOnClickListener() {
                instance.onSwipeDelete(viewHolder.adapterPosition)
            }
            clSurface.setOnClickListener {
                //Edit Visa Type
                val fragment = VisaTypeForm()
                fragment.setmVisaTypeObj(item)

                (context as DockActivity).replaceDockableFragment(fragment, VisaTypeForm::class.java.simpleName)
            }
        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}