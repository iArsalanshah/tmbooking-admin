package com.tmbooking.admin.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.phase4.Notifications;
import com.tmbooking.admin.helpers.TextViewHelper;
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter;

//Only for use case of Generic @RecyclerViewListAdapter
public class NotificationsAdapter extends RecyclerViewListAdapter<Notifications> {
    private Context context;

    public NotificationsAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_notification, viewGroup, false);
    }

    @Override
    protected void bindView(Notifications item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            int position = viewHolder.getAdapterPosition();
            TextView tvTitle = (TextView) viewHolder.getView(R.id.tvTitle);
            TextView tvDateTime = (TextView) viewHolder.getView(R.id.tvDateTime);
            TextView message = (TextView) viewHolder.getView(R.id.message);

            TextViewHelper.setText(tvTitle, item.getTitle());
            TextViewHelper.setText(tvDateTime, item.getUpdated_at());
            TextViewHelper.setText(message, "Order No. " + item.getReference_order_id() +
                    "\nGuest Name: " + item.getGuest_name() +
                    "\n" + item.getMessage());

        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
