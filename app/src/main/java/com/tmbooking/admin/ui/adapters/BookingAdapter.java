package com.tmbooking.admin.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.BookingEnt;
import com.tmbooking.admin.helpers.DateHelper;
import com.tmbooking.admin.helpers.TextViewHelper;
import com.tmbooking.admin.interfaces.OnListItemClick;
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter;

import java.util.Date;

//Only for use case of Generic @RecyclerViewListAdapter
public class BookingAdapter extends RecyclerViewListAdapter<BookingEnt> {
    private Context context;
    OnListItemClick listener;

    public BookingAdapter(Context context, OnListItemClick listener) {
        super(context);
        this.listener = listener;
        this.context = context;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_booking, viewGroup, false);
    }

    @Override
    protected void bindView(final BookingEnt item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            final int position = viewHolder.getAdapterPosition();
            TextView tvOrderNo = (TextView) viewHolder.getView(R.id.tvOrderNo);
            TextView tvTransportType = (TextView) viewHolder.getView(R.id.tvTransportType);
            TextView tvPickupTime = (TextView) viewHolder.getView(R.id.tvPickupTime);
            TextView tvPickup = (TextView) viewHolder.getView(R.id.tvPickup);
            TextView tvDroppOff = (TextView) viewHolder.getView(R.id.tvDropOff);
            TextView tvExtraDetails = (TextView) viewHolder.getView(R.id.tvDetails);
            TextView tvDriverAssigned = (TextView) viewHolder.getView(R.id.tvDriverAssigned);

           /* viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((DockActivity) context).replaceDockableFragment(new BookingDetailsFormFragment());


                }});*/


            Date date = DateHelper.stringToDate(item.getPickuptime(), DateHelper.DATE_TIME_FORMAT);
            String mDate = DateHelper.getFormattedDate(date);
            String mTime = DateHelper.getFormattedTime(date);

//            Ride Status
//            TextViewHelper.setText(btnStatus, "Status: " + rideStatus(Util.getParsedInteger(item.getStatus())));


            TextViewHelper.setText(tvOrderNo, "Order Number: " + item.getOrderid());
            TextViewHelper.setText(tvTransportType, "Transport Type: " + item.getCarName());
            TextViewHelper.setText(tvPickup, "Pickup:"+item.getPickup());
            TextViewHelper.setText(tvDroppOff, "Dropoff:"+item.getDropoff());
            TextViewHelper.setText(tvPickupTime, "Date: " + mDate + " at " + mTime);
            TextViewHelper.setText(tvExtraDetails, "Extra Details: " + item.getExtrainfo());
            TextViewHelper.setText(tvDriverAssigned, "Driver Assigned: " + item.getRider_name());
//            if (isHistory) {
//                tvDetails.setVisibility(View.GONE);
//            } else
//                tvDetails.setVisibility(View.VISIBLE);

            viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(position, item);
                }
            });
        }
    }

   /* private String rideStatus(Integer status) {
        switch (status) {
            case AppConstants.PENDING:
                return "Confirmation pending";
            case AppConstants.ASSIGNED:
                return "Assigned";
            case AppConstants.ARRIVED:
                return "Arrived";
            case AppConstants.START_RIDE:
                return "Ride has started";
            case AppConstants.END_RIDE:
                return "Ride is completed";
            case AppConstants.CANCEL:
                return "Cancel by admin";
            case AppConstants.CONFIRM:
                return "Confirmed";
            case AppConstants.CANCEL_BY_DRIVER:
                return "Cancel by driver";
            default:
                return "-";
        }
    }*/

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
