package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.fragments.DriverDetailFormFragment
import com.tmbooking.admin.fragments.phase_4.GuestDetailsFragment
import com.tmbooking.admin.interfaces.OnClick
import com.tmbooking.admin.interfaces.OnViewHolderClick
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class BookingFilterAdapter(context: Context, internal var listener: OnClick) : RecyclerViewListAdapter<Drawable>(context) {
    private var selectedPosition = -1


    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_booking_filter, viewGroup, false)
    }

    override fun bindView(item: Drawable?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val clParent = viewHolder.getView(R.id.clParent) as ConstraintLayout
            val imgType = viewHolder.getView(R.id.imgType) as ImageView

            imgType.setImageDrawable(item)
            clParent.isSelected = selectedPosition == position

            clParent.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    selectedPosition = viewHolder.adapterPosition
                    listener.onItemClick(viewHolder.adapterPosition)
                    notifyDataSetChanged()
                }

            })

        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}