package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.entities.phase4.GuestEnt
import com.tmbooking.admin.fragments.DriverDetailFormFragment
import com.tmbooking.admin.fragments.phase_4.GuestDetailsFragment
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class GuestListAdapter(context: Context) : RecyclerViewListAdapter<GuestEnt>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_guest_list, viewGroup, false)
    }

    override fun bindView(item: GuestEnt?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val tvGuestName = viewHolder.getView(R.id.tvGuestName) as TextView

            TextViewHelper.setText(tvGuestName, item.name)

            viewHolder.getView().setOnClickListener {
                var fragment = GuestDetailsFragment()
                fragment.guestDetails = item
                (context as DockActivity).replaceDockableFragment(fragment)
            }

        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}