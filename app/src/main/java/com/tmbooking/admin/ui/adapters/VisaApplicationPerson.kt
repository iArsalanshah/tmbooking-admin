package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.entities.phase4.VisaMemberEnt
import com.tmbooking.admin.fragments.phase_4.visa_application.VisaPersonDetailFormFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class VisaApplicationPerson(context: Context) : RecyclerViewListAdapter<VisaMemberEnt>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_visa_person, viewGroup, false)
    }

    override fun bindView(item: VisaMemberEnt?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val tvName = viewHolder.getView(R.id.tvName) as TextView
            val tvGroupLeader = viewHolder.getView(R.id.tvGroupLeader) as TextView

            TextViewHelper.setText(tvName, item.fullname)

//            if (position == 0)
//                tvGroupLeader.visibility = View.VISIBLE
//            else
//                tvGroupLeader.visibility = View.GONE

            if (item.is_lead == AppConstants.LEAD) {
                TextViewHelper.setText(tvGroupLeader, "Leader")
            } else
                TextViewHelper.setText(tvGroupLeader, item.relation)


            viewHolder.getView().setOnClickListener {
                var fragment = VisaPersonDetailFormFragment()
                fragment.memberId = item.memberid.toInt()
                (context as DockActivity).replaceDockableFragment(fragment)
            }

        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}