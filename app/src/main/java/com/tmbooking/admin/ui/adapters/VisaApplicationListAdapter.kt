package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.entities.phase4.VisaEnt
import com.tmbooking.admin.fragments.phase_4.visa_application.VisaPersonDetailFormFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.interfaces.OnVisaItemClick
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

class VisaApplicationListAdapter(context: Context, var mFragmentName: String, var clickListener: OnVisaItemClick) : RecyclerViewListAdapter<VisaEnt>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_visa_application, viewGroup, false)
    }

    override fun bindView(item: VisaEnt?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val tvApplicant = viewHolder.getView(R.id.tvApplicant) as TextView
            val tvOrderNo = viewHolder.getView(R.id.tvOrderNo) as TextView
            val tvStatus = viewHolder.getView(R.id.tvStatus) as TextView
            val tvCountry = viewHolder.getView(R.id.tvCountry) as TextView
            val tvVisaStatus = viewHolder.getView(R.id.tvVisaStatus) as TextView
            val tvType = viewHolder.getView(R.id.tvType) as TextView
            val tvDataOfEntry = viewHolder.getView(R.id.tvDataOfEntry) as TextView
            val btnEdit = viewHolder.getView(R.id.btnEdit) as Button

            val orderNo = "Order No: ";
            TextViewHelper.setHtmlText(tvOrderNo, "Order No. ", item.orderid)
//            tvOrderNo.setText(Html.fromHtml("<b>" + orderNo + "</b> "+item.orderid))

            TextViewHelper.setText(tvApplicant, item.fullname)
            TextViewHelper.setHtmlText(tvCountry, "Country: ", item.country)
            TextViewHelper.setHtmlText(tvType, "Type: ", item.visatype)
            TextViewHelper.setHtmlText(tvStatus, "Payment Status: ", item.payment_status)
            TextViewHelper.setHtmlText(tvVisaStatus, "Visa Status: ", item.status_name)
            TextViewHelper.setHtmlText(tvDataOfEntry, "Date of entry: ", item.date)

            viewHolder.getView().setOnClickListener {
//                when (mFragmentName) {
//                    AppConstants.GROUP -> {
//                        var fragment = VisaApplicationDetailFragment()
//                        fragment.visaObj = item
//                        (context as DockActivity).replaceDockableFragment(fragment)
//                    }
//                    AppConstants.INDIVIDUAL -> {
//                        var fragment = VisaPersonDetailFormFragment()
//                        fragment.memberId = item.memberid.toInt()
//                        (context as DockActivity).replaceDockableFragment(fragment)
//                    }
//                }
                clickListener.onClick(position,item)

            }
            when (mFragmentName) {
                AppConstants.GROUP ->
                    tvVisaStatus.visibility = View.GONE // Because every person has its own visa status
                AppConstants.INDIVIDUAL ->
                    tvVisaStatus.visibility = View.VISIBLE
            }
            btnEdit.setOnClickListener {
                var fragment = VisaPersonDetailFormFragment()
                fragment.memberId = item.memberid.toInt()
                (context as DockActivity).replaceDockableFragment(fragment)
            }

        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }
}