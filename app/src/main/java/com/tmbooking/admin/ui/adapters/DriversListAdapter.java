package com.tmbooking.admin.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.common.base.Function;
import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;
import com.tmbooking.admin.entities.DriverEnt;
import com.tmbooking.admin.fragments.DriverDetailFormFragment;
import com.tmbooking.admin.fragments.DriverMapFragment;
import com.tmbooking.admin.helpers.TextViewHelper;
import com.tmbooking.admin.helpers.UIHelper;
import com.tmbooking.admin.interfaces.OnViewHolderClick;
import com.tmbooking.admin.ui.adapters.abstracts.FilterableRecyclerViewListAdapter;
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter;

import de.hdodenhof.circleimageview.CircleImageView;

//Only for use case of Generic @RecyclerViewListAdapter
public class DriversListAdapter extends FilterableRecyclerViewListAdapter<DriverEnt>
        implements Filterable {
    private Context context;
    private String[] visaType = {"Title:", "Visa Type:", "Price:"};
    private String[] visaApplication = {"Full Name:", "Date:", "Status:"};

    public DriversListAdapter(Context context, OnViewHolderClick listener, Function<DriverEnt, String> converter) {
        super(context, listener, converter);
        this.context = context;

    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_drivers_list, viewGroup, false);
    }

    @Override
    protected void bindView(final DriverEnt item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            int position = viewHolder.getAdapterPosition();


            TextView tvName = (TextView) viewHolder.getView(R.id.tvName);
            TextView tvCar = (TextView) viewHolder.getView(R.id.tvCar);
            TextView tvLicenseNumber = (TextView) viewHolder.getView(R.id.tvLicenseNumber);
            TextView tvEmergencyContact = (TextView) viewHolder.getView(R.id.tvEmergencyContact);
            CircleImageView civLiveLocation = (CircleImageView) viewHolder.getView(R.id.civLiveLocation);

            TextViewHelper.setText(tvName, item.getName() + "-" + item.getDriverid());
            TextViewHelper.setText(tvCar, item.getAssignedCar());
            TextViewHelper.setText(tvLicenseNumber, item.getLicenceNo());
            TextViewHelper.setText(tvEmergencyContact, item.getEmergencyContact());

            civLiveLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (item.getLatitude() != null && item.getLongitude() != null) {
                        DriverMapFragment fragment = new DriverMapFragment();
                        fragment.setOnlyDriverMarker(item);
                        ((DockActivity) context).replaceDockableFragment(fragment);
                    }
                    else
                        UIHelper.showShortToastInCenter(context, "Invalid Location");
                }
            });


            viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DriverDetailFormFragment fragment = new DriverDetailFormFragment();
                    fragment.setObjectData(item);
                    ((DockActivity) context).replaceDockableFragment(fragment);
                }
            });

        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
