package com.tmbooking.admin.ui.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.MeetGreetImages
import com.tmbooking.admin.helpers.ImageLoaderHelper
import com.tmbooking.admin.interfaces.RemovePicture
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter


class PictureListAdapter(context: Context, internal var removePictureListener: RemovePicture) : RecyclerViewListAdapter<MeetGreetImages>(context) {

    override fun createView(context: Context, viewGroup: ViewGroup?, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.item_picture, viewGroup, false)
    }

    override fun bindView(item: MeetGreetImages?, viewHolder: RecyclerviewViewHolder) {
        if (item != null) {
            val position = viewHolder.getAdapterPosition()
            val imgPicture = viewHolder.getView(R.id.imgPicture) as ImageView
            val imgCancel = viewHolder.getView(R.id.imgCancel) as ImageView

            if (item.isResponseImage) {//URL IMAGE
                val target = object : Target {
                    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                        Log.d("DEBUG", "onBitmapLoaded")
                        item.imageBitmap = bitmap
                        imgPicture.setImageBitmap(bitmap)
                    }

                    override fun onBitmapFailed(errorDrawable: Drawable) {
                        Log.d("DEBUG", "onBitmapFailed")
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable) {
                        Log.d("DEBUG", "onPrepareLoad")
                    }
                }

                Picasso.with(context)
                        .load(item.imageUrl)
                        .placeholder(R.drawable.placeholder_image)
                        .error(R.drawable.placeholder_image)
                        .into(target)

            } else {//LOCAL IMAGE
                ImageLoaderHelper.loadImageWithPicasso(context, item.imageUrl, imgPicture)
            }

//            CameraHelper.retrieveAndDisplayPicture(item.requestCode, item.resultCode, item.dataIntent, context as DockActivity, imgPicture)

            imgCancel.setOnClickListener { removePictureListener.onRemove(position, item) }

        }
    }

    override fun bindItemViewType(position: Int): Int {
        return 0
    }

    override fun bindItemId(position: Int): Int {
        return position
    }

    private fun getPicasso(): Picasso {
        val picasso = Picasso.Builder(context).listener(object : Picasso.Listener {
            override fun onImageLoadFailed(picasso: Picasso, uri: Uri, exception: Exception) {}
        }).build()

        return picasso
    }

}