package com.tmbooking.admin.retrofit;


import com.tmbooking.admin.entities.BookingEnt;
import com.tmbooking.admin.entities.DriverEnt;
import com.tmbooking.admin.entities.Nothing;
import com.tmbooking.admin.entities.ResponseWrapper;
import com.tmbooking.admin.entities.RideListEnt;
import com.tmbooking.admin.entities.UserEnt;
import com.tmbooking.admin.entities.UserProfileEnt;
import com.tmbooking.admin.entities.VisaApplicationEnt;
import com.tmbooking.admin.entities.VisaTypesEnt;
import com.tmbooking.admin.entities.phase4.AddDelivery;
import com.tmbooking.admin.entities.phase4.AuthToken;
import com.tmbooking.admin.entities.phase4.BookingDates;
import com.tmbooking.admin.entities.phase4.CheckIn;
import com.tmbooking.admin.entities.phase4.CountryListEnt;
import com.tmbooking.admin.entities.phase4.Delivery;
import com.tmbooking.admin.entities.phase4.DeliverySucces;
import com.tmbooking.admin.entities.phase4.DriverLogList;
import com.tmbooking.admin.entities.phase4.GuestDetails;
import com.tmbooking.admin.entities.phase4.GuestEnt;
import com.tmbooking.admin.entities.phase4.LoginResponse;
import com.tmbooking.admin.entities.phase4.ManageBooking;
import com.tmbooking.admin.entities.phase4.MeetAndGreetEnt;
import com.tmbooking.admin.entities.phase4.Notifications;
import com.tmbooking.admin.entities.phase4.VisaEnt;
import com.tmbooking.admin.entities.phase4.VisaMemberEnt;
import com.tmbooking.admin.entities.phase4.VisaStatusEnt;
import com.tmbooking.admin.entities.phase4.VisaStatusListEnt;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface WebService {


    @FormUrlEncoded
    @POST("login")
    Call<ResponseWrapper<UserEnt>> login(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device") String device,
            @Field("device_token") String deviceToken
    );

    /*TODO Arsalan Login Work Start*/
    @POST("wp/v2/users/me")
    Call<LoginResponse> login(
            @Header("Authorization") String authToken
    );

    @FormUrlEncoded
    @POST("jwt-auth/v1/token")
    Call<AuthToken> authToken(
            @Field("username") String username,
            @Field("password") String password
    );
    /*TODO Arsalan Login Work End*/

    //update profile
    @Multipart
    @POST("updateProfile")
    //CHange after get it
    Call<ResponseWrapper<UserEnt>> updateProfile(
            @Part MultipartBody.Part profile_picture,
            @Part("full_name") RequestBody full_name,
            @Part("email") RequestBody email,
            @Part("mobile_no") RequestBody mobile_no,
            @Part("location") RequestBody location
    );


    @FormUrlEncoded
    @POST("getmyrides")
    Call<ResponseWrapper<List<RideListEnt>>> getmyrides(
            @Field("userid") int userid
    );

    //Update ride status
    @FormUrlEncoded
    @POST("updateridestatus")
    Call<ResponseWrapper<RideListEnt>> updateridestatus(
            @Field("rideid") int rideid,
            @Field("status") int status
    );

    //Active status
    @FormUrlEncoded
    @POST("getactivedriverrides")
    Call<ResponseWrapper<List<RideListEnt>>> getactivedriverrides(
            @Field("driverid") int driverid
    );

    //Past rides
    @FormUrlEncoded
    @POST("getpastriverrides")
    Call<ResponseWrapper<List<RideListEnt>>> getpastriverrides(
            @Field("driverid") int driverid
    );

    //User profile
    @FormUrlEncoded
    @POST("getuserprofile")
    Call<ResponseWrapper<UserProfileEnt>> getuserprofile(
            @Field("userid") int userid
    );

    //User profile
    @FormUrlEncoded
    @POST("updatelocation")
    Call<ResponseWrapper> updatelocation(
            @Field("rideid") int rideid,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude
    );

    /*Admin constants*/
//visatypelist
    @FormUrlEncoded
    @POST("visatypelist")
    Call<ResponseWrapper<List<VisaTypesEnt>>> visatypelist(
            @Field("userid") int userid
    );

    @FormUrlEncoded
    @POST("visaapplicationlist")
    Call<ResponseWrapper<List<VisaApplicationEnt>>> visaapplicationlist(
            @Field("userid") int userid
    );


    @POST("activerides")
    Call<ResponseWrapper<List<BookingEnt>>> activerides(
//            @Field("userid") int userid *//* *//*
    );
/*
    @FormUrlEncoded
    @POST("pendingrides")
    Call<ResponseWrapper<List<BookingEnt>>> pendingrides(
            @Field("userid") int userid *//* *//*
    );

    @FormUrlEncoded
    @POST("cancelrideslist")
    Call<ResponseWrapper<List<BookingEnt>>> cancelrideslist(
            @Field("userid") int userid *//* *//*
    );


    @FormUrlEncoded
    @POST("bookinghistory")
    Call<ResponseWrapper<List<BookingEnt>>> bookinghistory(
            @Field("userid") int userid *//* *//*
    );*/
    @FormUrlEncoded
    @POST("getRidesBooking")
    Call<ResponseWrapper<List<BookingEnt>>> getRidesBooking(
            @Field("status") int status /* */
    );
    @FormUrlEncoded
    @POST("getassigneddrivers")
    Call<ResponseWrapper<List<DriverLogList>>> getassigneddrivers(
            @Field("id") String id /* */
    );

    @FormUrlEncoded
    @POST("confirmationBooking")
    Call<ResponseWrapper> confirmationBooking(
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("unbookdriver")
    Call<ResponseWrapper<List<DriverEnt>>> unbookdriver(
            @Field("id") String userid /* */
    );

    @FormUrlEncoded
    @POST("assigndriver")
    Call<ResponseWrapper> assigndriver(
            @Field("pickuptime") String pickuptime,
            @Field("driverid") String driverid,
            @Field("id") String rideid /*Ride id*/
    );


    @Multipart
    @POST("addcountryvisatype") /*updated*/
    Call<ResponseWrapper> addvisatype(
            @Part MultipartBody.Part image,
            @Part("visatype") RequestBody visatype,
            @Part("title") RequestBody title,
            @Part("duration") RequestBody duration,
            @Part("price") RequestBody price,
            @Part("description") RequestBody description,
            @Part("apply") RequestBody apply,
            @Part("restriction") RequestBody restriction,
            @Part("arrival") RequestBody arrival,
            @Part("countryid") RequestBody countryid

    );

    @Multipart
    @POST("updatecountryvisatype")  /*updated*/
    Call<ResponseWrapper> updatevisatype(
            @Part MultipartBody.Part image,
            @Part("visatype") RequestBody visatype,
            @Part("title") RequestBody title,
            @Part("duration") RequestBody duration,
            @Part("price") RequestBody price,
            @Part("description") RequestBody description,
            @Part("apply") RequestBody apply,
            @Part("restriction") RequestBody restriction,
            @Part("arrival") RequestBody arrival,
            @Part("id") RequestBody id,
            @Part("countryid") RequestBody countryid
    );


    @FormUrlEncoded
    @POST("removevisatype")
    Call<ResponseWrapper> removevisatype(
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("updateparentvisastatus")
    Call<ResponseWrapper> updateparentvisastatus(
            @Field("status") String status,
            @Field("reason") String reason,
            @Field("visaformid") String visaformid
    );

    @FormUrlEncoded
    @POST("updatemembervisastatus")
    Call<ResponseWrapper> updatemembervisastatus(
            @Field("status") String status,
            @Field("reason") String reason,
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("getmembers")
    Call<ResponseWrapper<List<VisaApplicationEnt>>> getmembers(
            @Field("userid") String userid
    );


    @Multipart
    @POST("driverregistration")
    Call<ResponseWrapper> driverregistration(
            @Part MultipartBody.Part drivers_photo,
            @Part MultipartBody.Part license_photo,
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("ikama") RequestBody ikama,
            @Part("nationality") RequestBody nationality,
            @Part("kafil_name") RequestBody kafil_name,
            @Part("emergency_contact") RequestBody emergency_contact,
            @Part("age") RequestBody age,
            @Part("licence_no") RequestBody licence_no,
            @Part("license_expiry") RequestBody license_expiry,
            @Part("registeration_numnber") RequestBody registeration_numnber,
            @Part("assigned_Car") RequestBody assigned_Car,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("state") RequestBody state,
            @Part("zipcode") RequestBody zipcode,
            @Part("home_phone") RequestBody home_phone,
            @Part("work_phone") RequestBody work_phone,
            @Part("joinning") RequestBody joinning,
            @Part("id") RequestBody id
    );


    @FormUrlEncoded
    @POST("driverlist")
    Call<ResponseWrapper<List<DriverEnt>>> driverlist(
            @Field("id") String id /* */
    );

    @FormUrlEncoded
    @POST("block_unblockdriver")
    Call<ResponseWrapper> block_unblockdriver(
            //1 unblock
            //2 block
            @Field("driverid") String driverid,
            @Field("status") int status
    );

    @FormUrlEncoded
    @POST("removedriver")
    Call<ResponseWrapper> removedriver(
            @Field("driverid") String driverid);

    /*PHASE 4*/


    @GET("tmbooking-api/v1/route/list_bookings")
    Call<ManageBooking> list_bookings(
            @Query("dates") String dates,
            @Query("user_id") int user_id
    );

    @GET("tmbooking-api/v1/route/list_bookings_count")
    Call<BookingDates> list_bookings_count(
            @Query("month") String month,
            @Query("year") String year,
            @Query("user_id") int user_id
    );

    @FormUrlEncoded
    @POST("tmbooking-api/v1/route/deliveries/delivered")
    Call<DeliverySucces> delivery(
            @Field("delivery_id") String delivery_id,
            @Field("user_id") int user_id
    );

    @FormUrlEncoded
    @POST("tmbooking-api/v1/route/checkin")
    Call<CheckIn> checkIn(
            @Field("booking_id") String booking_id,
            @Field("room_numbers") String room_numbers,
            @Field("check_in_comments") String check_in_comments
    );

    @FormUrlEncoded
    @POST("tmbooking-api/v1/route/checkout")
    Call<List<Nothing>> checkout(
            @Field("booking_id") String booking_id
    );

    @GET("tmbooking-api/v1/route/all_guests")
    Call<List<GuestEnt>> all_guests(
            @Query("order_id") String order_id
    );

    @GET("tmbooking-api/v1/route/guest_detail")
    Call<GuestDetails> guest_detail(
            @Query("order_id") String order_id,
            @Query("guest_id") String guest_id,
            @Query("user_id") int user_id
    );


    @FormUrlEncoded
    @POST("tmbooking-api/v1/route/update_guest")
    Call<GuestEnt> update_guest(
            @Field("guest_id") String guest_id,
            @Field("order_id") String order_id,
            @Field("date_of_birth") String date_of_birth,
            @Field("email") String email,
            @Field("local_contact_number") String local_contact_number,
            @Field("name") String name,
            @Field("passport_expiry") String passport_expiry,
            @Field("passport_number") String passport_number,
            @Field("saudi_contact_number") String saudi_contact_number,
            @Field("visa_number") String visa_number,
            @Field("visa_sponser") String visa_sponser
    );


    @GET("tmbooking-api/v1/route/meetgreet")
    Call<MeetAndGreetEnt> getMeetgreet(
            @Query("booking_id") String booking_id
    );


    @Multipart
    @POST("tmbooking-api/v1/route/meetgreet")
    Call<MeetAndGreetEnt> postMeetgreet(
            @Part("booking_id") RequestBody booking_id,
            @Part("meet_id") RequestBody meet_id,
            @Part("is_mandoob_meet") RequestBody is_mandoob_meet,
            @Part("meet_date_time") RequestBody meet_date_time,
            @Part("meet_comments") RequestBody meet_comments,
//            @Part("meet_picture[]") List<RequestBody> meet_picture
            @Part List<MultipartBody.Part> meet_picture
    );

    @GET("tmbooking-api/v1/route/deliveries/items")
    Call<AddDelivery> getDeliveryItems();

    @FormUrlEncoded
    @POST("tmbooking-api/v1/route/deliveries")
    Call<Delivery> addDeliveries(
            @Field("booking_id") String booking_id,
            @Field("item_name") String item_name,
            @Field("assigned_to") String assigned_to,
            @Field("delivery_time") String delivery_time,
            @Field("room_number") String room_number,
            @Field("quantity") String quantity

    );

    @GET("tmbooking-api/v1/route/get_user_notifications")
    Call<List<Notifications>> getUserNotifications(
            @Query("user_id") int user_id,
            @Query("notification_for") String notification_for
    );

    @FormUrlEncoded
    @POST("addtoken")
    Call<ResponseWrapper> addToken(
            @Field("userid") int userid,
            @Field("token") String token
    );

    //Visas
    @GET("getAllVisaApplications")
    Call<ResponseWrapper<List<VisaEnt>>> getallvisas(
            @Query("fromDate") String fromDate,
            @Query("toDate") String toDate,
            @Query("orderNo") String orderNo,
            @Query("passportNo") String passportNo,
            @Query("country") String country,
            @Query("paymentStatus") String paymentStatus,
            @Query("visaStatus") String visaStatus

    ); //Visas
    @GET("getAllVisaApplications")
    Call<ResponseWrapper<List<VisaEnt>>> getvisas(
            @Query("visaStatus") String visaStatus

    );

    @FormUrlEncoded
    @POST("getmember")
    Call<ResponseWrapper<List<VisaMemberEnt>>> getmember(
            @Field("memberid") int memberid
    );

    @FormUrlEncoded
    @POST("getvisatstatus")
    Call<ResponseWrapper<VisaStatusEnt>> getvisatstatus(
            @Field("formid") int formid
    );

    @FormUrlEncoded
    @POST("getgroupmembers")
    Call<ResponseWrapper<List<VisaMemberEnt>>> getgroupmembers(
            @Field("applicationid") int applicationid
    );

    @Multipart
    @POST("updatevisatstatus")
    Call<ResponseWrapper> updatevisatstatus(
            @Part("formid") RequestBody formid,
            @Part("status") RequestBody status,
            @Part("reason") RequestBody reason,
            @Part("comments") RequestBody comments,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST("updatemember")
    Call<ResponseWrapper> updatemember(
            @Part("fullname") RequestBody fullname,
            @Part("sirname") RequestBody sirname,
            @Part("mobile") RequestBody mobile,
            @Part("gender") RequestBody gender,
            @Part("martialstatus") RequestBody martialstatus,
            @Part("dob") RequestBody dob,
            @Part("countryofbirth") RequestBody countryofbirth,
            @Part("cnic") RequestBody cnic,
            @Part("aqama") RequestBody aqama,
            @Part("socialsecuritynumber") RequestBody socialsecuritynumber,
            @Part("livingin") RequestBody livingin,
            @Part("passportexpiry") RequestBody passportexpiry,
            @Part("passportdateofissue") RequestBody passportdateofissue,
            @Part("issuecountry") RequestBody issuecountry,
            @Part("issuecity") RequestBody issuecity,
            @Part("healthstatus") RequestBody healthstatus,
            @Part("education") RequestBody education,
            @Part("occupation") RequestBody occupation,
            @Part("memberid") RequestBody memberid,
            @Part MultipartBody.Part passport,
            @Part MultipartBody.Part photo,
            @Part MultipartBody.Part cnicphoto
    );

    @GET("getallgroup")
    Call<ResponseWrapper<List<VisaEnt>>> getallgroup();

    @GET("getstatus")
    Call<ResponseWrapper<List<VisaStatusListEnt>>> getstatus();


    @FormUrlEncoded
    @POST("tmbooking-api/v1/route/send_visa_email")
    Call<ResponseWrapper> send_visa_email(
            @Field("formid") String formid,
            @Field("email") String email
    );

    /*visa management*/
    @GET("countrylist")
    Call<ResponseWrapper<List<CountryListEnt>>> countrylist();

    @FormUrlEncoded
    @POST("removecountry")
    Call<ResponseWrapper> removecountry(
            @Field("countryid") String countryid
    );

    @FormUrlEncoded
    @POST("removecountryvisatype")
    Call<ResponseWrapper> removecountryvisatype(
            @Field("visatypeid") String visatypeid
    );


    @Multipart
    @POST("addcountry")
    Call<ResponseWrapper> addcountry(
            @Part("cname") RequestBody cname,
            @Part MultipartBody.Part photo
    );

    @Multipart
    @POST("updatecountry")
    Call<ResponseWrapper> updatecountry(
            @Part("countryid") RequestBody countryid,
            @Part("cname") RequestBody cname,
            @Part MultipartBody.Part photo
    );

 /*   @Multipart
    @POST("meetgreet")
    Call<MeetAndGreetEnt> postMeetgreet(
            @Part("booking_id") RequestBody booking_id,
            @Part("meet_id") RequestBody meet_id,
            @Part("meet_date_time") RequestBody meet_date_time,
            @Part("meet_comments") RequestBody meet_comments,
//            @Part("meet_picture[]") List<RequestBody> meet_picture
            @PartMap Map<String, RequestBody> meet_picture
    );*/



    /*

    @Multipart
    @POST("meetgreet")
    Call<MeetAndGreetEnt> postMeetgreet(
            @Query("booking_id") String booking_id,
            @Query("meet_id") String meet_id,
            @Query("meet_date_time") String meet_date_time,
            @Query("meet_comments") String meet_comments,
//            @Part("meet_picture[]") List<RequestBody> meet_picture
            @PartMap Map<String, RequestBody> meet_picture
    );*/
}