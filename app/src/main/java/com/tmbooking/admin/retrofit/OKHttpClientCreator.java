package com.tmbooking.admin.retrofit;


import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.tmbooking.admin.global.WebServiceConstants;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class OKHttpClientCreator {

    private static NotificationManager mNotifyManager;
    private static NotificationCompat.Builder mBuilder;

    public static OkHttpClient createMyCustomForPreLollipop(){
        OkHttpClient client=new OkHttpClient();
        try {
            client = new OkHttpClient.Builder()
                    .sslSocketFactory(new TLSSocketFactory())
                    .build();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return client;
    }

    public static OkHttpClient createCustomInterceptorClient(Context context) {

        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new CustomInterceptor(progressListener))
                .build();


        return client;


    }

    public static OkHttpClient createDefaultInterceptorClient(Context context) {

        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Response originalResponse = chain.proceed(chain.request());
                        return originalResponse.newBuilder()
                                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                                .build();
                    }
                })
                .build();


        return client;

    }

    final static ProgressResponseBody.ProgressListener progressListener = new ProgressResponseBody.ProgressListener() {
        @Override
        public void update(long bytesRead, long contentLength, boolean done) {
            int percent = (int) ((500 * bytesRead) / contentLength);
//            int percent = (int) ((100 * bytesRead) / contentLength);
        }
    };

/*    static OkHttpClient createOAuth1InterceptorClient(Context context) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
//                .addInterceptor(oauth1Woocommerce)// Interceptor oauth1Woocommerce added
                .build();
        return client;
    }*/

    static OkHttpClient createOAuth1InterceptorClient(Context context) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
//                .addInterceptor(oauth1Woocommerce)// Interceptor oauth1Woocommerce added
                .build();
        return client;
    }

  /*  private static OAuthInterceptor oauth1Woocommerce = new OAuthInterceptor.Builder()
            .consumerKey(WebServiceConstants.INSTANCE.getCUSTOMER_KEY_HERE())
            .consumerSecret(WebServiceConstants.INSTANCE.getCUSTOMER_SECERT_HERE())
            .build();*/


//    private static OAuthInterceptor oauth1Woocommerce = new OAuthInterceptor.Builder()
//            .consumerKey(WebServiceConstants.INSTANCE.getCUSTOMER_KEY_HERE())
//            .consumerSecret(WebServiceConstants.INSTANCE.getCUSTOMER_SECERT_HERE())
//            .build();

}
