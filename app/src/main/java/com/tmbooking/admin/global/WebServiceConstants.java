package com.tmbooking.admin.global;

import org.jetbrains.annotations.Nullable;

public class WebServiceConstants {
    public static final String SERVICE_URL = "";
    public static final String BASE_URL = "https://tmbooking.com/tmbooking/api/";
    public static final String BASE_URL_WP = "https://tmbooking.com/wp-json/";
    public static final String SUCCESS_RESPONSE_CODE = "success";
    public static final String login = "login";
    public static final String updateProfile = "updateProfile";
    public static final String getmyrides = "getmyrides";
    public static final String updateridestatus = "updateridestatus";
    public static final String getuserprofile = "getuserprofile";
    public static final String getpastriverrides = "getpastriverrides";
    public static final String getactivedriverrides = "getactivedriverrides";
    public static final String updatelocation = "updatelocation";
    public static final String ACTIVE = "Active Rides";
    public static final String UPCOMING = "Upcoming Rides";
    public static final String visatypelist = "visatypelist";
    public static final String visaapplicationlist = "visaapplicationlist";
    public static final String activerides = "activerides";
    public static final String confirmationBooking = "confirmationBooking";
    public static final String unbookdriver = "unbookdriver";
    public static final String assigndriver = "assigndriver";
    public static final String addvisatype = "addvisatype";
    public static final String updatevisatype = "updatevisatype";
    public static final String removevisatype = "removevisatype";
    public static final String updateparentvisastatus = "updateparentvisastatus";
    public static final String getmembers = "getmembers";
    public static final String updatemembervisastatus = "updatemembervisastatus";
    public static final String driverregistration = "driverregistration";
    public static final String driverlist = "driverlist";
    public static final String block_unblockdriver = "block_unblockdriver";
    public static final String removedriver = "removedriver";

    /*Phase 4*/
    public static final String LIST_BOOKINGS = "list_bookings";
    public static final String DELIVERY = "delivery";
    public static final String CHECKIN = "checkIn";
    public static final String LIST_BOOKINGS_COUNT = "list_booking_count";
    public static final String ALL_GUEST = "all_guests";
    public static final String UPDATE_GUEST = "update_guest";
    public static final String GET_MEET_GREET = "get_meet_greet";
    public static final String POST_MEET_GREET = "post_meet_greet";
    public static final String GET_DELIVERY_ITEMS = "get_delivery_items";
    public static final String POST_DELIVERY_ITEMS = "post_delivery_items";
    public static final String GUEST_DETAIL = "guest_detail";
    public static final String GET_USER_NOTIFICATIONS = "get_user_notifications";
    public static String addToken = "addToken";
    public static final String getmember = "getmember";
    public static final String getvisatstatus = "getvisatstatus";
    public static final String getallgroup = "getallgroup";
    public static final String getgroupmembers = "getgroupmembers";
    public static final String getstatus = "getstatus";
    public static final String updatevisatstatus = "updatevisatstatus";

    public static final String updatemember = "updatemember";
    @Nullable
    public static final String SEND_VISA_EMAIL = "send_visa_email";
    @Nullable
    public static final String countrylist = "countrylist";
    @Nullable
    public static final String removecountry = "removecountry";
    @Nullable
    public static final String removecountryvisatype = "removecountryvisatype";
    @Nullable
    public static final String addcountry = "addcountry";
    @Nullable
    public static final String updatecountry = "updatecountry";

    //    updates
    public static final String getAllVisaApplications = "getAllVisaApplications";

    @Nullable
    public static final String GET_ASSIGNED_DRIVERS="getassigneddrivers";
}
