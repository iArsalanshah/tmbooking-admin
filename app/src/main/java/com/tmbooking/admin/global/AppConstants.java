package com.tmbooking.admin.global;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AppConstants {
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String PENDING_TEXT = "Pending Bookings";
    public static final String ACTIVE_TEXT = "Active Bookings";
    public static final String CANCEL_TEXT = "Cancel Bookings";
    public static final String HISTORY_TEXT = "History Bookings";
    public static final String CONFIRMED_TEXT = "Confirmed Bookings";

    public static String twitter = "twitter";
    public static final String Device_Type = "android";
    public static final String SOCIAL_MEDIA_TYPE = "facebook";
    public static int INTENT_ID = 100;
    //Firebase Constants
    // broadcast receiver intent filters
    public static final String TOPIC_GLOBAL = "global";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static int NOTIFICATION_ID = 100;
    public static String DateFormat_DMY2 = "dd-MM-yy";
    public static String DateFormat_HM = "HH:mm";
    public static String DateFormat_YMDHMS = "yyyy-MM-dd HH:mm:ss";

    //Location
    public static final String SEND_LOCATION_BROADCAST = "locationBroadCast";
    public static final String RECEIVE_LOCATION_BROADCAST = "locationBroadCast";
    public static final String LAT = "latitude";
    public static final String LNG = "longitude";
    //=====================
    //Ride status
    public static final int PENDING = 0;
    public static final int ASSIGNED = 1;
    public static final int ARRIVED = 2;
    public static final int START_RIDE = 3;
    public static final int END_RIDE = 4;
    public static final int CANCEL = 5;
    public static final int CONFIRM = 6;
    public static final int CANCEL_BY_DRIVER = 7;

    /*Visa COnstant*/
    public static final int APPROVED = 1;
    public static final int REJECTED = 2;


    public static final String MAKKAH = "MAKKAH";
    public static final String MADINA = "MADINA";

    public static final String DELIVERY = "0";
    public static final String TRANSPORT = "1";
    public static final String HOTEL = "2";
    public static final String ALL = "3";

    public static final String CHECKOUT = "Check-out";
    public static final String CHECKIN = "Check-in";

    public static final String ADMINISTRATOR = "administrator";
    public static final String MANDOOB_MAKKAH = "mandoob_makkah";
    public static final String MANDOOB_MADINA = "mandoob_madina";
    public static final String MANDOOB_DELIVERY = "mandoob_delivery";
    public static final String RM_MAIN = "rm_main";
    public static final String RM_TRADE_PARTNER = "rm_trade_partner";
    public static final String PRIVLEDGED_MEMBER = "privledged_member";
    public static final String TRADE_PARTNER = "trade_partner";
    public static final String PENDING_DELIVERY = "Pending";
    public static final String DELIVERED = "Delivered";
    @NotNull
    public static final String GROUP = "group";
    public static final String INDIVIDUAL = "individual";
    @Nullable
    public static final String LEAD = "1";
}

