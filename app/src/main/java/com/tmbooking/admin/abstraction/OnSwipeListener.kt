package com.tmbooking.admin.abstraction

import com.tmbooking.admin.entities.phase4.CountryListEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.fragments.phase_4.visa_management.CountryFormFragment


/**
 * Created by Syeda Urooba Tafseer on 12,March,2019
 */
abstract class OnSwipeListener : BaseFragment() {
    fun onSwipeEdit(countryDetails: CountryListEnt?) {

        var fragment = CountryFormFragment()
        fragment.countryDetails = countryDetails
        dockActivity.replaceDockableFragment(fragment)
    }

    abstract fun onSwipeDelete(position: Int)
}