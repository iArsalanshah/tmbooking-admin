package com.tmbooking.admin.interfaces;

public interface FormActionButtons {
    void onAdd();
    void onEdit();
    void onUpdate();
}
