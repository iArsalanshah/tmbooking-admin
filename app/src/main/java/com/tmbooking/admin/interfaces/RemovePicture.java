package com.tmbooking.admin.interfaces;

import com.tmbooking.admin.entities.phase4.MeetGreetImages;

public interface RemovePicture {
    void onRemove(int position, MeetGreetImages item);

}
