package com.tmbooking.admin.interfaces;

import com.tmbooking.admin.entities.BookingEnt;

public interface OnListItemClick {
    void onClick(int position, BookingEnt obj);
}