package com.tmbooking.admin.interfaces;

/**
 * Created on 7/17/2017.
 */

public interface WebServiceResponseWPListener<T> {
    public void ResponseSuccess(T result, String Tag);
    public void  ResponseFailure(String tag);
}
