package com.tmbooking.admin.interfaces;

import com.tmbooking.admin.entities.phase4.VisaEnt;

public interface OnVisaItemClick {
    void onClick(int position, VisaEnt obj);
}