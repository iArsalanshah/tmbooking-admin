package com.tmbooking.admin.interfaces;

/**
 * Created by syed.shah on 8/9/17.
 */

public interface RouteFailedListener {
    void onRouteCreationFailed();
}
