package com.tmbooking.admin.interfaces;

import android.view.View;

public interface OnClick {
    void onItemClick(int position);
}