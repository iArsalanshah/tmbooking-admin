package com.tmbooking.admin.interfaces;

import android.view.View;

public interface OnViewHolderClick {
    void onItemClick(View view, int position);
}