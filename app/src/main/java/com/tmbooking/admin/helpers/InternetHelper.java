package com.tmbooking.admin.helpers;

import android.content.Context;
import android.net.ConnectivityManager;

import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;

public class InternetHelper {

    public static boolean CheckInternetConectivityandShowToast(DockActivity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            return true;
        } else {
            UIHelper.showLongToastInCenter(activity, activity.getString(R.string.connection_lost));
            return false;
        }
    }

    public static boolean CheckInternetConectivity(DockActivity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            return true;
        } else {
            return false;
        }
    }
}
