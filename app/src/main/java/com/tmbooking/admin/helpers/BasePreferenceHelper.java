package com.tmbooking.admin.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.tmbooking.admin.entities.UserEnt;
import com.tmbooking.admin.entities.phase4.LoginResponse;
import com.tmbooking.admin.retrofit.GsonFactory;


public class BasePreferenceHelper extends PreferenceHelper {

    private Context context;
    private static final String FILENAME = "preferences";
    private static final String KEY_LOGIN_STATUS = "islogin";
    private static final String KEY_USER = "key_user";
    private static final String TOKEN_KEY = "token_key";
    private static final String LOGIN_DATA_KEY = "loginDataKey";


    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    public void setLoginStatus(boolean isLogin) {
        putBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS, isLogin);
    }

    public boolean isLogin() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

    public UserEnt getUser() {
        return GsonFactory.getConfiguredGson().fromJson(
                getStringPreference(context, FILENAME, KEY_USER), UserEnt.class);
    }

    public void putUser(UserEnt user) {
        putStringPreference(context, FILENAME, KEY_USER, GsonFactory
                .getConfiguredGson().toJson(user));
    }


    /*Arsalan Login Work*/
    public void setToken(String token) {
        putStringPreference(context, FILENAME, TOKEN_KEY, token);
    }

    public String getToken() {
        return getStringPreference(context, FILENAME, TOKEN_KEY);
    }

    public LoginResponse getLoginData() {
        String obj = getStringPreference(context, FILENAME, LOGIN_DATA_KEY);
        return new Gson().fromJson(obj, LoginResponse.class);
    }

    public void setLoginData(String data) {
        putStringPreference(context, FILENAME, LOGIN_DATA_KEY, data);
    }

}
