package com.tmbooking.admin.helpers;

import android.util.Log;

import com.tmbooking.admin.R;
import com.tmbooking.admin.activities.DockActivity;
import com.tmbooking.admin.entities.ResponseWrapper;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.interfaces.WebServiceResponseWPListener;
import com.tmbooking.admin.interfaces.webServiceResponseLisener;
import com.tmbooking.admin.retrofit.WebService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created on 7/17/2017.
 */

public class ServiceHelper<T> {
    private webServiceResponseLisener serviceResponseLisener;
    private WebServiceResponseWPListener webServiceResponseWPListener;
    private DockActivity context;
    private WebService webService;

    public ServiceHelper(webServiceResponseLisener serviceResponseLisener,
                         WebServiceResponseWPListener webServiceResponseWPListener, DockActivity conttext, WebService webService) {
        this.serviceResponseLisener = serviceResponseLisener;
        this.webServiceResponseWPListener = webServiceResponseWPListener;
        this.context = conttext;
        this.webService = webService;
    }

    public void enqueueCall(Call<ResponseWrapper<T>> call, final String tag, final boolean showLoader) {
        if (InternetHelper.CheckInternetConectivityandShowToast(context)) {
            if (showLoader)
                context.onLoadingStarted();
            call.enqueue(new Callback<ResponseWrapper<T>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<T>> call, Response<ResponseWrapper<T>> response) {
                    if (showLoader)
                        context.onLoadingFinished();
                    if (response.body() != null &&
                            response.body().getResponse().equals(WebServiceConstants.SUCCESS_RESPONSE_CODE)) {
                        serviceResponseLisener.ResponseSuccess(response.body().getResult(), tag, response.body().getMessage());
                    } else if (response.body() != null && response.body().getMessage() != null) {
                        UIHelper.showShortToastInCenter(context, response.body().getMessage());
                    } else {
                        UIHelper.showShortToastInCenter(context, context.getResources().getString(R.string.error_failure));
                    }

                }

                @Override
                public void onFailure(Call<ResponseWrapper<T>> call, Throwable t) {
                    if (showLoader)
                        context.onLoadingFinished();
                    t.printStackTrace();
                    Log.e(ServiceHelper.class.getSimpleName() + " by tag: " + tag, t.toString());
                    UIHelper.showShortToastInCenter(context, t.toString());
                }
            });
        }
    }

    public void enqueueCallWP(Call<T> call, final String tag, final boolean showLoader) {
        if (InternetHelper.CheckInternetConectivityandShowToast(context)) {
            if (showLoader)
                context.onLoadingStarted();
            call.enqueue(new Callback<T>() {
                @Override
                public void onResponse(Call<T> call, Response<T> response) {
                    if (showLoader)
                        context.onLoadingFinished();
                    if (response.body() != null) {
                        Log.d("ServiceHelper", response.body().toString());
                        webServiceResponseWPListener.ResponseSuccess(response.body(), tag);
                    } else {
                        UIHelper.showShortToastInCenter(context, context.getResources().getString(R.string.error_failure));
                    }

                   /* if (response.body() != null &&
                            response.body().getResponse().equals(WebServiceConstants.SUCCESS_RESPONSE_CODE)) {
                    } else if (response.body() != null && response.body().getMessage() != null) {
                        UIHelper.showShortToastInCenter(context, response.body().getMessage());
                    } else {
                        UIHelper.showShortToastInCenter(context, context.getResources().getString(R.string.error_failure));
                    }*/

                }

                @Override
                public void onFailure(Call<T> call, Throwable t) {
                    if (showLoader)
                        context.onLoadingFinished();
                    t.printStackTrace();
                    Log.e(ServiceHelper.class.getSimpleName() + " by tag: " + tag, t.toString());
                    UIHelper.showShortToastInCenter(context, t.toString());
                }
            });
        }
    }

}
