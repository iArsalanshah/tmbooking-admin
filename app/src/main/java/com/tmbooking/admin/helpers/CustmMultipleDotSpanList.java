package com.tmbooking.admin.helpers;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.style.LineBackgroundSpan;

public class CustmMultipleDotSpanList implements LineBackgroundSpan {

    public static final float DEFAULT_RADIUS = 3;

    private final float radius;
    private int[] color = new int[0];
    int size;


    public CustmMultipleDotSpanList() {
        this.radius = DEFAULT_RADIUS;
        this.color[0] = 0;
    }


    public CustmMultipleDotSpanList(int color) {
        this.radius = DEFAULT_RADIUS;
        this.color[0] = 0;
    }


    public CustmMultipleDotSpanList(float radius) {
        this.radius = radius;
        this.color[0] = 0;
    }


    public CustmMultipleDotSpanList(float radius, int[] color, int size) {
        this.radius = radius;
        this.color = color;
        this.size = size;
    }

    @Override
    public void drawBackground(
            Canvas canvas, Paint paint,
            int left, int right, int top, int baseline, int bottom,
            CharSequence charSequence,
            int start, int end, int lineNum
    ) {

        int total = color.length > 5 ? 5 : color.length;
        int leftMost = (total - 1) * -10;

        for (int i = 0; i < total; i++) {
            int oldColor = paint.getColor();
            if (color[i] != 0) {
//                paint.setColor(color[i]);
            }
//            canvas.drawCircle((left + right) / 2 - leftMost, bottom + radius, radius, paint);

            leftMost = leftMost + 10;

            paint.setFakeBoldText(true);
            canvas.drawText(size + "", (left + right) / 2 - leftMost, bottom + radius, paint);

//            paint.setColor(oldColor);


        }


    }
}