package com.tmbooking.admin.helpers;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.tmbooking.admin.interfaces.ResultCallback;

import java.util.Calendar;
import java.util.Date;

public class DateTimePicker {

    @NonNull
    private final Calendar calendar = Calendar.getInstance();
    @Nullable
    private DatePickerDialog datePickerDialog;
    @Nullable
    private TimePickerDialog timePickerDialog;
    @Nullable
    private ResultCallback<Date> dateResultCallback;

    public void showDialog(@NonNull Context context, long time) {
        calendar.setTimeInMillis(time);
        closeDialogs();
        showDatePicker(context);
    }

    public void showDateDialog(@NonNull Context context, long time) {
        calendar.setTimeInMillis(time);
        closeDialogs();
        showOnlyDatePicker(context);
    }

    @Nullable
    public ResultCallback<Date> getDateResultCallback() {
        return dateResultCallback;
    }

    public void setDateResultCallback(@Nullable ResultCallback<Date> dateResultCallback) {
        this.dateResultCallback = dateResultCallback;
    }

    public long getTime() {
        return calendar.getTimeInMillis();
    }

    private void closeDialogs() {
        if (datePickerDialog != null) {
            datePickerDialog.dismiss();
            datePickerDialog = null;
        }
        if (timePickerDialog != null) {
            timePickerDialog.dismiss();
            timePickerDialog = null;
        }
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            timePicker(view.getContext());
        }
    };
    private DatePickerDialog.OnDateSetListener onlyDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//            if (dateResultCallback != null) {
//                dateResultCallback.onResult(calendar.getTime());
//            }
        }
    };

    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            if (dateResultCallback != null) {
                dateResultCallback.onResult(calendar.getTime());
            }
        }
    };

    private void showDatePicker(@NonNull Context context) {
        datePickerDialog = new DatePickerDialog(context,
                dateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void showOnlyDatePicker(@NonNull Context context) {
        datePickerDialog = new DatePickerDialog(context,
                onlyDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void timePicker(@NonNull Context context) {
        timePickerDialog = new TimePickerDialog(context,
                timeSetListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true);
        timePickerDialog.show();
    }

    public void release() {
        closeDialogs();
        dateResultCallback = null;
    }

}