package com.tmbooking.admin.helpers;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.tmbooking.admin.R;

public class ImageLoaderHelper {

    public static void loadImage(String url, ImageView img) {
        if (img == null) return;
        ImageLoader.getInstance()
                .displayImage(url, img);
    }

    public static void loadImageWithPicasso(Context context, String url, ImageView imageView) {
        if (imageView == null || TextUtils.isEmpty(url)) return;
        Picasso.with(context)
                .load(url)
//                    .error(R.drawable.placeholder_banner_home)
                .noPlaceholder()
                .into(imageView);
    }

    public static void loadImageWithPicasso(Context context, Uri uri, ImageView imageView) {
//        if (imageView == null || TextUtils.isEmpty(uri)) return;
        Picasso.with(context)
                .load(uri)
                .error(R.drawable.placeholder_image)
                .placeholder(null)
                .into(imageView);
    }
}
