package com.tmbooking.admin.helpers;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Decorate a day by making the text big and bold
 */
public class OneDayDecorator implements DayViewDecorator {

    private final int[] colors;
//    private final HashSet<CalendarDay> dates;
    private final CalendarDay dates;
    int size;


   /* public OneDayDecorator(Collection<CalendarDay> dates, int[] colors) {
        //this.color = color;
        this.dates = new HashSet<>(dates);
        this.colors = colors;

    }*/
    public OneDayDecorator(CalendarDay dates, int[] colors, int size) {
        //this.color = color;
        this.dates = dates;
        this.colors = colors;
        this.size = size;
    }

/*
    public OneDayDecorator(List<CalendarDay> filteredEvents) {
        //this.color = color;

        this.dates = new HashSet<>(filteredEvents.get(0).calDayArr);
        int[]colors = {0};
        colors[0]= filteredEvents.get(0).color;
        this.colors = colors;

    }*/

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.equals(day);
    }


    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan((new CustmMultipleDotSpanList(30, colors,size)));
    }
}
