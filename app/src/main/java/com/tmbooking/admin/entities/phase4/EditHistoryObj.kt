package com.tmbooking.admin.entities.phase4

data class EditHistoryObj(
        val current_value: String,
        val edit_history: List<EditHistory>
)