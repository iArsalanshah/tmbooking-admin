package com.tmbooking.admin.entities.phase4

data class EditHistory(
        val date_time: String,
        val new_value: String,
        val old_value: String,
        val user: String
)