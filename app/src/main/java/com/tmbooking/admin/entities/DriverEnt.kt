package com.tmbooking.admin.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DriverEnt {
    @SerializedName("id") /*unbook driver api*/
    @Expose
    private var id: String? = null
    @SerializedName("name")
    @Expose
    private var name: String? = null


    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(carName: String) {
        this.name = name
    }
    /*=================*/

    @SerializedName("driverid")
    @Expose
    private var driverid: String? = null
    @SerializedName("licence_no")
    @Expose
    private var licenceNo: String? = null
    @SerializedName("emergency_contact")
    @Expose
    private var emergencyContact: String? = null
    @SerializedName("latitude")
    @Expose
    private var latitude: String? = null
    @SerializedName("longitude")
    @Expose
    private var longitude: String? = null
    @SerializedName("email")
    @Expose
    private var email: String? = null
    @SerializedName("ikama")
    @Expose
    private var ikama: String? = null
    @SerializedName("nationality")
    @Expose
    private var nationality: String? = null
    @SerializedName("kafil_name")
    @Expose
    private var kafilName: String? = null
    @SerializedName("license_expiry")
    @Expose
    private var licenseExpiry: String? = null
    @SerializedName("license_photo")
    @Expose
    private var licensePhoto: String? = null
    @SerializedName("drivers_photo")
    @Expose
    private var driversPhoto: String? = null
    @SerializedName("assigned_Car")
    @Expose
    private var assignedCar: String? = null
    @SerializedName("age")
    @Expose
    private var age: String? = null
    @SerializedName("password")
    @Expose
    private var password: String? = null
    @SerializedName("address")
    @Expose
    private var address: String? = null
    @SerializedName("city")
    @Expose
    private var city: String? = null
    @SerializedName("state")
    @Expose
    private var state: String? = null
    @SerializedName("zipcode")
    @Expose
    private var zipcode: String? = null
    @SerializedName("home_phone")
    @Expose
    private var homePhone: String? = null
    @SerializedName("work_phone")
    @Expose
    private var workPhone: String? = null
    @SerializedName("joinning")
    @Expose
    private var joinning: String? = null
    @SerializedName("status")
    @Expose
    private var status: String? = null
    @SerializedName("registeration_numnber")
    @Expose
    private var registerationNumnber: String? = null

    fun getDriverid(): String? {
        return driverid
    }

    fun setDriverid(driverid: String) {
        this.driverid = driverid
    }

    fun getLicenceNo(): String? {
        return licenceNo
    }

    fun setLicenceNo(licenceNo: String) {
        this.licenceNo = licenceNo
    }

    fun getEmergencyContact(): String? {
        return emergencyContact
    }

    fun setEmergencyContact(emergencyContact: String) {
        this.emergencyContact = emergencyContact
    }

    fun getLatitude(): String? {
        return latitude
    }

    fun setLatitude(latitude: String) {
        this.latitude = latitude
    }

    fun getLongitude(): String? {
        return longitude
    }

    fun setLongitude(longitude: String) {
        this.longitude = longitude
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String) {
        this.email = email
    }

    fun getIkama(): String? {
        return ikama
    }

    fun setIkama(ikama: String) {
        this.ikama = ikama
    }

    fun getNationality(): String? {
        return nationality
    }

    fun setNationality(nationality: String) {
        this.nationality = nationality
    }

    fun getKafilName(): String? {
        return kafilName
    }

    fun setKafilName(kafilName: String) {
        this.kafilName = kafilName
    }

    fun getLicenseExpiry(): String? {
        return licenseExpiry
    }

    fun setLicenseExpiry(licenseExpiry: String) {
        this.licenseExpiry = licenseExpiry
    }

    fun getLicensePhoto(): String? {
        return licensePhoto
    }

    fun setLicensePhoto(licensePhoto: String) {
        this.licensePhoto = licensePhoto
    }

    fun getDriversPhoto(): String? {
        return driversPhoto
    }

    fun setDriversPhoto(driversPhoto: String) {
        this.driversPhoto = driversPhoto
    }

    fun getAssignedCar(): String? {
        return assignedCar
    }

    fun setAssignedCar(assignedCar: String) {
        this.assignedCar = assignedCar
    }

    fun getAge(): String? {
        return age
    }

    fun setAge(age: String) {
        this.age = age
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String) {
        this.password = password
    }

    fun getAddress(): String? {
        return address
    }

    fun setAddress(address: String) {
        this.address = address
    }

    fun getCity(): String? {
        return city
    }

    fun setCity(city: String) {
        this.city = city
    }

    fun getState(): String? {
        return state
    }

    fun setState(state: String) {
        this.state = state
    }

    fun getZipcode(): String? {
        return zipcode
    }

    fun setZipcode(zipcode: String) {
        this.zipcode = zipcode
    }

    fun getHomePhone(): String? {
        return homePhone
    }

    fun setHomePhone(homePhone: String) {
        this.homePhone = homePhone
    }

    fun getWorkPhone(): String? {
        return workPhone
    }

    fun setWorkPhone(workPhone: String) {
        this.workPhone = workPhone
    }

    fun getJoinning(): String? {
        return joinning
    }

    fun setJoinning(joinning: String) {
        this.joinning = joinning
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }

    fun getRegisterationNumnber(): String? {
        return registerationNumnber
    }

    fun setRegisterationNumnber(registerationNumnber: String) {
        this.registerationNumnber = registerationNumnber
    }

}