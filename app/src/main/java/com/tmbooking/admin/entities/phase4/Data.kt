package com.tmbooking.admin.entities.phase4

data class Data(
        val params: Params,
        val status: Int
)