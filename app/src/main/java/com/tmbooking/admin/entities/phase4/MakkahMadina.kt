package com.tmbooking.admin.entities.phase4

data class MakkahMadina (
        val delivery: List<Delivery>,
        val hotels: List<Hotel>,
        val selectedData: String,
        val transport: List<Transport>
)