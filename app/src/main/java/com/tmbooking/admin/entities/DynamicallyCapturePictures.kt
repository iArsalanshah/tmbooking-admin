package com.tmbooking.admin.entities

import android.content.Intent

class DynamicallyCapturePictures {
    var requestCode: Int = 0


    var resultCode: Int = 0
    var dataIntent: Intent? = null

    constructor(requestCode: Int, resultCode: Int, dataIntent: Intent?) {
        this.requestCode = requestCode
        this.resultCode = resultCode
        this.dataIntent = dataIntent
    }
}