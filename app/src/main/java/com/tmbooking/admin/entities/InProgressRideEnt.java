package com.tmbooking.admin.entities;

/**
 * Created by syedatafseer on 4/4/2018.
 */

public class InProgressRideEnt {
    RideListEnt rideListEnt;
    UserProfileEnt userProfileEnt;

    public InProgressRideEnt(RideListEnt rideListEnt, UserProfileEnt userProfileEnt) {
        this.rideListEnt = rideListEnt;
        this.userProfileEnt = userProfileEnt;
    }

    public RideListEnt getRideListEnt() {
        return rideListEnt;
    }

    public void setRideListEnt(RideListEnt rideListEnt) {
        this.rideListEnt = rideListEnt;
    }

    public UserProfileEnt getUserProfileEnt() {
        return userProfileEnt;
    }

    public void setUserProfileEnt(UserProfileEnt userProfileEnt) {
        this.userProfileEnt = userProfileEnt;
    }
}
