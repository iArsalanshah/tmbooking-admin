package com.tmbooking.admin.entities.phase4

data class Delivery(
        val booking_id: String,
        val contact_number: String,
        val date: String,
        val delivery_id: String,
        val guest_name: String,
        val name: String,
        val room_number: Int,
        val status: String,
        val time: String,
        val timestamp: Int,
        val quantity: Int,
        val hotel_name: String
)