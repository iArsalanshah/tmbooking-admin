package com.tmbooking.admin.entities.phase4

data class DeliverySucces(
        val assigned_to: String,
        val booking_id: String,
        val delivery_id: String,
        val delivery_time: String,
        val item_name: String,
        val status: String
)