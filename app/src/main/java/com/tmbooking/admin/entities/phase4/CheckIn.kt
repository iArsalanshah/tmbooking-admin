package com.tmbooking.admin.entities.phase4

data class CheckIn(
        val check_in_comments: String,
        val room_count: String,
        val room_numbers: List<String>
)