package com.tmbooking.admin.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingEnt {

    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("rider_name")
    @Expose
    private String rider_name;
    @SerializedName("carid")
    @Expose
    private String carid;
    @SerializedName("pickup")
    @Expose
    private String pickup;
    @SerializedName("dropoff")
    @Expose
    private String dropoff;
    @SerializedName("pickuptime")
    @Expose
    private String pickuptime;
    @SerializedName("extrainfo")
    @Expose
    private String extrainfo;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("car_name")
    @Expose
    private String carName;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("status_value")
    @Expose
    private String statusValue;

    public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getDropoff() {
        return dropoff;
    }

    public void setDropoff(String dropoff) {
        this.dropoff = dropoff;
    }

    public String getPickuptime() {
        return pickuptime;
    }

    public void setPickuptime(String pickuptime) {
        this.pickuptime = pickuptime;
    }

    public String getExtrainfo() {
        return extrainfo;
    }

    public void setExtrainfo(String extrainfo) {
        this.extrainfo = extrainfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getRider_name() {
        return rider_name;
    }

    public void setRider_name(String rider_name) {
        this.rider_name = rider_name;
    }
}
