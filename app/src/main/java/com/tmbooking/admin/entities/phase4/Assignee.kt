package com.tmbooking.admin.entities.phase4

data class Assignee(
        val name: String,
        val user_id: String
)