package com.tmbooking.admin.entities.phase4

import com.google.gson.annotations.SerializedName

data class All(
        val count: Int,
        val selectedData: String
)

/*Auth Token API Result*/
data class AuthToken(
        val token: String,
        val message: String = "",
        val user_email: String,
        val user_nicename: String,
        val user_display_name: String
)

/*Login API Response*/
data class LoginResponse(
        val id: Int,
        val name: String,
        val username: String,
        val email: String,
        val message: String,
        val can_use_storecredit: String,
        var storeCredit: Int?,
        @SerializedName("_featured_image") val featuredImage: String = "",
        @SerializedName("_user_role") val user_role: String = "",
        @SerializedName("first_name") val firstName: String = "",
        @SerializedName("last_name") val lastName: String = "",
        @SerializedName("billing_address_1") val billing_address_1: String = "",
        @SerializedName("billing_address_2") val billing_address_2: String = "",
        @SerializedName("billing_city") val billing_city: String = "",
        @SerializedName("billing_company") val billing_company: String = "",
        @SerializedName("billing_country") val billing_country: String = "",
        @SerializedName("billing_first_name") val billing_first_name: String = "",
        @SerializedName("billing_last_name") val billing_last_name: String = "",
        @SerializedName("billing_postcode") val billing_postcode: String = "",
        @SerializedName("billing_state") val billing_state: String = "",
        @SerializedName("billing_email") val billing_email: String = "",
        @SerializedName("billing_phone") val billing_phone: String = ""

)

data class VisaFilter(
        val fromDate: String,
        val toDate: String,
        val orderNo: String,
        val passportNo: String,
        val country: String,
        val paymentStatus: String,
        val visaStatus: String

)

data class DriverLogList(
        val driverid: String,
        val name: String,
        val rideid: String,
        val status: String,
        val updated_at: String
)