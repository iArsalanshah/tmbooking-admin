package com.tmbooking.admin.entities.phase4

data class Notifications(
        val comments: String,
        val created_at: String,
        val guest_name: String,
        val id: String,
        val is_sent: String,
        val message: String,
        val notification_for: String,
        val notification_type: Any,
        val reference_order_id: String,
        val title: String,
        val updated_at: String,
        val user_id: String
)