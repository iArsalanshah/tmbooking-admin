package com.tmbooking.admin.entities.phase4

import java.lang.reflect.Constructor

data class Hotel(
        val agent_name: String,
        val agent_refference_number: String,
        val booking_id: String,
        val check_in_status: Boolean,
        val checkout_date: String,
        val confirmation_number: String,
        val date: String,
        val name: String,
        val number_of_guests: Int,
        val status: String,
        val supplier_code: String,
        val time: String,
        val timestamp: Int,
        val total_rooms: String,
        val type: String,
        val order_id: String

/*

*/
/*Delivery*//*

//        val booking_id: String,
        val contact_number: String,
//        val date: String,
        val delivery_id: String,
        val guest_name: String,
//        val name: String,
        val room_number: Int,
//        val status: String,
//        val time: String,
//        val timestamp: Int

*/
/*Transport*//*

//        val agent_name: String,
//        val agent_refference_number: String,
//        val booking_id: String,
//        val confirmation_number: String,
//        val date: String,
        val dropoff_point: String,
//        val name: String,
        val pickup_point: String
//        val status: String,
//        val supplier_code: String,
//        val time: String,
//        val timestamp: Int,
//        val type: String

*/

)