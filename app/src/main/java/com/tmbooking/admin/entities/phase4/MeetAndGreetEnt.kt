package com.tmbooking.admin.entities.phase4

data class MeetAndGreetEnt(
        val booking_id: String,
        val is_mandoob_meet: Any,
        val meet_comments: String,
        val meet_date_time: String,
        val meet_id: String,
        val meet_picture: List<String>
)