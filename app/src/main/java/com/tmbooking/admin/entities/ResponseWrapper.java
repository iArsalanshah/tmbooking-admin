package com.tmbooking.admin.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
// <<<<<<< HEAD

// public class ResponseWrapper<T> {

// =======

public class ResponseWrapper<T> {
// >>>>>>> 250ce023eb910c1c628fbbcd8c8bf129285ae452
    @SerializedName("reason")
    @Expose
    private String Message;
    @SerializedName("status")
    @Expose
    private String Response;
    @SerializedName("data")
    @Expose
    private T Result;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public T getResult() {
        return Result;
    }

    public void setResult(T result) {
        Result = result;
    }
}
