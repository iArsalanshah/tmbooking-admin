package com.tmbooking.admin.entities.phase4

data class ManageBooking(
        val madina: List<MakkahMadina>,
        val makkah: List<MakkahMadina>
)