package com.tmbooking.admin.entities.phase4

data class AddDelivery(
        val assignee: List<Assignee>,
        val gift_items: List<String>
)