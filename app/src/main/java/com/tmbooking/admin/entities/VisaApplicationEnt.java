package com.tmbooking.admin.entities;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisaApplicationEnt {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("visatypeid")
    @Expose
    private String visatypeid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("datetime")/*for member*/
    @Expose
    private String datetime;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("livingin")
    @Expose
    private String livingin;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("referalcode")
    @Expose
    private String referalcode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("file_url")
    @Expose
    private String fileUrl;
    @SerializedName("passport")
    @Expose
    private List<String> passport = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisatypeid() {
        return visatypeid;
    }

    public void setVisatypeid(String visatypeid) {
        this.visatypeid = visatypeid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getLivingin() {
        return livingin;
    }

    public void setLivingin(String livingin) {
        this.livingin = livingin;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getReferalcode() {
        return referalcode;
    }

    public void setReferalcode(String referalcode) {
        this.referalcode = referalcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public List<String> getPassport() {
        return passport;
    }

    public void setPassport(List<String> passport) {
        this.passport = passport;
    }


    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
