package com.tmbooking.admin.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RideListEnt {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("pickup")
    @Expose
    private String pickup;
    @SerializedName("pickuplat")
    @Expose
    private String pickuplat;
    @SerializedName("pickuplong")
    @Expose
    private String pickuplong;
    @SerializedName("dropoff")
    @Expose
    private String dropoff;
    @SerializedName("dropofflat")
    @Expose
    private String dropofflat;
    @SerializedName("dropofflong")
    @Expose
    private String dropofflong;
    @SerializedName("extrainfo")
    @Expose
    private String extrainfo;
    @SerializedName("pickuptime")
    @Expose
    private String pickuptime;
    @SerializedName("car")
    @Expose
    private String car;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getDropoff() {
        return dropoff;
    }

    public void setDropoff(String dropoff) {
        this.dropoff = dropoff;
    }

    public String getExtrainfo() {
        return extrainfo;
    }

    public void setExtrainfo(String extrainfo) {
        this.extrainfo = extrainfo;
    }

    public String getPickuptime() {
        return pickuptime;
    }

    public void setPickuptime(String pickuptime) {
        this.pickuptime = pickuptime;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPickuplat() {
        return pickuplat;
    }

    public void setPickuplat(String pickuplat) {
        this.pickuplat = pickuplat;
    }

    public String getPickuplong() {
        return pickuplong;
    }

    public void setPickuplong(String pickuplong) {
        this.pickuplong = pickuplong;
    }

    public String getDropofflat() {
        return dropofflat;
    }

    public void setDropofflat(String dropofflat) {
        this.dropofflat = dropofflat;
    }

    public String getDropofflong() {
        return dropofflong;
    }

    public void setDropofflong(String dropofflong) {
        this.dropofflong = dropofflong;
    }
}