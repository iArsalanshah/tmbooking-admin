package com.tmbooking.admin.entities.phase4

data class Transport(
        val agent_name: String,
        val agent_refference_number: String,
        val booking_id: String,
        val confirmation_number: String,
        val date: String,
        val dropoff_point: String,
        val name: String,
        val pickup_point: String,
        val status: String,
        val supplier_code: String,
        val time: String,
        val timestamp: Int,
        val type: String,
        val order_id: String

)