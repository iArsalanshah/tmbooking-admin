package com.tmbooking.admin.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.tmbooking.admin.entities.phase4.Delivery;
import com.tmbooking.admin.entities.phase4.Hotel;
import com.tmbooking.admin.entities.phase4.Transport;

public class ChildBookingItems implements Parcelable {
    Delivery delivery;
    Hotel hotel;
    Transport transport;
    String type;
    String time;
    String status;


    public ChildBookingItems() {
    }

    public ChildBookingItems(Delivery delivery, String type, String time, String status) {
        this.delivery = delivery;
        this.type = type;
        this.time = time;
        this.status = status;
    }

    public ChildBookingItems(Hotel hotel, String type, String time, String status) {
        this.hotel = hotel;
        this.type = type;
        this.time = time;
        this.status = status;
    }

    public ChildBookingItems(Transport transport, String type, String time, String status) {
        this.transport = transport;
        this.type = type;
        this.time = time;
        this.status = status;
    }

    private ChildBookingItems(Parcel in) {
        type = in.readString();
        time = in.readString();
        status = in.readString();
    }

    public static final Creator<ChildBookingItems> CREATOR = new Creator<ChildBookingItems>() {
        @Override
        public ChildBookingItems createFromParcel(Parcel in) {
            return new ChildBookingItems(in);
        }

        @Override
        public ChildBookingItems[] newArray(int size) {
            return new ChildBookingItems[size];
        }
    };

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(type);
        dest.writeString(time);
        dest.writeString(status);
    }


}
