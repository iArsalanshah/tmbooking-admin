package com.tmbooking.admin.entities.phase4

data class VisaEnt(
        val country: String,
        val formid: String,
        val fullname: String,
        val memberid: String,
        val applicationid: String,
        val orderid: String,
        val validity: String,
        val visatype: String,
        val payment_status: String,
        val date: String,
        val type: String,
        val status_name: String
)

data class VisaMemberEnt(
        val is_lead: String,
        val relation: String,
        val aqama: String,
        val bankstatement: Any,
        val cnic: String,
        val cnicphoto: Any,
        val countryofbirth: String,
        val date: String,
        val dob: String,
        val education: String,
        val email: String,
        val formid: String,
        val fullname: String,
        val gender: String,
        val healthstatus: String,
        val memberid: String,
        val id: String,
        val issuecity: String,
        val issuecountry: String,
        val livingin: Any,
        val martialstatus: String,
        val mobile: String,
        val nationality: String,
        val occupation: String,
        val passport: List<String>,
        val passportdateofissue: String,
        val passportexpiry: String,
        val photo: Any,
        val referalcode: String,
        val resident: String,
        val sirname: String,
        val socialsecuritynumber: String,
        val userid: String,
        val visatypeid: String
)

data class VisaStatusEnt(
        val countryvisatypeid: String,
        val date: String,
        val formid: String,
        val image: String,
        val memberid: String,
        val orderid: String,
        val reason: String,
        val status: String,
        val comments: String,
        val userid: String
)

data class VisaStatusListEnt(
        val id: String,
        val status: String
)
data class CountryListEnt(
        val id: String,
        val cname: String,
        val photo: String,
        val countryvisatypeinfo: List<CountryVisaTypeEnt>
)
data class CountryVisaTypeEnt(
        val id: String,
        val countryid: String,
        val visatype: String,
        val title: String,
        val duration: String,
        val price: String,
        val description: String,
        val apply: String,
        val image: String,
        val restriction: String,
        val arrival: String,
        val entries: String
)