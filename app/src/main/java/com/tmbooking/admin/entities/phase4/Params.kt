package com.tmbooking.admin.entities.phase4

data class Params(
        val delivery_id: String,
        val user_id: String
)