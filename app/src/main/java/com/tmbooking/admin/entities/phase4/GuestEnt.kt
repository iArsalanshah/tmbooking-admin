package com.tmbooking.admin.entities.phase4

data class GuestEnt(
        val guest_id: String,
        val order_id: String,
        val date_of_birth: String,
        val email: String,
        val local_contact_number: String,
        val name: String,
        val passport_expiry: String,
        val passport_number: String,
        val saudi_contact_number: String,
        val visa_number: String,
        val visa_sponser: String
)