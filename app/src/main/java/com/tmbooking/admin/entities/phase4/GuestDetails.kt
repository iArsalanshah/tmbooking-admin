package com.tmbooking.admin.entities.phase4

data class GuestDetails(
        val date_of_birth: EditHistoryObj,
        val email: EditHistoryObj,
        val order_id: String,
        val guest_id: String,
        val local_contact_number: EditHistoryObj,
        val name: EditHistoryObj,
        val passport_expiry: EditHistoryObj,
        val passport_number: EditHistoryObj,
        val saudi_contact_number: EditHistoryObj,
        val visa_number: EditHistoryObj,
        val visa_sponser: EditHistoryObj
)