package com.tmbooking.admin.entities.phase4

data class DeliveryFail(
        val code: String,
        val `data`: Data,
        val message: String
)