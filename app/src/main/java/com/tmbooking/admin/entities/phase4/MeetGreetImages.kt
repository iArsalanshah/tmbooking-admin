package com.tmbooking.admin.entities.phase4

import android.graphics.Bitmap
import java.io.File

data class MeetGreetImages(
        val isResponseImage: Boolean,
        val imageUrl: String,
        var imageBitmap: Bitmap?,
        var imageFile: File?

)