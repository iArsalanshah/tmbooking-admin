package com.tmbooking.admin.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flyco.animation.BounceEnter.BounceEnter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.tmbooking.admin.R;
import com.tmbooking.admin.expandable_recycleview.Parent;
import com.tmbooking.admin.expandable_recycleview.ParentAdapter;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.fragments.phase_4.BookingsCalendarFragment;
import com.tmbooking.admin.fragments.phase_4.visa_application.VisaApplicationTabsFragment;
import com.tmbooking.admin.fragments.phase_4.visa_management.CountryListFragment;
import com.tmbooking.admin.ui.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SideMenuFragment extends BaseFragment {

    @BindView(R.id.rvSidmenu)
    RecyclerView rvSidmenu;
    Unbinder unbinder;
    ParentAdapter adapter;

    public static SideMenuFragment newInstance() {
        return new SideMenuFragment();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sidemenu, container, false);
        unbinder = ButterKnife.bind(this, view);
        List<Parent> genres = getGenres();
        adapter = new ParentAdapter(getDockActivity(), genres);
        rvSidmenu.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvSidmenu.setAdapter(adapter);
        return view;
    }

    private List<Parent> getGenres() {
        List<Parent> mList = new ArrayList<>();

        mList.add(new Parent("Home", null, null, null));
        mList.add(new Parent("Visa Applications", null, new VisaApplicationTabsFragment(), null));
        mList.add(new Parent("Visa Management", null, new CountryListFragment(), null));

        mList.add(new Parent("Rides Management", null, new BookingFragment(), null));
        mList.add(new Parent("Booking Management", null, new BookingsCalendarFragment(), null));
        mList.add(new Parent("Driver Listing", null, new DriverListFragment(), null));

        mList.add(new Parent("Logout", null, null, showLogoutDialog()));

        return mList;
    }
    /* previous list
    private List<Parent> getGenres() {
        List<Parent> mList = new ArrayList<>();

        mList.add(new Parent("Visa Types", null));
        mList.add(new Parent("Visa Applications", null));

        *//*For drivers child*//*
        List<SideMenuChild> mChildOne = new ArrayList<>();
        mChildOne.add(new SideMenuChild("Driver List"));
        mChildOne.add(new SideMenuChild("Driver Location"));
        mChildOne.add(new SideMenuChild("Register Driver"));

        mList.add(new Parent("Drivers", mChildOne));

        *//*For booking child*//*
        List<SideMenuChild> mChildTwo = new ArrayList<>();
        mChildTwo.add(new SideMenuChild("Pending"));
        mChildTwo.add(new SideMenuChild("Active"));
        mChildTwo.add(new SideMenuChild("Cancel"));
        mChildTwo.add(new SideMenuChild("History"));

        mList.add(new Parent("Bookings", mChildTwo));

        mList.add(new Parent("Phase 4 Booking", null));


        return mList;
    }*/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        adapter.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter.onRestoreInstanceState(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private NormalDialog showLogoutDialog() {
        final NormalDialog dialog = new NormalDialog(getDockActivity());
        dialog.title(getString(R.string.logout))
                .style(NormalDialog.STYLE_TWO)
                .titleTextColor(R.color.colorAccent)
                .contentGravity(Gravity.CENTER)
                .content(getString(R.string.logout_msg))
                .btnText(getString(R.string.yes),
                        getString(R.string.no))
                .showAnim(new BounceEnter());
//                .show();

        dialog.setOnBtnClickL(new OnBtnClickL() {
            @Override
            public void onBtnClick() {
                dialog.dismiss();
                //on Logout click
                logout();
            }
        }, new OnBtnClickL() {
            @Override
            public void onBtnClick() {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    private void logout() {
        prefHelper.setLoginStatus(false);
        prefHelper.setLoginData(null);
        getDockActivity().popBackStackTillEntry(0);
        getDockActivity().replaceDockableFragment(LoginFragment.newInstance(),
                LoginFragment.class.getSimpleName());
    }
}
