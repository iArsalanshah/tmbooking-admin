package com.tmbooking.admin.fragments.phase_4.visa_application


import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.annotation.NonNull
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import com.flyco.dialog.widget.ActionSheetDialog
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.tmbooking.admin.R
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.phase4.VisaMemberEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.ImageLoaderHelper
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*


class VisaPersonDetailFormFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgProfilePicture -> {
                ImageNumber = 1
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }
            R.id.imgPassport -> {
                ImageNumber = 2
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }
            R.id.imgCNICPicture -> {
                ImageNumber = 3
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }
            R.id.btnEmailVisaCopy -> {
                openEmailDialog()

            }
            R.id.etGender -> {
                openFlycoDialog(genderList, etGender, "Select Gender")
            }
            R.id.etMaritalStatus -> {
                openFlycoDialog(maritalStatusList, etMaritalStatus, "Select Marital Status")
            }
            R.id.etHealthStatus -> {
                openFlycoDialog(healthStatusList, etHealthStatus, "Select Health Status")
            }
            R.id.etEducation -> {
                openFlycoDialog(educationList, etEducation, "Select Education")
            }
            R.id.etDOB -> {
                callDatePicker(etDOB)
            }
            R.id.etPED -> {
                callDatePicker(etPED)
            }
            R.id.etPID -> {
                callDatePicker(etPID)
            }
            R.id.etCOB -> {
                openCountryDialog(etCOB)
            }
            R.id.etResidency -> {
                openCountryDialog(etResidency)
            }
            R.id.etCOIP -> {
                openCountryDialog(etCOIP)
            }
        }
    }

    private fun openCountryDialog(edittext: AnyEditTextView) {
        builder = CountryPicker.Builder().with(dockActivity)
                .listener(OnCountryPickerListener { country ->
                    TextViewHelper.setText(edittext, country.name)
                })
        var picker = builder.build()

        picker.showDialog(dockActivity)
    }

    private val TAG: String = "VisaPersonDetailFormFragment"

    lateinit var etFullName: AnyEditTextView
    lateinit var etLastName: AnyEditTextView
    lateinit var etMobileNo: AnyEditTextView
    lateinit var etEmail: AnyEditTextView
    lateinit var etGender: AnyEditTextView
    lateinit var etMaritalStatus: AnyEditTextView
    lateinit var etDOB: AnyEditTextView
    lateinit var etCOB: AnyEditTextView
    lateinit var etCNIC: AnyEditTextView
    lateinit var etAqama: AnyEditTextView
    lateinit var etSSN: AnyEditTextView
    lateinit var etResidency: AnyEditTextView
    lateinit var etPED: AnyEditTextView
    lateinit var etPID: AnyEditTextView
    lateinit var etCOIP: AnyEditTextView
    lateinit var etCityOIP: AnyEditTextView
    lateinit var etHealthStatus: AnyEditTextView
    lateinit var etEducation: AnyEditTextView
    lateinit var etOccupation: AnyEditTextView
    lateinit var imgPassport: ImageView
    lateinit var imgPassport1: ImageView
    lateinit var imgPassport2: ImageView
    lateinit var imgCNICPicture: ImageView
    lateinit var imgProfilePicture: ImageView
    lateinit var imgPassport1Cancel: CircleImageView
    lateinit var imgPassport2Cancel: CircleImageView
    lateinit var imgProfileCancel: CircleImageView
    lateinit var btnEditApplication: Button
    lateinit var btnChangeStatus: Button
    lateinit var btnEmailVisaCopy: Button
    var memberId: Int = 0

    internal lateinit var mObj: VisaMemberEnt

    private lateinit var fileUriCNIC: Uri
    private var fileUrlCNIC: File? = null

    private lateinit var fileUriPassport: Uri
    private var fileUrlPassport: File? = null
    private lateinit var fileUriProfile: Uri
    private var fileUrlProfile: File? = null
    private var ImageNumber: Int = 0
    private var passportBitmap: Bitmap? = null
    private var pictureBitmap: Bitmap? = null
    private var CNICBitmap: Bitmap? = null

    internal var genderList = arrayOf("Male", "Female", "Others")
    internal var maritalStatusList = arrayOf("Single", "Married", "Others")
    internal var healthStatusList = arrayOf("Healthy", "Not Healthy")
    internal var educationList = arrayOf("Associate Degree", "Bachelor's Degree", "Master's Degree", "Doctoral Degree")

    var builder = CountryPicker.Builder()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_visa_person_details, container, false)
        etFullName = view.findViewById(R.id.etFullName)
        etLastName = view.findViewById(R.id.etLastName)
        etMobileNo = view.findViewById(R.id.etMobileNo)
        etEmail = view.findViewById(R.id.etEmail)
        etGender = view.findViewById(R.id.etGender)
        etMaritalStatus = view.findViewById(R.id.etMaritalStatus)
        etDOB = view.findViewById(R.id.etDOB)
        etCOB = view.findViewById(R.id.etCOB)
        etCNIC = view.findViewById(R.id.etCNIC)
        etAqama = view.findViewById(R.id.etAqama)
        etSSN = view.findViewById(R.id.etSSN)
        etResidency = view.findViewById(R.id.etResidency)
        etPED = view.findViewById(R.id.etPED)
        etPID = view.findViewById(R.id.etPID)
        etCOIP = view.findViewById(R.id.etCOIP)
        etCityOIP = view.findViewById(R.id.etCityOIP)
        etHealthStatus = view.findViewById(R.id.etHealthStatus)
        etEducation = view.findViewById(R.id.etEducation)
        etOccupation = view.findViewById(R.id.etOccupation)
        imgCNICPicture = view.findViewById(R.id.imgCNICPicture)


        imgPassport = view.findViewById(R.id.imgPassport)
        imgPassport1 = view.findViewById(R.id.imgPassport1)
        imgPassport1Cancel = view.findViewById(R.id.imgPassport1Cancel)
        imgPassport2 = view.findViewById(R.id.imgPassport2)
        imgPassport2Cancel = view.findViewById(R.id.imgPassport2Cancel)
        imgProfilePicture = view.findViewById(R.id.imgProfilePicture)
        imgProfileCancel = view.findViewById(R.id.imgProfileCancel)

        btnEditApplication = view.findViewById(R.id.btnEditApplication)
        btnChangeStatus = view.findViewById(R.id.btnChangeStatus)
        btnEmailVisaCopy = view.findViewById(R.id.btnEmailVisaCopy)
        callService()
        setListener()

        return view
    }


    private var clickOnce: Boolean = true

    private fun setListener() {
        btnEmailVisaCopy.setOnClickListener(this)
        btnChangeStatus.setOnClickListener(View.OnClickListener {
            if (mObj.formid == null) {
                UIHelper.showShortToastInCenter(dockActivity, "Form id is null")
                return@OnClickListener
            }
            var fragment = VisaChangeStatusFragment()
            fragment.formid = mObj.formid.toInt()
            dockActivity.replaceDockableFragment(fragment)
        })
        btnEditApplication.setOnClickListener {
            if (clickOnce) {
                clickOnce = false
                btnEditApplication.setText(resources.getString(R.string.save_application))
                enablefields()
            } else {
                onUpdate()
            }
        }
    }

    private fun enablefields() {
        etGender.isEnabled = true
        etMaritalStatus.isEnabled = true
        etHealthStatus.isEnabled = true
        etEducation.isEnabled = true

        etCOB.isEnabled = true
        etDOB.isEnabled = true
        etPED.isEnabled = true
        etPID.isEnabled = true
        etResidency.isEnabled = true
        etMobileNo.isEnabled = true
        etCNIC.isEnabled = true
        etAqama.isEnabled = true
        etSSN.isEnabled = true
        etCOIP.isEnabled = true
        etCityOIP.isEnabled = true
        etOccupation.isEnabled = true

        imgProfilePicture.setOnClickListener(this)
        imgPassport.setOnClickListener(this)
        imgCNICPicture.setOnClickListener(this)
        etGender.setOnClickListener(this)
        etMaritalStatus.setOnClickListener(this)
        etHealthStatus.setOnClickListener(this)
        etEducation.setOnClickListener(this)
        etCOB.setOnClickListener(this)
        etDOB.setOnClickListener(this)
        etPED.setOnClickListener(this)
        etPID.setOnClickListener(this)
        etResidency.setOnClickListener(this)
        etCOIP.setOnClickListener(this)

    }

    @RestAPI
    private fun callService() {
        serviceHelper.enqueueCall(webService.getmember(memberId),
                WebServiceConstants.getmember, true)
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stubßß
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.visa))
    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        when (Tag) {
            WebServiceConstants.getmember -> {
                var mlist = result as List<VisaMemberEnt>
                if (mlist.size > 0) {
                    mObj = mlist[0]
                    setData()
                }
            }
            WebServiceConstants.updatemember -> {
                UIHelper.showShortToastInCenter(dockActivity, message)
            }
        }

    }

    private fun setData() {
        TextViewHelper.setText(etFullName, mObj.fullname)
        TextViewHelper.setText(etLastName, mObj.sirname)
        TextViewHelper.setText(etMobileNo, mObj.mobile)
        TextViewHelper.setText(etEmail, mObj.email)
        TextViewHelper.setText(etGender, mObj.gender)
        TextViewHelper.setText(etMaritalStatus, mObj.martialstatus)
        TextViewHelper.setText(etDOB, mObj.dob)
        TextViewHelper.setText(etCOB, mObj.countryofbirth)
        TextViewHelper.setText(etCNIC, mObj.cnic)
        TextViewHelper.setText(etAqama, mObj.aqama)
        TextViewHelper.setText(etSSN, mObj.socialsecuritynumber)
        TextViewHelper.setText(etResidency, mObj.resident)
        TextViewHelper.setText(etPED, mObj.passportexpiry)
        TextViewHelper.setText(etPID, mObj.passportdateofissue)
        TextViewHelper.setText(etCOIP, mObj.issuecountry)
        TextViewHelper.setText(etCityOIP, mObj.issuecity)
        TextViewHelper.setText(etHealthStatus, mObj.healthstatus)
        TextViewHelper.setText(etEducation, mObj.education)
        TextViewHelper.setText(etOccupation, mObj.occupation)

        if (mObj.passport.size > 0)
            ImageLoaderHelper.loadImage(mObj.passport.get(0), imgPassport)
//        if (mObj.passport.size > 1)
//            ImageLoaderHelper.loadImage(mObj.passport.get(1), imgPassport2)
    }


    @SuppressLint("LongLogTag")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK && ImageNumber == 1) {
                fileUriProfile = result.uri
                fileUrlProfile = File(fileUriPassport.path)
                ImageLoaderHelper.loadImageWithPicasso(dockActivity, fileUriProfile, imgProfilePicture)

            } else if (resultCode == RESULT_OK && ImageNumber == 2) {
                fileUriPassport = result.uri
                fileUrlPassport = File(fileUriPassport.path)
                ImageLoaderHelper.loadImageWithPicasso(dockActivity, fileUriPassport, imgPassport)

            } else if (resultCode == RESULT_OK && ImageNumber == 3) {
                fileUriCNIC = result.uri
                fileUrlCNIC = File(fileUriCNIC.path)
                ImageLoaderHelper.loadImageWithPicasso(dockActivity, fileUriCNIC, imgCNICPicture)

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e(TAG, "onActivityResult: " + error.localizedMessage)
            }
        }
    }


    @RestAPI
    fun onUpdate() {
//        if (isValidate()) {
        var bodyPassport: MultipartBody.Part? = null
        if (fileUrlPassport != null) {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrlPassport)
            bodyPassport = MultipartBody.Part.createFormData("passport[0]",
                    fileUrlPassport!!.getName(), requestFile)
        } else if (passportBitmap != null) {
            val requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), getByteArray(passportBitmap!!))
            bodyPassport = MultipartBody.Part.createFormData("passport[0]", "temp_file_name1", requestFile)
        }
        var bodyPicture: MultipartBody.Part? = null
        if (fileUrlProfile != null) {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrlProfile)
            bodyPicture = MultipartBody.Part.createFormData("photo", fileUrlProfile!!.getName(), requestFile)
        } else if (pictureBitmap != null) {
            val requestFile = RequestBody.create(MediaType.parse("application/octet-stream"),
                    getByteArray(pictureBitmap!!))
            bodyPicture = MultipartBody.Part.createFormData("license_photo",
                    "photo", requestFile)
        }
        var bodyCNIC: MultipartBody.Part? = null
        if (fileUrlCNIC != null) {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrlCNIC)
            bodyPicture = MultipartBody.Part.createFormData("photo", fileUrlCNIC!!.getName(), requestFile)
        } else if (CNICBitmap != null) {
            val requestFile = RequestBody.create(MediaType.parse("application/octet-stream"),
                    getByteArray(pictureBitmap!!))
            bodyCNIC = MultipartBody.Part.createFormData("cnicphoto",
                    "photo", requestFile)
        }

        val fullName = RequestBody.create(MediaType.parse("text/plain"), etFullName.getText().toString())
        val lastName = RequestBody.create(MediaType.parse("text/plain"), etLastName.getText().toString())
        val mobile = RequestBody.create(MediaType.parse("text/plain"), etMobileNo.getText().toString())
        val gender = RequestBody.create(MediaType.parse("text/plain"), etGender.getText().toString())
        val maritalStatus = RequestBody.create(MediaType.parse("text/plain"), etMaritalStatus.getText().toString())
        val dob = RequestBody.create(MediaType.parse("text/plain"), etDOB.getText().toString())
        val nationality = RequestBody.create(MediaType.parse("text/plain"), etCOB.getText().toString())

        val cnic = RequestBody.create(MediaType.parse("text/plain"), etCNIC.getText().toString())
        val aqama = RequestBody.create(MediaType.parse("text/plain"), etAqama.getText().toString())
        val ssn = RequestBody.create(MediaType.parse("text/plain"), etSSN.getText().toString())

        val livingin = RequestBody.create(MediaType.parse("text/plain"), etResidency.getText().toString())
        val ped = RequestBody.create(MediaType.parse("text/plain"), etPED.getText().toString())
        val pid = RequestBody.create(MediaType.parse("text/plain"), etPID.getText().toString())
        val coip = RequestBody.create(MediaType.parse("text/plain"), etCOIP.getText().toString())
        val cityoip = RequestBody.create(MediaType.parse("text/plain"), etCityOIP.getText().toString())
        val healthStatus = RequestBody.create(MediaType.parse("text/plain"), etHealthStatus.getText().toString())
        val education = RequestBody.create(MediaType.parse("text/plain"), etEducation.getText().toString())
        val occupation = RequestBody.create(MediaType.parse("text/plain"), etOccupation.getText().toString())

        val memberId = RequestBody.create(MediaType.parse("text/plain"), mObj.id)


        serviceHelper.enqueueCall(webService.updatemember(fullName, lastName, mobile, gender, maritalStatus,
                dob, nationality, cnic, aqama, ssn, livingin, ped, pid, coip, cityoip, healthStatus, education, occupation,
                memberId,
                bodyPassport, bodyPicture, bodyCNIC),
                WebServiceConstants.updatemember, true)

//        } else
//            UIHelper.showShortToastInCenter(dockActivity, "Please fill all fields")
    }

    private fun getByteArray(@NonNull bm: Bitmap): ByteArray {
        var stream = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream)
        return stream.toByteArray()
    }

    /*Flyco list Dialogs*/
    private fun openFlycoDialog(mList: Array<String>, edittext: AnyEditTextView, title: String) {
        val dialog = ActionSheetDialog(dockActivity, mList, null)
        dialog.title(title)
                .titleTextSize_SP(14.5f)
                .cancelText(resources.getString(android.R.string.cancel))
                .show()
        dialog.setOnOperItemClickL { parent, view, position, id ->
            TextViewHelper.setText(edittext, mList[position])
            dialog.dismiss()
        }

    }


    private fun callDatePicker(edittext: AnyEditTextView) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in textbox
            var mMonth = monthOfYear
            mMonth++
            edittext.setText("$dayOfMonth/$mMonth/$year")

        }, year, month, day)
        dpd.show()
    }

    fun openEmailDialog() {
        val dialog = Dialog(dockActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_email_dialog)
        dialog.window.setBackgroundDrawableResource(R.drawable.bg_rounded_white)
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        val etEmail = dialog.findViewById<View>(R.id.etEmail) as AnyEditTextView
        val btnCancel = dialog.findViewById<View>(R.id.btn_cancel) as Button
        val btnSend = dialog.findViewById<View>(R.id.btn_send) as Button

        btnSend.setOnClickListener {
            if (etEmail.testValidity()) {
                dialog.dismiss()
                //call service
                getServiceHelperWP().enqueueCallWP(webServiceWP
                        .send_visa_email(mObj.formid, etEmail.text.toString()),
                        WebServiceConstants.SEND_VISA_EMAIL, true)
            }
        }
        btnCancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    override fun ResponseSuccess(result: Any?, Tag: String?) {
        UIHelper.showShortToastInCenter(dockActivity, Tag)
    }
}
