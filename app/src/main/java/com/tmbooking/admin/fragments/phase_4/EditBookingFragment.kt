package com.tmbooking.admin.fragments.phase_4

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.tmbooking.admin.R
import com.tmbooking.admin.R.id.*
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.Nothing
import com.tmbooking.admin.entities.phase4.Hotel
import com.tmbooking.admin.entities.phase4.Transport
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.TitleBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 *
 */
class EditBookingFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnCheckIn -> {
                if (hotelObj!!.check_in_status)
//                    checkInFragment.mName = AppConstants.CHECKOUT
                    checkoutAPI()
                else { /*means checkin status == null */
                    val checkInFragment = CheckInFragment()
                    checkInFragment.mName = AppConstants.CHECKIN
                    checkInFragment.hotelObj = this.hotelObj!!
                    dockActivity.replaceDockableFragment(checkInFragment)
                }

            }
            R.id.btnGuestDetails -> {
                var fragment = GuestListFragment()

                fragment.bookingOrderId = if (hotelObj == null) transportObj.order_id else hotelObj!!.order_id

                dockActivity.replaceDockableFragment(fragment)
            }
            R.id.btnAddDelivery -> {
                var fragment = AddDeliveryFragment()
                fragment.hotelObj = hotelObj!!
                dockActivity.replaceDockableFragment(fragment)

            }
            R.id.btnMeetAndGreet -> {
                var fragment = MeetAndGreetFragment()
                fragment.hotelObj = hotelObj!!
                dockActivity.replaceDockableFragment(fragment)

            }
        }
    }

    lateinit var tvHotelName: TextView
    lateinit var tvHotelDetails: TextView
    lateinit var btnCheckIn: Button
    lateinit var btnGuestDetails: Button
    lateinit var btnAddDelivery: Button
    lateinit var btnMeetAndGreet: Button
    var isHotelBooking: Boolean = false
    lateinit var transportObj: Transport
    var hotelObj: Hotel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_edit_booking, container, false)
        tvHotelName = view.findViewById(R.id.tvHotelName)
        tvHotelDetails = view.findViewById(R.id.tvHotelDetails)
        btnCheckIn = view.findViewById(R.id.btnCheckIn)
        btnGuestDetails = view.findViewById(R.id.btnGuestDetails)
        btnAddDelivery = view.findViewById(R.id.btnAddDelivery)
        btnMeetAndGreet = view.findViewById(R.id.btnMeetAndGreet)

        btnCheckIn.setOnClickListener(this)
        btnGuestDetails.setOnClickListener(this)
        btnMeetAndGreet.setOnClickListener(this)

        btnAddDelivery.setOnClickListener(this)

//        setViews()
        //ButterKnife.bind(this, view)
        setData()
        return view
    }

    private fun setViews() {
        /*For btnAddDelivery*/

        when (prefHelper.loginData.user_role) {
            AppConstants.ADMINISTRATOR -> {
                btnAddDelivery.visibility = View.VISIBLE
            }
            else -> {
                btnAddDelivery.visibility = View.GONE
            }
        }
        /*FOr btnCheckIn*/
        if (prefHelper.loginData.user_role == AppConstants.ADMINISTRATOR
                || prefHelper.loginData.user_role == AppConstants.MANDOOB_MADINA
                || prefHelper.loginData.user_role == AppConstants.MANDOOB_MAKKAH)
            btnCheckIn.visibility = View.VISIBLE
        else
            btnCheckIn.visibility = View.GONE

        /*For btnMeetAndGreet*/
        if (prefHelper.loginData.user_role == AppConstants.ADMINISTRATOR
                || prefHelper.loginData.user_role == AppConstants.MANDOOB_MADINA
                || prefHelper.loginData.user_role == AppConstants.MANDOOB_MAKKAH
                || prefHelper.loginData.user_role == AppConstants.RM_MAIN
                || prefHelper.loginData.user_role == AppConstants.RM_TRADE_PARTNER
        )
            btnMeetAndGreet.visibility = View.VISIBLE
        else
            btnMeetAndGreet.visibility = View.GONE

    }


    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.edit_booking)
    }

    private fun setData() {
        if (isHotelBooking && hotelObj != null) {
            /*btnCheckIn.visibility = View.VISIBLE
            btnGuestDetails.visibility = View.VISIBLE
            btnMeetAndGreet.visibility = View.VISIBLE*/
            setViews()

            /*  if (hotelObj!!.check_in_status)
                  btnCheckIn.text = AppConstants.CHECKOUT
              else
                  btnCheckIn.text = AppConstants.CHECKIN
              */
            if (hotelObj!!.check_in_status == null) {
                /*means show checkin*/
                btnCheckIn.text = AppConstants.CHECKIN
            } else if (hotelObj!!.check_in_status) {
                /*means show checkout*/
                btnCheckIn.text = AppConstants.CHECKOUT
            } else {
                btnCheckIn.setTextColor(resources.getColor(R.color.grey))
                btnCheckIn.isClickable = false
            }

            TextViewHelper.setText(tvHotelName, hotelObj!!.name)
            TextViewHelper.setText(tvHotelDetails, "Check-in : " + hotelObj?.date +
                    "\nCheck-out:  " + hotelObj?.checkout_date +
                    "\nNumber of Guests: " + hotelObj?.number_of_guests +
                    "\nConfirmation Number: " + hotelObj!!.confirmation_number +
                    "\nTotal Rooms: " + hotelObj?.total_rooms +
                    "\nRoom Type: " + hotelObj?.type +
                    "\nAgent Name: " + hotelObj?.agent_name +
                    "\nAgent Reference Number: " + hotelObj?.agent_refference_number +
                    "\nSupplier Code: " + hotelObj?.supplier_code)
        } else if (transportObj != null) {
            btnCheckIn.visibility = View.GONE
            btnAddDelivery.visibility = View.GONE
            btnMeetAndGreet.visibility = View.GONE
            btnGuestDetails.visibility = View.VISIBLE

            TextViewHelper.setText(tvHotelName, transportObj.name)
            TextViewHelper.setText(tvHotelDetails, "Pickup : " + transportObj.pickup_point +
                    "\nDrop-off:  " + transportObj.dropoff_point +
                    "\nStatus: " + transportObj.status +
                    "\nConfirmation Number: " + transportObj.confirmation_number +
                    "\nCar Type: " + transportObj.type +
                    "\nAgent Name: " + transportObj.agent_name +
                    "\nAgent Reference Number: " + transportObj.agent_refference_number +
                    "\nSupplier Code: " + transportObj.supplier_code)
        }
    }

    @RestAPI
    private fun checkoutAPI() {
        loadingStarted()
        getWebServiceWP().checkout(hotelObj!!.booking_id).enqueue(object : Callback<List<Nothing>> {
            override fun onResponse(call: Call<List<Nothing>>, response: Response<List<Nothing>>) {
                loadingFinished()
                UIHelper.showShortToastInCenter(dockActivity, "Checkout Succesfully")
                dockActivity.popFragment()
                TODO("have to update makkah and madina list")

            }

            override fun onFailure(call: Call<List<Nothing>>, t: Throwable) {
                loadingFinished()
                UIHelper.showShortToastInCenter(dockActivity, t.localizedMessage)
            }
        })
    }


}
