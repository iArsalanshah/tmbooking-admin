package com.tmbooking.admin.fragments.phase_4.visa_application


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.VisaEnt
import com.tmbooking.admin.entities.phase4.VisaMemberEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.SimpleDividerItemDecoration
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.VisaApplicationPerson
import com.tmbooking.admin.ui.views.TitleBar

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class VisaApplicationDetailFragment : BaseFragment() {

    lateinit var tvOrderNo: TextView
    lateinit var tvCountry: TextView
    lateinit var tvType: TextView
    lateinit var tvStatus: TextView
    lateinit var tvDataOfEntry: TextView


    lateinit var rvVisaAppliedPeople: RecyclerView
    lateinit var adapter: VisaApplicationPerson
    lateinit var visaObj: VisaEnt
    private lateinit var mList: List<VisaMemberEnt>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_visa_application_detail, container, false)

        tvOrderNo = view.findViewById(R.id.tvOrderNo)
        tvStatus = view.findViewById(R.id.tvStatus)
        tvCountry = view.findViewById(R.id.tvCountry)
        tvType = view.findViewById(R.id.tvType)
        tvDataOfEntry = view.findViewById(R.id.tvDataOfEntry)

        rvVisaAppliedPeople = view.findViewById(R.id.rvVisaAppliedPeople)

        initAdapter()
        callService()
        setData()
        return view
    }

    private fun callService() {
        serviceHelper.enqueueCall(webService.getgroupmembers(visaObj.applicationid.toInt()),
                WebServiceConstants.getgroupmembers, true)
    }

    private fun setData() {
        TextViewHelper.setHtmlText(tvOrderNo, "Order No. ", visaObj.orderid)
        TextViewHelper.setHtmlText(tvCountry, "Country: ", visaObj.country)
        TextViewHelper.setHtmlText(tvType, "Type: ", visaObj.visatype)
        TextViewHelper.setHtmlText(tvStatus, "Payment Status: ", visaObj.payment_status)
        TextViewHelper.setHtmlText(tvDataOfEntry, "Date of entry: ", visaObj.date)

    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.visa_application))
    }

    private fun initAdapter() {
        /* adapter = BookingAdapter(getDockActivity(),
                 OnListItemClick { position, obj ->
                     val fragment = BookingDetailsFormFragment()
                     fragment.setObjectData(obj, mFragmentName)
                     dockActivity.addDockableFragment(fragment, BookingDetailsFormFragment()::class.java.canonicalName)
                 })*/
        adapter = VisaApplicationPerson(dockActivity)
        rvVisaAppliedPeople.layoutManager = LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false)
        rvVisaAppliedPeople.addItemDecoration(SimpleDividerItemDecoration(dockActivity))
        rvVisaAppliedPeople.adapter = adapter

        /*   val mList = ArrayList<String>()
           mList.add("")
           mList.add("")
           mList.add("")
           mList.add("")
           mList.add("")
           mList.add("")
           adapter.addAll(mList)*/
    }


    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        mList = result as List<VisaMemberEnt>
        adapter.addAll(mList)
    }
}
