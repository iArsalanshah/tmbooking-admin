package com.tmbooking.admin.fragments.phase_4.visa_management


import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.dmallcott.dismissibleimageview.DismissibleImageView
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.tmbooking.admin.R
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.phase4.CountryListEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.ImageLoaderHelper
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CountryFormFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgAttachCountry -> {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }
            R.id.btnSubmit -> {
                if (etCountryName.testValidity()) {
                    onAddUpdate()
                }
            }
        }
    }

    @RestAPI
    private fun onAddUpdate() {
        var bodyCountry: MultipartBody.Part? = null
        if (fileUrlCountry != null) {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrlCountry)
            bodyCountry = MultipartBody.Part.createFormData("photo",
                    fileUrlCountry!!.getName(), requestFile)
        }
        val CountryName = RequestBody.create(MediaType.parse("text/plain"), etCountryName.text.toString())

        if (countryDetails == null) {
            //add
            serviceHelper.enqueueCall(webService.addcountry(CountryName, bodyCountry),
                    WebServiceConstants.addcountry, true)
        } else //update
        {
            val countryId = RequestBody.create(MediaType.parse("text/plain"), countryDetails!!.id)

            serviceHelper.enqueueCall(webService.updatecountry(countryId, CountryName, bodyCountry),
                    WebServiceConstants.updatecountry, true)
        }
    }

    private val TAG: String = "CountryFormFragment"

    lateinit var imgAttachCountry: ImageView
    lateinit var imgCountryPicture: DismissibleImageView
    lateinit var etCountryName: AnyEditTextView
    lateinit var btnSubmit: Button

    private lateinit var fileUriCountry: Uri
    private var fileUrlCountry: File? = null
    var countryDetails: CountryListEnt? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_country_form, container, false)
        imgAttachCountry = view.findViewById(R.id.imgAttachCountry)
        imgCountryPicture = view.findViewById(R.id.imgCountryPicture)
        etCountryName = view.findViewById(R.id.etCountryName)
        btnSubmit = view.findViewById(R.id.btnSubmit)

        imgAttachCountry.setOnClickListener(this)
        btnSubmit.setOnClickListener(this)
        if (countryDetails != null) //true == edit
        {
            setData()
        }
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        if (countryDetails != null)
            titleBar.subHeading = countryDetails!!.cname
        else
            titleBar.subHeading = ""

    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        UIHelper.showShortToastInCenter(dockActivity, message)
        dockActivity.popFragment()
    }

    private fun setData() {
        btnSubmit.text = resources.getString(R.string.update)
        TextViewHelper.setText(etCountryName, countryDetails!!.cname)
        ImageLoaderHelper.loadImage(countryDetails!!.photo, imgCountryPicture)
    }

    @SuppressLint("LongLogTag")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                fileUriCountry = result.uri
                fileUrlCountry = File(fileUriCountry.path)
                ImageLoaderHelper.loadImageWithPicasso(dockActivity, fileUriCountry, imgCountryPicture)

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e(TAG, "onActivityResult: " + error.localizedMessage)
            }
        }
    }
}
