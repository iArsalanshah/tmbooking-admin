package com.tmbooking.admin.fragments


import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.tmbooking.admin.R
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.DriverEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.ImageLoaderHelper
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import com.tmbooking.admin.ui.views.Util
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */

class DriverDetailFormFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.DriverPicture -> {
                isDriverImage = true

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }
            R.id.imglicensePhoto -> {
                isDriverImage = false

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }
            R.id.etJoiningDate -> {
                isLicenseExpiry = false
                DatePickerDialog(dockActivity, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show()
            }
            R.id.etLicenseExpiry -> {
                isLicenseExpiry = true
                DatePickerDialog(dockActivity, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show()
            }
            /* R.id.btnRight -> { *//*Title bar right button*//*
                when (titleBar!!.rightButtonText) {
                    "Edit" -> onEdit()
                    "Update" ->
                        if (isRegistration)
//                            onAdd()
                        else
                            onUpdate()
                }
            }*/
            R.id.btnBlock -> {
                var msg: String = ""

                if (btnBlock.text == "Block") msg = "Block this driver?" else msg = "Un-Block this driver?"

                showYesNoDialog("Confirmation", msg, true)
            }
            R.id.btnDelete -> {
                showYesNoDialog("Alert", "Are you sure to remove this record?", false)

            }
        }
    }

    internal lateinit var driverEnt: DriverEnt
    internal var isRegistration: Boolean = false
    internal var isDriverImage: Boolean = false
    internal var isLicenseExpiry: Boolean = false

    private var TAG: String = DriverDetailFormFragment::class.java.canonicalName

    internal var titleBar: TitleBar? = null


    lateinit var driverPicture: ImageView
    lateinit var imglicensePhoto: ImageView
    lateinit var etDriverName: AnyEditTextView
    lateinit var etEmail: AnyEditTextView
    lateinit var etPassword: AnyEditTextView
    lateinit var etIkama: AnyEditTextView
    lateinit var etNationality: AnyEditTextView
    lateinit var etKafilName: AnyEditTextView
    lateinit var etEmergenyContact: AnyEditTextView
    lateinit var etAge: AnyEditTextView
    lateinit var etLicenseNumber: AnyEditTextView
    lateinit var etLicenseExpiry: AnyEditTextView
    lateinit var etRegistrationNumber: AnyEditTextView
    lateinit var etCar: AnyEditTextView
    lateinit var etAddress: AnyEditTextView
    lateinit var etCity: AnyEditTextView
    lateinit var etState: AnyEditTextView
    lateinit var etZipCode: AnyEditTextView
    lateinit var etHomePhone: AnyEditTextView
    lateinit var etWorkPhone: AnyEditTextView
    lateinit var etJoiningDate: AnyEditTextView

    lateinit var btnBlock: Button
    lateinit var btnDelete: Button

    private lateinit var fileUriDriver: Uri
    private var fileUrlDriver: File? = null

    private lateinit var fileUriPassport: Uri
    private var fileUrlPassport: File? = null

    private var myCalendar = Calendar.getInstance()
    lateinit var date: DatePickerDialog.OnDateSetListener

    private var driverPictureBitmap: Bitmap? = null
    private var driverPassportBitmap: Bitmap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_driver_detail_form, container, false)
        driverPicture = view.findViewById(R.id.DriverPicture)
        imglicensePhoto = view.findViewById(R.id.imglicensePhoto)
        etDriverName = view.findViewById(R.id.etDriverName)
        etEmail = view.findViewById(R.id.etEmail)
        etPassword = view.findViewById(R.id.etPassword)
        etIkama = view.findViewById(R.id.etIkama)
        etNationality = view.findViewById(R.id.etNationality)
        etKafilName = view.findViewById(R.id.etKafilName)
        etEmergenyContact = view.findViewById(R.id.etEmergenyContact)
        etAge = view.findViewById(R.id.etAge)
        etLicenseNumber = view.findViewById(R.id.etLicenseNumber)
        etLicenseExpiry = view.findViewById(R.id.etLicenseExpiry)
        etRegistrationNumber = view.findViewById(R.id.etRegistrationNumber)
        etCar = view.findViewById(R.id.etCar)
        etAddress = view.findViewById(R.id.etAddress)
        etCity = view.findViewById(R.id.etCity)
        etState = view.findViewById(R.id.etState)
        etZipCode = view.findViewById(R.id.etZipCode)
        etHomePhone = view.findViewById(R.id.etHomePhone)
        etWorkPhone = view.findViewById(R.id.etWorkPhone)
        etJoiningDate = view.findViewById(R.id.etJoiningDate)

        btnBlock = view.findViewById(R.id.btnBlock)
        btnDelete = view.findViewById(R.id.btnDelete)

        driverPicture.setOnClickListener(this)
        imglicensePhoto.setOnClickListener(this)
        etLicenseExpiry.setOnClickListener(this)
        etJoiningDate.setOnClickListener(this)

        btnBlock.setOnClickListener(this)
        btnDelete.setOnClickListener(this)


        date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        setViews()
        if (!isRegistration) {
            setData()
        }

        return view
    }

    private fun setViews() {
        if (isRegistration) {
            btnBlock.visibility = View.GONE
            btnDelete.visibility = View.GONE
            onEdit()
        } else {
            btnBlock.visibility = View.VISIBLE
            btnDelete.visibility = View.VISIBLE
        }
    }

    override fun setTitleBar(_titleBar: TitleBar) {
        super.setTitleBar(_titleBar)
        titleBar = _titleBar
        titleBar!!.hideButtons()
        titleBar!!.showBackButton()
        titleBar!!.subHeading = resources.getString(R.string.driver_detail)
        if (isRegistration) {
            titleBar!!.setRightBtnHeading(resources.getString(R.string.update))
        } else
            titleBar!!.setRightBtnHeading(resources.getString(R.string.edit))
        titleBar!!.setRightBtnListener {
            when (titleBar!!.rightButtonText) {
                "Edit" -> onEdit()
                "Update" -> {
                    /* if (isRegistration)
                 //                            onAdd()
                                     else*/
                    onUpdate()
                }
            }
        }
    }

    private fun updateLabel() {
        val myFormat = "MM/dd/yy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        if (isLicenseExpiry)
            etLicenseExpiry.setText(sdf.format(myCalendar.getTime()))
        else
            etJoiningDate.setText(sdf.format(myCalendar.getTime()))
    }

    fun setIsRegistration(_isRegistration: Boolean) {
        isRegistration = _isRegistration
    }

    fun setObjectData(driverEnt: DriverEnt) {
        this.driverEnt = driverEnt
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                if (isDriverImage) {
                    fileUriDriver = result.uri
                    fileUrlDriver = File(fileUriDriver.path)
                    ImageLoaderHelper.loadImageWithPicasso(dockActivity, fileUriDriver, driverPicture)
                } else {
                    fileUriPassport = result.uri
                    fileUrlPassport = File(fileUriPassport.path)
                    ImageLoaderHelper.loadImageWithPicasso(dockActivity, fileUriPassport, imglicensePhoto)
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e(TAG, "onActivityResult: " + error.localizedMessage)
            }
        }
    }

    private fun getByteArray(@NonNull bm: Bitmap): ByteArray {
        var stream = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream)
        return stream.toByteArray()
    }

    @RestAPI
    fun onUpdate() {
        if (isValidate()) {
            var bodyDriver: MultipartBody.Part? = null
            if (fileUrlDriver != null) {
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrlDriver)
                bodyDriver = MultipartBody.Part.createFormData("drivers_photo",
                        fileUrlDriver!!.getName(), requestFile)
            } else if (driverPictureBitmap != null) {
                val requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), getByteArray(driverPictureBitmap!!))
                bodyDriver = MultipartBody.Part.createFormData("drivers_photo", "temp_file_name1", requestFile)
            }
            var bodyPassport: MultipartBody.Part? = null
            if (fileUrlPassport != null) {
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrlPassport)
                bodyPassport = MultipartBody.Part.createFormData("license_photo", fileUrlPassport!!.getName(), requestFile)
            } else if (driverPassportBitmap != null) {
                val requestFile = RequestBody.create(MediaType.parse("application/octet-stream"),
                        getByteArray(driverPassportBitmap!!))
                bodyPassport = MultipartBody.Part.createFormData("license_photo",
                        "temp_file_name2", requestFile)
            }

            val driverName = RequestBody.create(MediaType.parse("text/plain"), etDriverName.getText().toString())
            val email = RequestBody.create(MediaType.parse("text/plain"), etEmail.getText().toString())
            val password = RequestBody.create(MediaType.parse("text/plain"), etPassword.getText().toString())
            val ikama = RequestBody.create(MediaType.parse("text/plain"), etIkama.getText().toString())
            val nationality = RequestBody.create(MediaType.parse("text/plain"), etNationality.getText().toString())
            val kafilName = RequestBody.create(MediaType.parse("text/plain"), etKafilName.getText().toString())
            val emergenyContact = RequestBody.create(MediaType.parse("text/plain"), etEmergenyContact.getText().toString())
            val age = RequestBody.create(MediaType.parse("text/plain"), etAge.getText().toString())
            val licenseNumber = RequestBody.create(MediaType.parse("text/plain"), etLicenseNumber.getText().toString())
            val licenseExpiry = RequestBody.create(MediaType.parse("text/plain"), etLicenseExpiry.getText().toString())
            val registrationNumber = RequestBody.create(MediaType.parse("text/plain"), etRegistrationNumber.getText().toString())
            val car = RequestBody.create(MediaType.parse("text/plain"), etCar.getText().toString())
            val address = RequestBody.create(MediaType.parse("text/plain"), etAddress.getText().toString())
            val city = RequestBody.create(MediaType.parse("text/plain"), etCity.getText().toString())
            val state = RequestBody.create(MediaType.parse("text/plain"), etState.getText().toString())
            val zipCode = RequestBody.create(MediaType.parse("text/plain"), etZipCode.getText().toString())
            val homePhone = RequestBody.create(MediaType.parse("text/plain"), etHomePhone.getText().toString())
            val workPhone = RequestBody.create(MediaType.parse("text/plain"), etWorkPhone.getText().toString())
            val joiningDate = RequestBody.create(MediaType.parse("text/plain"), etJoiningDate.getText().toString())
            val id = RequestBody.create(MediaType.parse("text/plain"), "")


            serviceHelper.enqueueCall(webService.driverregistration(bodyDriver, bodyPassport, driverName, email,
                    password, ikama, nationality, kafilName, emergenyContact, age, licenseNumber
                    , licenseExpiry, registrationNumber, car, address
                    , city, state, zipCode, homePhone, workPhone, joiningDate, id),
                    WebServiceConstants.driverregistration, true)

        } else
            UIHelper.showShortToastInCenter(dockActivity, "Please fill all fields")
    }

    private fun onEdit() {
        if (titleBar != null)
            titleBar!!.setRightBtnHeading(resources.getString(R.string.update))

        driverPicture.isClickable = true
        imglicensePhoto.isClickable = true
        etDriverName.isEnabled = true
        etEmail.isEnabled = true
        etPassword.isEnabled = true
        etIkama.isEnabled = true
        etNationality.isEnabled = true
        etKafilName.isEnabled = true
        etEmergenyContact.isEnabled = true
        etAge.isEnabled = true
        etLicenseNumber.isEnabled = true
        etLicenseExpiry.isEnabled = true
        etRegistrationNumber.isEnabled = true
        etCar.isEnabled = true
        etAddress.isEnabled = true
        etCity.isEnabled = true
        etState.isEnabled = true
        etZipCode.isEnabled = true
        etHomePhone.isEnabled = true
        etWorkPhone.isEnabled = true
        etJoiningDate.isEnabled = true
    }

    private fun setData() {
        // Load image, decode it to Bitmap and return Bitmap to callback

        ImageLoaderHelper.loadImage(driverEnt.getDriversPhoto(), driverPicture)
        setBitmap(true, driverEnt.getDriversPhoto())
        ImageLoaderHelper.loadImage(driverEnt.getLicensePhoto(), imglicensePhoto)
        setBitmap(false, driverEnt.getLicensePhoto())

        TextViewHelper.setText(etDriverName, driverEnt.getName())
        TextViewHelper.setText(etEmail, driverEnt.getEmail())
        TextViewHelper.setText(etPassword, driverEnt.getPassword())
        TextViewHelper.setText(etIkama, driverEnt.getIkama())
        TextViewHelper.setText(etNationality, driverEnt.getNationality())
        TextViewHelper.setText(etKafilName, driverEnt.getKafilName())
        TextViewHelper.setText(etEmergenyContact, driverEnt.getEmergencyContact())
        TextViewHelper.setText(etAge, driverEnt.getAge())
        TextViewHelper.setText(etLicenseNumber, driverEnt.getLicenceNo())
        TextViewHelper.setText(etLicenseExpiry, driverEnt.getLicenseExpiry())
        TextViewHelper.setText(etRegistrationNumber, driverEnt.getRegisterationNumnber())
        TextViewHelper.setText(etCar, driverEnt.getAssignedCar())
        TextViewHelper.setText(etAddress, driverEnt.getAddress())
        TextViewHelper.setText(etCity, driverEnt.getCity())
        TextViewHelper.setText(etState, driverEnt.getState())
        TextViewHelper.setText(etZipCode, driverEnt.getZipcode())
        TextViewHelper.setText(etHomePhone, driverEnt.getHomePhone())
        TextViewHelper.setText(etWorkPhone, driverEnt.getWorkPhone())
        TextViewHelper.setText(etJoiningDate, driverEnt.getJoinning())

        if (1 == Util.getParsedInteger(driverEnt.getStatus())) /*means thisdriver is not block*/
            btnBlock.setText("Block")
        else if (2 == Util.getParsedInteger(driverEnt.getStatus())) /*means thisdriver is block*/
            btnBlock.setText("UnBlock")

    }

    private fun isValidate(): Boolean {
        return ((fileUrlDriver != null || driverPictureBitmap != null)
                && (fileUrlPassport != null || driverPassportBitmap != null)
                && etDriverName.testValidity()
                && etEmail.testValidity()
                && etPassword.testValidity()
                && etIkama.testValidity()
                && etNationality.testValidity()
                && etKafilName.testValidity()

                && etEmergenyContact.testValidity()
                && etAge.testValidity()
                && etLicenseNumber.testValidity()
                && etLicenseExpiry.testValidity()
                && etRegistrationNumber.testValidity()
                && etCar.testValidity()
                && etAddress.testValidity()
                && etCity.testValidity()
                && etState.testValidity()
                && etZipCode.testValidity()
                && etHomePhone.testValidity()
                && etWorkPhone.testValidity()
                && etJoiningDate.testValidity()
                )
    }

    private fun setBitmap(isPicture: Boolean, imgUrl: String?) {
        ImageLoader.getInstance().loadImage(imgUrl, object : SimpleImageLoadingListener() {
            override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
                if (isPicture)
                    driverPictureBitmap = loadedImage
                else driverPassportBitmap = loadedImage
            }
        })
    }

    private fun isPictureBitmap(img: ImageView): Boolean {

        return false
    }

    private fun showYesNoDialog(title: String, message: String, isBlock: Boolean) {
        val builder = AlertDialog.Builder(dockActivity)

        builder.setTitle(title)

        builder.setMessage(message)
        builder.setPositiveButton("Yes") { dialog, which ->
            //            Hit api for Yes
            if (isBlock)
                serviceHelper.enqueueCall(webService.block_unblockdriver(driverEnt.getDriverid(),
                        if (Util.getParsedInteger(driverEnt.getStatus()) == 1) 2 else 1),
                        WebServiceConstants.block_unblockdriver, true)
            else
                serviceHelper.enqueueCall(webService.removedriver(driverEnt.getDriverid()),
                        WebServiceConstants.removedriver, true)

            dialog.dismiss()

        }
        builder.setNegativeButton("No") { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    override fun ResponseSuccess(result: Any?, Tag: String, message: String) {
        when (Tag) {
            WebServiceConstants.driverregistration -> {
                UIHelper.showShortToastInCenter(dockActivity, "Record updated successfully")
                dockActivity.popFragment()
            }
            WebServiceConstants.block_unblockdriver -> {
                /* var driverObj = result as DriverEnt
                 if (1 == Util.getParsedInteger(driverObj.getStatus())) *//*means thisdriver is not block*//*
                    btnBlock.setText("Block")
                else if (2 == Util.getParsedInteger(driverObj.getStatus())) *//*means thisdriver is block*//*
                    btnBlock.setText("UnBlock")
                */
                if (btnBlock.text == "Block") btnBlock.text = "UnBlock" else btnBlock.text = "Block"

                UIHelper.showShortToastInCenter(dockActivity, "Record updated successfully")
            }
            WebServiceConstants.removedriver -> {
                UIHelper.showShortToastInCenter(dockActivity, "Record removed successfully")
                dockActivity.popFragment()
            }
        }
    }
}
