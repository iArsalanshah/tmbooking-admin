package com.tmbooking.admin.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.UserEnt;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.helpers.ImageLoaderHelper;
import com.tmbooking.admin.ui.views.TitleBar;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends BaseFragment {


    private static final String TAG = MyProfileFragment.class.getSimpleName();
    @BindView(R.id.userPicture)
    CircleImageView userPicture;
    @BindView(R.id.etUserName)
    TextInputEditText etUserName;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etPhoneNumber)
    TextInputEditText etPhoneNumber;
    @BindView(R.id.etAddress)
    TextInputEditText etAddress;
    Unbinder unbinder;
    private Uri fileUri;
    private File fileUrl;


    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (prefHelper.getUser() != null) {
            setData();
        }
    }

    private void setData() {
        UserEnt loginData = prefHelper.getUser();
        etUserName.setText(loginData.getName());
        etEmail.setText(loginData.getEmail());
        etPhoneNumber.setText(loginData.getHomePhone());
        etAddress.setText(loginData.getAddress());
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
//        titleBar.showHomeButton();
        titleBar.setSubHeading(getResources().getString(R.string.my_profile));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.userPicture, R.id.btnsaveChanges})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.userPicture:
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(getDockActivity(), this);
                break;
            case R.id.btnsaveChanges:
                notImplemented();
//                callService(etUserName.getText().toString(), etUsername.getText().toString(),
//                        etPhoneNumber.getText().toString(), etAddress.getText().toString());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                fileUri = result.getUri();
                fileUrl = new File(fileUri.getPath());
                ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), fileUri, userPicture);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e(TAG, "onActivityResult: " + error.getLocalizedMessage());
            }
        }
    }


    private void callService(String _fullName, String _email, String _mobileNo,
                             String _location) {
        MultipartBody.Part body = null;
        if (fileUrl != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrl);
            body = MultipartBody.Part.createFormData("profile_picture", fileUrl.getName(), requestFile);
        }

        RequestBody full_name = RequestBody.create(MediaType.parse("text/plain"), _fullName);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), _email);
        RequestBody mobile_no = RequestBody.create(MediaType.parse("text/plain"), _mobileNo);
        RequestBody location = RequestBody.create(MediaType.parse("text/plain"), _location);

        serviceHelper.enqueueCall(webService.updateProfile(body, full_name, email,
                mobile_no, location), WebServiceConstants.updateProfile, true);
    }

    @OnClick(R.id.tvLogout)
    public void onViewClicked() {
        final Dialog dialog = new Dialog(getDockActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_logout_dialog);
        dialog.getWindow().setWindowAnimations(R.style.DialogBounceTheme);

        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                prefHelper.setLoginStatus(false);
                getDockActivity().popBackStackTillEntry(0);
                getDockActivity().replaceDockableFragment(new LoginFragment());
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        dialog.show();
    }
}
