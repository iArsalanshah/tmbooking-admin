package com.tmbooking.admin.fragments.phase_4.visa_application


import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.flyco.dialog.widget.ActionSheetDialog
import com.tmbooking.admin.R
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.phase4.CountryListEnt
import com.tmbooking.admin.entities.phase4.VisaFilter
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class VisaFilterFragment : BaseFragment(), View.OnClickListener {

    private val mStatusList: Array<String?> = arrayOf("Pending", "Approved", "Reject")
    private val mPaymentStatusList: Array<String?> = arrayOf("Pending Payment", "Processing", "On-Hold", "Cancelled", "Confirmed")

    override fun onClick(v: View) {
        when (v.id) {

            R.id.etCountry -> {
                openFlycoDialog(mCountryList, etCountry, "Select Country")
            }
            R.id.etPaymentStatus -> {
                openFlycoDialog(mPaymentStatusList, etPaymentStatus, "Select Payment Status")
            }
            R.id.etVisaStatus -> {
                openFlycoDialog(mStatusList, etVisaStatus, "Select Visa Status")
            }
            R.id.etDateFrom -> {
                callDatePicker(etDateFrom)
            }
            R.id.etDateTo -> {
                callDatePicker(etDateTo)
            }
            R.id.btnSearch -> {
                returnWithData()
            }

        }
    }

    /*Flyco list Dialogs*/
    private fun openFlycoDialog(mList: Array<String?>, edittext: AnyEditTextView, title: String) {
        val dialog = ActionSheetDialog(dockActivity, mList, null)
        dialog.title(title)
                .titleTextSize_SP(14.5f)
                .cancelText(resources.getString(android.R.string.cancel))
                .show()
        dialog.setOnOperItemClickL { parent, view, position, id ->
            TextViewHelper.setText(edittext, mList[position])
            dialog.dismiss()
        }

    }

    private fun callDatePicker(edittext: AnyEditTextView) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in textbox
            var mMonth = monthOfYear
            mMonth++
            edittext.setText("$dayOfMonth/$mMonth/$year")

        }, year, month, day)
        dpd.show()
    }


    lateinit var etDateFrom: AnyEditTextView
    lateinit var etDateTo: AnyEditTextView
    lateinit var etOrderNo: AnyEditTextView
    lateinit var etPassportNo: AnyEditTextView
    lateinit var etCountry: AnyEditTextView
    lateinit var etPaymentStatus: AnyEditTextView
    lateinit var etVisaStatus: AnyEditTextView
    lateinit var btnSearch: Button
    private var mCountryList = arrayOfNulls<String>(100)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_visa_filter, container, false)

        etDateFrom = view.findViewById(R.id.etDateFrom)
        etDateTo = view.findViewById(R.id.etDateTo)
        etOrderNo = view.findViewById(R.id.etOrderNo)
        etPassportNo = view.findViewById(R.id.etPassportNo)
        etCountry = view.findViewById(R.id.etCountry)
        etPaymentStatus = view.findViewById(R.id.etPaymentStatus)
        etVisaStatus = view.findViewById(R.id.etVisaStatus)
        btnSearch = view.findViewById(R.id.btnSearch)
        callGetService()
        setListener()
        return view
    }

    private fun setListener() {
        etDateFrom.setOnClickListener(this)
        etDateTo.setOnClickListener(this)
        etCountry.setOnClickListener(this)
        etPaymentStatus.setOnClickListener(this)
        etVisaStatus.setOnClickListener(this)
        btnSearch.setOnClickListener(this)
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stubßß
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.filters))
    }

    @RestAPI
    private fun callGetService() {
        serviceHelper.enqueueCall(webService.countrylist(),
                WebServiceConstants.countrylist, true)
    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        when (Tag) {
            WebServiceConstants.countrylist -> {
                var mList = result as List<CountryListEnt>
                for (i in 0 until mList.size) {
                    mCountryList[i] = mList[i].cname
                }
            }
        }
    }

    fun returnWithData() {
        if (checkValidate()) {
            var filterObj = VisaFilter(etDateFrom.text.toString(), etDateTo.text.toString(), etOrderNo.text.toString(),
                    etPassportNo.text.toString(), etCountry.text.toString(), etPaymentStatus.text.toString(), etVisaStatus.text.toString())


            (getBaseTargetFragment() as? VisaApplicationTabsFragment)?.setFilterData(filterObj)
            dockActivity.supportFragmentManager.popBackStackImmediate()


        }

    }

    private fun checkValidate(): Boolean {
        return (etDateFrom.testValidity() || etDateTo.testValidity() || etOrderNo.testValidity() ||
                etPassportNo.testValidity() || etCountry.testValidity() || etPaymentStatus.testValidity() || etVisaStatus.testValidity())
    }
}
