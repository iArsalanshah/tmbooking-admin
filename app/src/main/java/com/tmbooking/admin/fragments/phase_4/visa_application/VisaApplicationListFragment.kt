package com.tmbooking.admin.fragments.phase_4.visa_application


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fueled.flowr.annotations.DeepLink
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.phase4.VisaEnt
import com.tmbooking.admin.entities.phase4.VisaFilter
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.interfaces.OnVisaItemClick
import com.tmbooking.admin.ui.adapters.VisaApplicationListAdapter
import com.tmbooking.admin.ui.views.TitleBar
import java.util.*

public const val SET_NAME_REQUEST_CODE = 101

/**
 * A simple [Fragment] subclass.
 *
 */
@DeepLink(value = "/first")
class VisaApplicationListFragment : BaseFragment(), OnVisaItemClick {
    override fun onClick(position: Int, obj: VisaEnt) {
        //OnVisa Item Click
        when (mFragmentName) {
            AppConstants.GROUP -> {
                var fragment = VisaApplicationDetailFragment()
                fragment.visaObj = obj
                (context as DockActivity).replaceDockableFragment(fragment)
            }
            AppConstants.INDIVIDUAL -> {
                var fragment = VisaPersonDetailFormFragment()
                fragment.memberId = obj.memberid.toInt()
                (context as DockActivity).replaceDockableFragment(fragment)
            }
        }
    }


    lateinit var tvRecord: TextView
    lateinit var rvVisa: RecyclerView
    lateinit var adapter: VisaApplicationListAdapter
    private var mIndividualList: MutableList<VisaEnt> = ArrayList()
    private var mGroupList: MutableList<VisaEnt> = ArrayList()


    internal var mFragmentName: String = ""
    internal lateinit var titleBar: TitleBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(com.tmbooking.admin.R.layout.fragment_visa_application_list, container, false)
        tvRecord = view.findViewById(com.tmbooking.admin.R.id.tvRecord)

        rvVisa = view.findViewById(com.tmbooking.admin.R.id.rvVisa)
        initAdapter()
        callService()

        return view
    }

    @RestAPI
     fun callService() {
        serviceHelper.enqueueCall(webService.getallvisas("", "", "", "", "", "", ""),
                WebServiceConstants.getAllVisaApplications, true)
    }


    private fun setData() {
        when (mFragmentName) {
            AppConstants.INDIVIDUAL ->
                adapter.addAll(mIndividualList)
            AppConstants.GROUP ->
                adapter.addAll(mGroupList)

        }

    }

/*
    public fun launchFilterFragment() {
        val newFragment = VisaFilterFragment()
        val tag = VisaFilterFragment::class.java.simpleName

        newFragment.setTargetFragment(this, SET_NAME_REQUEST_CODE)
        dockActivity?.replaceDockableFragment2(newFragment, tag)
    }*/

    private fun initAdapter() {
        /* adapter = BookingAdapter(getDockActivity(),
                 OnListItemClick { position, obj ->
                     val fragment = BookingDetailsFormFragment()
                     fragment.setObjectData(obj, mFragmentName)
                     dockActivity.addDockableFragment(fragment, BookingDetailsFormFragment()::class.java.canonicalName)
                 })*/
        adapter = VisaApplicationListAdapter(dockActivity, mFragmentName, this)
        rvVisa.layoutManager = LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false)
        rvVisa.adapter = adapter
//
//        val mList = ArrayList<String>()
//        mList.add("")
//        mList.add("")
//        mList.add("")
//        mList.add("")
//        mList.add("")
//        mList.add("")
//        adapter.addAll(mList)
    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        val mList = result as List<VisaEnt>
        if (mList.size > 0) {
            tvRecord.visibility = View.GONE
        } else
            tvRecord.visibility = View.VISIBLE

        mIndividualList.clear()
        mGroupList.clear()
        for (item in mList) {
            if (item.type == AppConstants.INDIVIDUAL)
                mIndividualList.add(item)
            else
                mGroupList.add(item)

        }
        setData()

    }

    private var mFilterData: VisaFilter? = null

    fun setFilterData(data: VisaFilter) {
        mFilterData = data
        if (mFilterData != null) {
//            tvClearFilter.visibility = View.VISIBLE
            serviceHelper.enqueueCall(webService.getallvisas(mFilterData!!.fromDate, mFilterData!!.toDate, mFilterData!!.orderNo,
                    mFilterData!!.passportNo, mFilterData!!.country,
                    mFilterData!!.paymentStatus, mFilterData!!.visaStatus),
                    WebServiceConstants.getAllVisaApplications, true)
//            serviceHelper.enqueueCall(webService.getvisas("Pending"),
//                    WebServiceConstants.getAllVisaApplications, true)


        }
    }

    override fun onResume() {
        super.onResume()
        /*   if (mFilterData != null) {
               tvClearFilter.visibility = View.VISIBLE
   //            serviceHelper.enqueueCall(webService.getallvisas(mFilterData!!.fromDate, mFilterData!!.toDate, mFilterData!!.orderNo,
   //                    mFilterData!!.passportNo, mFilterData!!.country,
   //                    mFilterData!!.paymentStatus, mFilterData!!.visaStatus),
   //                    WebServiceConstants.getAllVisaApplications, true)
               serviceHelper.enqueueCall(webService.getvisas("Pending"),
                       WebServiceConstants.getAllVisaApplications, true)


           }*/
    }

}
