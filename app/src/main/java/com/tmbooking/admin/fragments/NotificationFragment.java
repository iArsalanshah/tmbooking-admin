package com.tmbooking.admin.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.phase4.Notifications;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.AppConstants;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.ui.adapters.NotificationsAdapter;
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.tmbooking.admin.ui.views.TitleBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment {


    @BindView(R.id.rvNotification)
    RecyclerView rvNotification;
    Unbinder unbinder;
    RecyclerViewListAdapter adapter;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        unbinder = ButterKnife.bind(this, view);
        initAdapter();
        callService();
        return view;
    }

    private void callService() {
        String notificationFor = "";
        if (prefHelper.getLoginData().getUser_role().equals(AppConstants.ADMINISTRATOR)) {
            notificationFor = "admin";
        }
        /*else if (prefHelper.getLoginData().getUser_role().equals(AppConstants.PRIVLEDGED_MEMBER) ||
                prefHelper.getLoginData().getUser_role().equals(AppConstants.TRADE_PARTNER)) {
            notificationFor = "customer";
        }*/
        if (!notificationFor.isEmpty())
            getServiceHelperWP().enqueueCallWP(getWebServiceWP()
                            .getUserNotifications(prefHelper.getLoginData().getId(), notificationFor),
                    WebServiceConstants.GET_USER_NOTIFICATIONS, true);

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.notification));
    }

    private void initAdapter() {
        adapter = new NotificationsAdapter(getDockActivity());
        rvNotification.setLayoutManager(new LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false));
      /*  List<String> list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");
        list.add("");
        adapter.addAll(list);*/
        rvNotification.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        List<Notifications> mlist = (List<Notifications>) result;
        if (mlist != null && mlist.size() > 0) {
            adapter.addAll(mlist);
        }
    }
}
