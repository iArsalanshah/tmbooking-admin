package com.tmbooking.admin.fragments.phase_4


import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.EditHistory
import com.tmbooking.admin.entities.phase4.GuestDetails
import com.tmbooking.admin.entities.phase4.GuestEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import java.util.*

/**
 * A simple [Fragment] subclass.
 *
 */
class GuestDetailsFragment : BaseFragment(), View.OnClickListener {
    private var newFragment: EditHistoryFragment = EditHistoryFragment()

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvGuestName -> {
                showFragmentDialog(editGuestDetail.name.edit_history, "Guest Name")

            }
            R.id.tvPassportNumber -> {
                showFragmentDialog(editGuestDetail.passport_number.edit_history, "Passport Number")
            }
            R.id.tvPassportExpiry -> {
                showFragmentDialog(editGuestDetail.passport_expiry.edit_history, "Passport Expiry")
            }
            R.id.tvVisaNumber -> {
                showFragmentDialog(editGuestDetail.visa_number.edit_history, "Visa Number")
            }
            R.id.tvLocalContactNumber -> {
                showFragmentDialog(editGuestDetail.local_contact_number.edit_history, "Local Contact Number")
            }
            R.id.tvSaudiContactNumber -> {
                showFragmentDialog(editGuestDetail.saudi_contact_number.edit_history, "Saudi Contact Number")
            }
            R.id.tvDateOfBirth -> {
                showFragmentDialog(editGuestDetail.date_of_birth.edit_history, "Date Of Birth")
            }
            R.id.tvEmailAddress -> {
                showFragmentDialog(editGuestDetail.email.edit_history, "Email Address")
            }
            R.id.tvVisaSponsor -> {
                showFragmentDialog(editGuestDetail.visa_sponser.edit_history, "Visa Sponsor")
            }


            R.id.etPassportExpiry -> {
                openCalendar(etPassportExpiry)
            }
            R.id.etDateOfBirth -> {
                openCalendar(etDateOfBirth)
            }
            R.id.btnSave -> {
                if (clickOnce) {
                    clickOnce = false
                    btnSave.text = resources.getString(R.string.save)
                    openClicks()
                } else {
                    btnSave.isEnabled = false
                    postCallService()
//                    UIHelper.showShortToastInCenter(dockActivity, "Successfully Submit!")
//                    dockActivity.popFragment()
                }
            }
        }

    }

    private fun showFragmentDialog(editHistoryList: List<EditHistory>, text: String) {
        if (editHistoryList.size > 0) {
            newFragment.mContext = dockActivity
            newFragment.fragmentContext = this
            newFragment.name = text
            newFragment.mEditHistory = editHistoryList
            newFragment.isCancelable = false


            newFragment.show(dockActivity.fragmentManager, "dialog")
        } else
            UIHelper.showShortToastInCenter(dockActivity, "No history found")
    }

    lateinit var etGuestName: AnyEditTextView
    lateinit var etPassportNumber: AnyEditTextView
    lateinit var etPassportExpiry: AnyEditTextView
    lateinit var etVisaNumber: AnyEditTextView
    lateinit var etLocalContactNumber: AnyEditTextView
    lateinit var etSaudiContactNumber: AnyEditTextView
    lateinit var etDateOfBirth: AnyEditTextView
    lateinit var etEmailAddress: AnyEditTextView
    lateinit var etVisaSponsor: AnyEditTextView

    lateinit var tvGuestName: TextView
    lateinit var tvPassportNumber: TextView
    lateinit var tvPassportExpiry: TextView
    lateinit var tvVisaNumber: TextView
    lateinit var tvLocalContactNumber: TextView
    lateinit var tvSaudiContactNumber: TextView
    lateinit var tvDateOfBirth: TextView
    lateinit var tvEmailAddress: TextView
    lateinit var tvVisaSponsor: TextView

    lateinit var btnSave: Button
    lateinit var guestDetails: GuestEnt
    var clickOnce: Boolean = true
    private lateinit var editGuestDetail: GuestDetails


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_guest_details, container, false)
        etGuestName = view.findViewById(R.id.etGuestName)
        etPassportNumber = view.findViewById(R.id.etPassportNumber)
        etPassportExpiry = view.findViewById(R.id.etPassportExpiry)
        etVisaNumber = view.findViewById(R.id.etVisaNumber)
        etLocalContactNumber = view.findViewById(R.id.etLocalContactNumber)
        etSaudiContactNumber = view.findViewById(R.id.etSaudiContactNumber)
        etDateOfBirth = view.findViewById(R.id.etDateOfBirth)
        etEmailAddress = view.findViewById(R.id.etEmailAddress)
        etVisaSponsor = view.findViewById(R.id.etVisaSponsor)


        tvGuestName = view.findViewById(R.id.tvGuestName)
        tvPassportNumber = view.findViewById(R.id.tvPassportNumber)
        tvPassportExpiry = view.findViewById(R.id.tvPassportExpiry)
        tvVisaNumber = view.findViewById(R.id.tvVisaNumber)
        tvLocalContactNumber = view.findViewById(R.id.tvLocalContactNumber)
        tvSaudiContactNumber = view.findViewById(R.id.tvSaudiContactNumber)
        tvDateOfBirth = view.findViewById(R.id.tvDateOfBirth)
        tvEmailAddress = view.findViewById(R.id.tvEmailAddress)
        tvVisaSponsor = view.findViewById(R.id.tvVisaSponsor)
        btnSave = view.findViewById(R.id.btnSave)

        setViews()
        getCallService()

        return view
    }

    private fun setViews() {
        if (prefHelper.loginData.user_role == AppConstants.ADMINISTRATOR
                || prefHelper.loginData.user_role == AppConstants.RM_MAIN
                || prefHelper.loginData.user_role == AppConstants.PRIVLEDGED_MEMBER
                || prefHelper.loginData.user_role == AppConstants.TRADE_PARTNER
        )
            btnSave.visibility = View.VISIBLE
        else
            btnSave.visibility = View.GONE

    }

    private fun openClicks() {
        if (prefHelper.loginData.user_role == AppConstants.ADMINISTRATOR
                || prefHelper.loginData.user_role == AppConstants.PRIVLEDGED_MEMBER /*Customer*/
                || prefHelper.loginData.user_role == AppConstants.TRADE_PARTNER /*Customer*/) {
//        etGuestName.isEnabled = true  /*not allowed*/
            etPassportNumber.isEnabled = true
            etPassportExpiry.isEnabled = true
            etVisaNumber.isEnabled = true
            etLocalContactNumber.isEnabled = true
            etSaudiContactNumber.isEnabled = true
            etDateOfBirth.isEnabled = true
//        etEmailAddress.isEnabled = true /*not allowed*/
            etVisaSponsor.isEnabled = true
        } else if (prefHelper.loginData.user_role == AppConstants.RM_MAIN) { /*Rm Main can only edit contact no*/
            etLocalContactNumber.isEnabled = true
        }
    }

    private fun openCalendar(edittext: AnyEditTextView) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in textbox
            var mMonth = monthOfYear
            mMonth++
            edittext.setText("" + dayOfMonth + "-" + mMonth + "-" + year)

        }, year, month, day)
        dpd.show()
    }

    fun dismissDialog() {
        newFragment.dismiss()
    }

    private fun setData(guestDetails: GuestDetails) {
        btnSave.text = resources.getString(R.string.edit)
        if (guestDetails != null) {
            TextViewHelper.setText(etGuestName, guestDetails.name.current_value)
            TextViewHelper.setText(etPassportNumber, guestDetails.passport_number.current_value)
            TextViewHelper.setText(etPassportNumber, guestDetails.passport_number.current_value)
            TextViewHelper.setText(etPassportExpiry, guestDetails.passport_expiry.current_value)
            TextViewHelper.setText(etVisaNumber, guestDetails.visa_number.current_value)
            TextViewHelper.setText(etLocalContactNumber, guestDetails.local_contact_number.current_value)
            TextViewHelper.setText(etSaudiContactNumber, guestDetails.saudi_contact_number.current_value)
            TextViewHelper.setText(etDateOfBirth, guestDetails.date_of_birth.current_value)
            TextViewHelper.setText(etEmailAddress, guestDetails.email.current_value)
            TextViewHelper.setText(etVisaSponsor, guestDetails.visa_sponser.current_value)
        }
    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.guest_details)
    }


    private fun postCallService() {
        if (etGuestName.testValidity()) {
            getServiceHelperWP().enqueueCallWP(webServiceWP
                    .update_guest(
                            guestDetails.guest_id,
                            guestDetails.order_id,
                            etDateOfBirth.text.toString(),
                            etEmailAddress.text.toString(),
                            etLocalContactNumber.text.toString(),
                            etGuestName.text.toString(),
                            etPassportExpiry.text.toString(),
                            etPassportNumber.text.toString(),
                            etSaudiContactNumber.text.toString(),
                            etVisaNumber.text.toString(),
                            etVisaSponsor.text.toString()
                    ), WebServiceConstants.UPDATE_GUEST, true)
        } else {
            UIHelper.showShortToastInCenter(dockActivity, "Please enter guest name")
        }
    }

    private fun getCallService() {

        getServiceHelperWP().enqueueCallWP(webServiceWP
                .guest_detail(
                        guestDetails.order_id,
                        guestDetails.guest_id,
                        prefHelper.loginData.id/*"1"*/), WebServiceConstants.GUEST_DETAIL, true)

    }


    override fun ResponseSuccess(result: Any?, Tag: String?) {
        when (Tag) {
            WebServiceConstants.UPDATE_GUEST -> {
                btnSave.isEnabled = true

                var guestList = result as GuestEnt
                if (guestList != null) {
                    UIHelper.showShortToastInCenter(dockActivity, "Guest details updated successfully!")
                    dockActivity.popFragment()
                }
            }
            WebServiceConstants.GUEST_DETAIL -> {
                editGuestDetail = result as GuestDetails
                setListener()
                setData(editGuestDetail)

            }
        }


    }

    private fun setListener() {
        if (prefHelper.loginData.user_role != AppConstants.MANDOOB_DELIVERY) {
            tvGuestName.setOnClickListener(this)
            tvPassportNumber.setOnClickListener(this)
            tvPassportExpiry.setOnClickListener(this)
            tvVisaNumber.setOnClickListener(this)
            tvLocalContactNumber.setOnClickListener(this)
            tvSaudiContactNumber.setOnClickListener(this)
            tvDateOfBirth.setOnClickListener(this)
            tvEmailAddress.setOnClickListener(this)
            tvVisaSponsor.setOnClickListener(this)
            etPassportExpiry.setOnClickListener(this)
            etDateOfBirth.setOnClickListener(this)
            btnSave.setOnClickListener(this)
        }
    }

    override fun ResponseFailure(tag: String?) {
        btnSave.isEnabled = true
    }


}
