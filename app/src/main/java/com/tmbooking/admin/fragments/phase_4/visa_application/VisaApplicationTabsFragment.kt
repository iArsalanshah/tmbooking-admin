package com.tmbooking.admin.fragments.phase_4.visa_application


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.VisaFilter
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.ui.views.TitleBar


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class VisaApplicationTabsFragment : BaseFragment() {

    lateinit var tabLayout: TabLayout
    lateinit var vpVisa: ViewPager
    private lateinit var adapter: ViewPagerAdapter
    lateinit var tvClearFilter: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_visa_application_tabs, container, false)
        /*init view*/
        tabLayout = view.findViewById(R.id.tabs)
        vpVisa = view.findViewById(R.id.vpVisa)
        tvClearFilter = view.findViewById(com.tmbooking.admin.R.id.tvClearFilter)

        /*=========*/

        setupViewPager(vpVisa)
        tabLayout.setupWithViewPager(vpVisa)
        setTabsLayout()

        tvClearFilter.setOnClickListener {
            tvClearFilter.visibility = View.GONE
            var fragment = adapter.getItem(0) as VisaApplicationListFragment
            fragment.callService()
            var fragment2 = adapter.getItem(1) as VisaApplicationListFragment
            fragment2.callService()
        }

        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.visa_applications))
        titleBar.setAddButton {
            //Filter button
            if (checkLoading()) {
//                var firstFragment = adapter.getItem(0) as VisaApplicationListFragment
//                firstFragment.launchFilterFragment()
                launchFilterFragment()
            }
        }
        titleBar.showFilterButton()
    }

    private fun launchFilterFragment() {
        val newFragment = VisaFilterFragment()
        val tag = VisaFilterFragment::class.java.simpleName

        newFragment.setTargetFragment(this, SET_NAME_REQUEST_CODE)
        dockActivity?.addDockableFragment(newFragment, tag)
    }

    private fun setTabsLayout() {
        /* val textView1 = LayoutInflater.from(getDockActivity()).inflate(R.layout.custom_tab, null) as TextView
         val textView2 = LayoutInflater.from(getDockActivity()).inflate(R.layout.custom_tab, null) as TextView

         textView1.text = resources.getString(R.string.pending)
         textView2.text = resources.getString(R.string.confirmed)
         textView1.gravity = Gravity.CENTER_HORIZONTAL
         textView2.gravity = Gravity.CENTER_HORIZONTAL
         tabLayout.getTabAt(0)!!.customView = textView1
         tabLayout.getTabAt(1)!!.customView = textView2 //?. --> only safe , !!. --> not null
 */

        for (i in 0 until tabLayout.tabCount) {
            val tab = tabLayout.getTabAt(i)

            val tabTextView = LayoutInflater.from(getDockActivity()).inflate(R.layout.custom_tab2, null) as TextView
            tabTextView.text = tab!!.text
            tabTextView.layoutParams = TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            tab.customView = tabTextView
//            tab.select()
        }


    }


    private fun setupViewPager(viewpager: ViewPager) {
        adapter = ViewPagerAdapter(childFragmentManager)
        var individualFragment = VisaApplicationListFragment()
        individualFragment.mFragmentName = AppConstants.INDIVIDUAL
        adapter.addFragment(individualFragment, resources.getString(R.string.individuals))

        var groupFragment = VisaApplicationListFragment()
        groupFragment.mFragmentName = AppConstants.GROUP
        adapter.addFragment(groupFragment, resources.getString(R.string.groups))

        viewpager.adapter = adapter
    }


    internal inner class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment? {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }


        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    fun setFilterData(data: VisaFilter) {
//        mFilterData = data
        tvClearFilter.visibility = View.VISIBLE
        var fragment = adapter.getItem(0) as VisaApplicationListFragment
        fragment.setFilterData(data!!)
        var fragment2 = adapter.getItem(1) as VisaApplicationListFragment
        fragment2.setFilterData(data!!)
    }

}
