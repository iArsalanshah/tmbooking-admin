package com.tmbooking.admin.fragments.phase_4


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.DriverLogList
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.ui.adapters.DriverLogListAdapter
import com.tmbooking.admin.ui.views.TitleBar

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DriverLogListFragment : BaseFragment() {
    lateinit var rvDriverList: RecyclerView
    lateinit var tvAlternateText: TextView
    lateinit var id: String
    private lateinit var adapter: DriverLogListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_driver_log_list, container, false)
        rvDriverList = view.findViewById(R.id.rvDriverList)
        tvAlternateText = view.findViewById(R.id.tvAlternateText)
        init()
        callService()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.assigned_driver)
    }

    private fun init() {
        adapter = DriverLogListAdapter(dockActivity)
        rvDriverList.layoutManager = LinearLayoutManager(dockActivity, LinearLayoutManager.VERTICAL, false)
        rvDriverList.adapter = adapter
/*
        val mList = ArrayList<String>()
        mList.add("")
        mList.add("")
        mList.add("")
        mList.add("")
        adapter.addAll(mList)*/
    }


    private fun callService() {
        serviceHelper.enqueueCall(webService
                .getassigneddrivers(id), WebServiceConstants.GET_ASSIGNED_DRIVERS, true)
    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        var driverList = result as List<DriverLogList>
        if (driverList.size > 0) {
            tvAlternateText.visibility = View.GONE
            adapter.addAll(driverList)
        } else
            tvAlternateText.visibility = View.VISIBLE
    }


}
