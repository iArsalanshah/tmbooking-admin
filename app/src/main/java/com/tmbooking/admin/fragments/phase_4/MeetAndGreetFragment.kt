package com.tmbooking.admin.fragments.phase_4


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView

import com.tmbooking.admin.R
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.phase4.Hotel
import com.tmbooking.admin.entities.phase4.MeetAndGreetEnt
import com.tmbooking.admin.entities.phase4.MeetGreetImages
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.CameraHelperNew
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.interfaces.RemovePicture
import com.tmbooking.admin.ui.adapters.PictureListAdapter
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL

/**
 * A simple [Fragment] subclass.
 *
 */
class MeetAndGreetFragment : BaseFragment(), View.OnClickListener, RemovePicture {
    override fun onRemove(position: Int, item: MeetGreetImages) {
        if (adapter.itemCount >= 1) {
            adapter.removeItem(item)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnAddPicture -> {
                if (adapter.itemCount != 3)
                    CameraHelperNew.openCameraIntent(dockActivity, this)
                else
                    UIHelper.showShortToastInCenter(dockActivity, "You can not add more than 3 pictures")
            }
            R.id.btnSubmit -> {
                if (clickOnce) {
                    clickOnce = false
                    setButton(true, resources.getString(R.string.submit))
                } else {
                    call()
//                    var byteArray = getLogoImage(adapter.getItem(0).imageUrl)
//                    byteArrayToBitmap(byteArray!!)
                }
            }
        }

    }

    /*Class started here*/
    lateinit var cbYes: CheckBox
    lateinit var cbNo: CheckBox
    lateinit var etOtherComment: AnyEditTextView
    lateinit var btnAddPicture: Button
    lateinit var rvPictures: RecyclerView
    lateinit var btnSubmit: Button
    lateinit var imageView1: ImageView
    lateinit var adapter: RecyclerViewListAdapter<MeetGreetImages>
    //    lateinit var mlist: ArrayList<MeetGreetImages>
    lateinit var hotelObj: Hotel
    var clickOnce: Boolean = true
    var meetId: String? = null


//    lateinit var mlist: ArrayList<DynamicallyCapturePictures>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_meet_and_greet, container, false)
        cbYes = view.findViewById(R.id.cbYes)
        cbNo = view.findViewById(R.id.cbNo)
        etOtherComment = view.findViewById(R.id.etOtherComment)
        imageView1 = view.findViewById(R.id.imageView1)
        btnAddPicture = view.findViewById(R.id.btnAddPicture)
        rvPictures = view.findViewById(R.id.rvPictures)
        btnSubmit = view.findViewById(R.id.btnSubmit)

        btnAddPicture.setOnClickListener(this)

//        mlist = ArrayList()
        init()
        setView()
        callService()
        return view
    }

    private fun setView() {
        if (prefHelper.loginData.user_role == AppConstants.ADMINISTRATOR
                || prefHelper.loginData.user_role == AppConstants.MANDOOB_MADINA
                || prefHelper.loginData.user_role == AppConstants.MANDOOB_MAKKAH)
            btnSubmit.visibility = View.VISIBLE
        else
            btnSubmit.visibility = View.GONE
    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.meet_and_greet)
    }

    private fun init() {
        adapter = PictureListAdapter(dockActivity, this)
        rvPictures.layoutManager = LinearLayoutManager(dockActivity, LinearLayoutManager.HORIZONTAL, false)
        rvPictures.adapter = adapter

        val list = ArrayList<MeetGreetImages>()
        adapter.addAll(list)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            adapter.addItem(MeetGreetImages(false, "file:///" + CameraHelperNew.getImageFilePath(), null,
                    File(CameraHelperNew.getImageFilePath())/*CameraHelperNew.getPhotoFile()*/))
            /*mlist.add(MeetGreetImages(false, "file:///" + CameraHelperNew.getImageFilePath(), null,
                    File(CameraHelperNew.getImageFilePath())*//*CameraHelperNew.getPhotoFile()*//*))

            adapter.addAll(mlist)*/

        }
    }

    @RestAPI
    private fun callService() {
        getServiceHelperWP().enqueueCallWP(webServiceWP
                .getMeetgreet(hotelObj.booking_id), WebServiceConstants.GET_MEET_GREET, true)
    }

    /* @RestAPI
     private fun callPostService() {
         var map: HashMap<String, RequestBody> = HashMap()
         if (isValidate()) {
             for (i in 0 until mlist.size) {
                 //current time in milliSeconds
                 val time = System.currentTimeMillis()
                 var requestFile: RequestBody
                 if (!mlist[i].isResponseImage) { */
    /*Means camera capture picture*//*
                    // create RequestBody instance from file
                    requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mlist[i].imageFile)
                    //add requested file in Map object
                    map.put("meet_picture[]\"; filename=\"" + time + "_IMG.jpg\"", requestFile);

                } else if (mlist[i].imageBitmap != null) { *//*response image*//*
                    requestFile = RequestBody.create(MediaType.parse("application/octet-stream"),
                            getByteArray(mlist[i].imageBitmap!!))
                    //add requested file in Map object
                    map.put("meet_picture[]\"; filename=\"" + time + "_IMG.jpg\"", requestFile)

                }

                val booking_id = RequestBody.create(MediaType.parse("text/plain"), hotelObj.booking_id)
                val meet_id = RequestBody.create(MediaType.parse("text/plain"), meetId)
                val timeString = RequestBody.create(MediaType.parse("text/plain"), System.currentTimeMillis().toString())
                val text = RequestBody.create(MediaType.parse("text/plain"), etOtherComment.text.toString())

                *//*  getServiceHelperWP().enqueueCallWP(webServiceWP
                          .postMeetgreet(booking_id, meet_id, timeString
                                  , text, map), WebServiceConstants.POST_MEET_GREET, true)*//*
*//*
                getServiceHelperWP().enqueueCallWP(webServiceWP
                        .postMeetgreet(hotelObj.booking_id, meetId, System.currentTimeMillis().toString()
                                , etOtherComment.text.toString(), map), WebServiceConstants.POST_MEET_GREET, true)
*//*

            }
        }
    }*/

    @RestAPI
    private fun call() {
        if (isValidate()) {
            val booking_id = RequestBody.create(MediaType.parse("text/plain"), hotelObj.booking_id)
            val meet_id = RequestBody.create(MediaType.parse("text/plain"), meetId)
            val is_mandoob_meet = RequestBody.create(MediaType.parse("text/plain"), getMandoobMeetId())
            val timeString = RequestBody.create(MediaType.parse("text/plain"), System.currentTimeMillis().toString())
            val text = RequestBody.create(MediaType.parse("text/plain"), etOtherComment.text.toString())

            getServiceHelperWP().enqueueCallWP(webServiceWP
                    .postMeetgreet(booking_id, meet_id, is_mandoob_meet, timeString
                            , text, getPartsArray()), WebServiceConstants.POST_MEET_GREET, true)
        }
    }

    private fun getPartsArray(): List<MultipartBody.Part> {
        var parts = ArrayList<MultipartBody.Part>()

        for (i in 0 until adapter.itemCount) {
            if (adapter.getItem(i).isResponseImage && adapter.getItem(i).imageBitmap != null)
                parts.add(prepareFilePart("meet_picture[]", adapter.getItem(i).imageBitmap!!))
            if (!adapter.getItem(i).isResponseImage && adapter.getItem(i).imageFile != null)
                parts.add(prepareFilePart("meet_picture[]", adapter.getItem(i).imageFile!!))

        }
        return parts
    }


    private fun getMandoobMeetId(): String {
        if (cbYes.isChecked)
            return "1"
        if (cbNo.isChecked)
            return "0"
        return "0"
    }

    private fun prepareFilePart(partName: String, file: File): MultipartBody.Part {
        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        return MultipartBody.Part.createFormData(partName, file.getName(), requestBody)
    }

    private fun prepareFilePart(partName: String, bitmap: Bitmap): MultipartBody.Part {
        val requestBody = RequestBody.create(MediaType.parse("application/octet-stream"), getByteArray(bitmap))
//        val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), getByteArray(bitmap))
        return MultipartBody.Part.createFormData(partName, System.currentTimeMillis().toString(), requestBody)
    }

    private fun getByteArray(@NonNull bm: Bitmap): ByteArray {
        var stream = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 10, stream)
        return stream.toByteArray()
    }

    override fun ResponseSuccess(result: Any?, Tag: String?) {
        when (Tag) {
            WebServiceConstants.GET_MEET_GREET -> {
                var meetAndGreetEnt = result as MeetAndGreetEnt
                if (meetAndGreetEnt != null && meetAndGreetEnt.meet_id != "0") {
                    /*Means has edit before = true*/
                    setData(meetAndGreetEnt)
                    setButton(false, resources.getString(R.string.edit))
                } else
                    setButton(true, resources.getString(R.string.submit))

            }
            WebServiceConstants.POST_MEET_GREET -> {
                UIHelper.showShortToastInCenter(dockActivity, "Submit successfully")
                dockActivity.popFragment()
            }
        }


    }

    private fun isValidate(): Boolean {
        if (cbYes.isChecked) {
            if (adapter.itemCount == 0) {
                UIHelper.showShortToastInCenter(dockActivity, "Please take at least 1 picture")
                return false
            }
        } else if (cbNo.isChecked) {
            if (!etOtherComment.testValidity()) {
                return false
            }
        } else if (!(cbYes.isChecked && cbNo.isChecked)) {
            UIHelper.showShortToastInCenter(dockActivity, "Please select at least one check box!")

            return false
        }
        return true
    }

    private fun setButton(isEnabled: Boolean, string: String) {
        btnSubmit.text = string
        /*do enable false*/
        cbYes.isEnabled = isEnabled
        cbNo.isEnabled = isEnabled
        etOtherComment.isEnabled = isEnabled
        btnAddPicture.isEnabled = isEnabled

        btnSubmit.setOnClickListener(this)
    }

    private fun setData(meetAndGreetEnt: MeetAndGreetEnt) {
        meetId = meetAndGreetEnt.meet_id
        if (meetAndGreetEnt.is_mandoob_meet != null && meetAndGreetEnt.is_mandoob_meet == "1")
            cbYes.isChecked = true
        else if (meetAndGreetEnt.is_mandoob_meet != null && meetAndGreetEnt.meet_id == "0")
            cbNo.isChecked = true

        TextViewHelper.setText(etOtherComment, meetAndGreetEnt.meet_comments)

        for (i in 0 until meetAndGreetEnt.meet_picture.size) {
            adapter.addItem(MeetGreetImages(true, meetAndGreetEnt.meet_picture[i]
                    , null, null))
        }
    }


    private fun byteArrayToBitmap(byteArray: ByteArray) {
        var bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)

        imageView1.setImageBitmap(Bitmap.createScaledBitmap(bmp, imageView1.getWidth(),
                imageView1.getHeight(), false))
    }

    private fun getLogoImage(url: String): ByteArray? {

        try {

            val imageUrl = URL(url)
            val ucon = imageUrl.openConnection()

            val _is = ucon.getInputStream()

            val baos = ByteArrayOutputStream()
            val buffer = ByteArray(1024)
            var read = 0

//            while ((read = _is.read(buffer, 0, buffer.size)) != -1) {
            baos.write(buffer, 0, read)
//            }

            return baos.toByteArray()

        } catch (e: Exception) {
            Log.d("ImageManager", "Error: " + e.toString())
        }

        return null
    }

}
