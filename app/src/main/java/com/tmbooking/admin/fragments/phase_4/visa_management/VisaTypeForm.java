package com.tmbooking.admin.fragments.phase_4.visa_management; /*updated*/


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tmbooking.admin.R;
import com.tmbooking.admin.annotation.RestAPI;
import com.tmbooking.admin.entities.phase4.CountryVisaTypeEnt;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.helpers.ImageLoaderHelper;
import com.tmbooking.admin.helpers.TextViewHelper;
import com.tmbooking.admin.helpers.UIHelper;
import com.tmbooking.admin.ui.views.AnyEditTextView;
import com.tmbooking.admin.ui.views.TitleBar;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisaTypeForm extends BaseFragment {

    final private String TAG = VisaTypeForm.class.getSimpleName();

    @BindView(R.id.userPicture)
    ImageView userPicture;
    @BindView(R.id.etVisaType)
    AnyEditTextView etVisaType;
    @BindView(R.id.etTitle)
    AnyEditTextView etTitle;
    @BindView(R.id.etDuration)
    AnyEditTextView etDuration;
    @BindView(R.id.etPrice)
    AnyEditTextView etPrice;
    @BindView(R.id.etDescription)
    AnyEditTextView etDescription;
    @BindView(R.id.etHowToApply)
    AnyEditTextView etHowToApply;
    @BindView(R.id.etRestriction)
    AnyEditTextView etRestriction;
    @BindView(R.id.etArrival)
    AnyEditTextView etArrival;
    Unbinder unbinder;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    private Uri fileUri;
    private File fileUrl;

    CountryVisaTypeEnt mVisaTypeObj;

    TitleBar titleBar = null;
    String mCountryId = "";

    public VisaTypeForm() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_visa_type_form, container, false);
        unbinder = ButterKnife.bind(this, view);

        onEdit();
        if (mVisaTypeObj != null) {
            btnSubmit.setText(getResources().getString(R.string.update));
            setData();
        }

        return view;
    }


    public CountryVisaTypeEnt getmVisaTypeObj() {
        return mVisaTypeObj;
    } /*updated*/

    public void setmVisaTypeObj(CountryVisaTypeEnt mVisaTypeObj) { /*updated*/
        this.mVisaTypeObj = mVisaTypeObj;
    }

    private void setData() {
        ImageLoaderHelper.loadImage(mVisaTypeObj.getImage(), userPicture);
        TextViewHelper.setText(etVisaType, mVisaTypeObj.getVisatype());
        TextViewHelper.setText(etTitle, mVisaTypeObj.getTitle());
        TextViewHelper.setText(etDuration, mVisaTypeObj.getDuration());
        TextViewHelper.setText(etPrice, mVisaTypeObj.getPrice());
        TextViewHelper.setText(etDescription, mVisaTypeObj.getDescription());
        TextViewHelper.setText(etHowToApply, mVisaTypeObj.getApply());
        TextViewHelper.setText(etRestriction, mVisaTypeObj.getRestriction());
        TextViewHelper.setText(etArrival, mVisaTypeObj.getArrival());
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        this.titleBar = titleBar;
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.visa_type));
//        if (mAddVisa) {
//            titleBar.setRightBtnHeading(getResources().getString(R.string.update));
//        } else
//            titleBar.setRightBtnHeading(getResources().getString(R.string.edit));
//        titleBar.setRightBtnListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.userPicture, R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.userPicture:
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(getDockActivity(), this);
                break;
            case R.id.btnSubmit:
                if (mVisaTypeObj == null)
                    onAdd();
                else
                    onUpdate();
//                showYesNoDialog("Alert", "Are you sure you want to remove this record?");

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                fileUri = result.getUri();
                fileUrl = new File(fileUri.getPath());
                ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), fileUri, userPicture);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e(TAG, "onActivityResult: " + error.getLocalizedMessage());
            }
        }
    }


    public void onEdit() {
        Log.e(TAG, "onEdit: ");
        userPicture.setClickable(true);
        etVisaType.setEnabled(true);
        etTitle.setEnabled(true);
        etDuration.setEnabled(true);
        etPrice.setEnabled(true);
        etDescription.setEnabled(true);
        etHowToApply.setEnabled(true);
        etRestriction.setEnabled(true);
        etArrival.setEnabled(true);

    }

    @RestAPI
    public void onAdd() {
        /*hit service*/
        Log.e(TAG, "onUpdate: ");

        if (fileUrl != null && Validate()) {
            /*TO add visa*/
            MultipartBody.Part body = null;
            if (fileUrl != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrl);
                body = MultipartBody.Part.createFormData("image", fileUrl.getName(), requestFile);
            }

            RequestBody visatype = RequestBody.create(MediaType.parse("text/plain"), etVisaType.getText().toString());
            RequestBody title = RequestBody.create(MediaType.parse("text/plain"), etTitle.getText().toString());
            RequestBody duration = RequestBody.create(MediaType.parse("text/plain"), etDuration.getText().toString());
            RequestBody price = RequestBody.create(MediaType.parse("text/plain"), etPrice.getText().toString());
            RequestBody description = RequestBody.create(MediaType.parse("text/plain"), etDescription.getText().toString());
            RequestBody apply = RequestBody.create(MediaType.parse("text/plain"), etHowToApply.getText().toString());
            RequestBody restriction = RequestBody.create(MediaType.parse("text/plain"), etRestriction.getText().toString());
            RequestBody arrival = RequestBody.create(MediaType.parse("text/plain"), etArrival.getText().toString());
            RequestBody countryId = RequestBody.create(MediaType.parse("text/plain"), mCountryId);

            serviceHelper.enqueueCall(webService.addvisatype(body, visatype, title,
                    duration, price, description, apply, restriction, arrival, countryId)
                    , WebServiceConstants.addvisatype, true);
        }
    }

    private boolean Validate() {
        if (etVisaType.testValidity() && etTitle.testValidity() && etDuration.testValidity()
                && etPrice.testValidity() && etDescription.testValidity()
                && etHowToApply.testValidity() && etRestriction.testValidity() && etArrival.testValidity()) {
            return true;
        }
        UIHelper.showShortToastInCenter(myDockActivity, "Please fill all fields, also take picture if not taken");
        return false;
    }

    @RestAPI
    public void onUpdate() {

        /*TO update visa type*/
        if (Validate()) {

            MultipartBody.Part body = null;
            if (fileUrl != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrl);
                body = MultipartBody.Part.createFormData("image", fileUrl.getName(), requestFile);
            }
            RequestBody visatype = RequestBody.create(MediaType.parse("text/plain"), etVisaType.getText().toString());
            RequestBody title = RequestBody.create(MediaType.parse("text/plain"), etTitle.getText().toString());
            RequestBody duration = RequestBody.create(MediaType.parse("text/plain"), etDuration.getText().toString());
            RequestBody price = RequestBody.create(MediaType.parse("text/plain"), etPrice.getText().toString());
            RequestBody description = RequestBody.create(MediaType.parse("text/plain"), etDescription.getText().toString());
            RequestBody apply = RequestBody.create(MediaType.parse("text/plain"), etHowToApply.getText().toString());
            RequestBody restriction = RequestBody.create(MediaType.parse("text/plain"), etRestriction.getText().toString());
            RequestBody arrival = RequestBody.create(MediaType.parse("text/plain"), etArrival.getText().toString());
            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), mVisaTypeObj.getId());
            RequestBody countryId = RequestBody.create(MediaType.parse("text/plain"), mVisaTypeObj.getCountryid());

            serviceHelper.enqueueCall(webService.updatevisatype(body, visatype, title,
                    duration, price, description, apply, restriction, arrival, id, countryId)
                    , WebServiceConstants.updatevisatype, true);
        }
    }

   /* @Override
    public void onClick(View v) {
        if (checkLoading())
            switch (titleBar.getRightButtonText()) {
                case "Add":
                    break;
                case "Edit":
                    onEdit();
                    break;
                case "Update":
                    if (mAddVisa)
                        onAdd();
                    else
                        onUpdate();
                    break;
            }
    }*/

  /*  @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        showYesNoDialog("Alert", "Are you sure you want to remove this record?");

    }*/

/*    private void showYesNoDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getDockActivity());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                *//*hit service*//*
                dialog.dismiss();
                serviceHelper.enqueueCall(webService.removevisatype(mVisaTypeObj.getId())
                        , WebServiceConstants.removevisatype, true);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }*/

    @Override
    public void ResponseSuccess(Object result, String Tag, String message) {
        switch (Tag) {
            case WebServiceConstants.addvisatype:
                UIHelper.showShortToastInCenter(getDockActivity(), "Record added successfully");
                getDockActivity().popFragment();
                break;

            case WebServiceConstants.updatevisatype:
                UIHelper.showShortToastInCenter(getDockActivity(), "Record updated successfully");
                getDockActivity().popFragment();
                break;

            case WebServiceConstants.removevisatype:
                UIHelper.showShortToastInCenter(getDockActivity(), "Record removed successfully");
                getDockActivity().popFragment();
                break;
        }
    }
}
