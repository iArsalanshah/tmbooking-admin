package com.tmbooking.admin.fragments.phase_4


import android.app.DialogFragment
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.tmbooking.admin.R
import com.tmbooking.admin.activities.DockActivity
import com.tmbooking.admin.entities.phase4.EditHistory
import com.tmbooking.admin.helpers.SimpleDividerItemDecoration
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.EditHistoryListAdapter
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class EditHistoryFragment : DialogFragment() {
    lateinit var tvEditHistoryName: TextView
    lateinit var rvEditHistory: RecyclerView
    lateinit var btnOk: Button

    lateinit var adapter: RecyclerViewListAdapter<EditHistory>
    lateinit var mContext: DockActivity
    lateinit var fragmentContext: GuestDetailsFragment
    lateinit var name: String
    lateinit var mEditHistory: List<EditHistory>

    /* fun newInstance(): EditHistoryFragment {
         val f = EditHistoryFragment()

         *//* // Supply num input as an argument.
         val args = Bundle()
         args.putCInt("num", num)
         f.setArguments(args)
 *//*
        return f
    }*/
//    public override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_edit_history, container, false)
        tvEditHistoryName = view.findViewById(R.id.tvEditHistoryName)
        rvEditHistory = view.findViewById(R.id.rvEditHistory)
        btnOk = view.findViewById(R.id.btnOk)

//        val width = resources.getDimensionPixelSize(R.dimen.popup_width)
//        val height = resources.getDimensionPixelSize(R.dimen.popup_height)
//        dialog.window!!.setLayout(width, height)

        TextViewHelper.setText(tvEditHistoryName, "Edit History\n" + name)
        btnOk.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onClick(v: View?) {
                fragmentContext.dismissDialog()
            }

        })
        init()
        return view
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = resources.getDimensionPixelSize(R.dimen.popup_width)
            val height = resources.getDimensionPixelSize(R.dimen.popup_height)
            dialog.window.setLayout(width, height)
        }
    }

    private fun init() {
        adapter = EditHistoryListAdapter(mContext)
        rvEditHistory.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvEditHistory.addItemDecoration(SimpleDividerItemDecoration(mContext))
        rvEditHistory.adapter = adapter

//        val mList = ArrayList<EditData>()
//        mList.add(EditData("admin@admin.com", "12:14 min ago", "Waqas Ahmed"))
//        mList.add(EditData("admin@admin.com", "14 days ago", "Zain Sohail"))
        adapter.addAll(mEditHistory)
    }

    /*  class EditData {
          var email: String? = null
          var time: String? = null
          var name: String? = null

          constructor(email: String?, time: String?, name: String?) {
              this.email = email
              this.time = time
              this.name = name
          }
      }*/
}

