package com.tmbooking.admin.fragments.phase_4.visa_application


import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.dmallcott.dismissibleimageview.DismissibleImageView
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.tmbooking.admin.R
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.phase4.VisaStatusEnt
import com.tmbooking.admin.entities.phase4.VisaStatusListEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.ImageLoaderHelper
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_visa_change_status.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * A simple [Fragment] subclass.
 *
 */
class VisaChangeStatusFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View) {
        when (v.id) {
            /*R.id.clAttachVisa -> {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }*/
            R.id.imgAttachVisa -> {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .start(dockActivity, this)
            }
            R.id.tvStatus -> {
                showDialog("Select Status", mVisaStatusName.toTypedArray(), tvStatus)
            }
            R.id.btnSave -> {
                if (Validate())
                    onUpdate()
            }
        }

    }

    private fun Validate(): Boolean {
        return (fileUrlVisa != null || statusId != null)
    }

    @RestAPI
    private fun onUpdate() {
        var bodyVisa: MultipartBody.Part? = null
        if (fileUrlVisa != null) {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUrlVisa)
            bodyVisa = MultipartBody.Part.createFormData("image",
                    fileUrlVisa!!.getName(), requestFile)
        }
        val formId = RequestBody.create(MediaType.parse("text/plain"), formid.toString())
        val statusId = RequestBody.create(MediaType.parse("text/plain"), statusId)
        val reason = RequestBody.create(MediaType.parse("text/plain"), tvStatus.text.toString())
        val comments = RequestBody.create(MediaType.parse("text/plain"), etComment.text.toString())

        serviceHelper.enqueueCall(webService.updatevisatstatus(formId, statusId, reason, comments, bodyVisa),
                WebServiceConstants.updatevisatstatus, true)

    }

    private val TAG: String = "VisaChangeStatusFragment"

    lateinit var imgAttachVisa: ImageView
    lateinit var imgProfilePicture: DismissibleImageView
    lateinit var imgProfileCancel: CircleImageView
    lateinit var etComment: AnyEditTextView
    lateinit var tvStatus: TextView
    lateinit var btnSave: Button
    private lateinit var fileUriVisa: Uri
    private var fileUrlVisa: File? = null

    private lateinit var mObj: VisaStatusEnt
    var formid: Int = 0

    private var mVisaStatusName: ArrayList<String> = ArrayList<String>()
    private var mStatusIdList: ArrayList<String> = ArrayList<String>()

    private var statusId: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_visa_change_status, container, false)
        imgAttachVisa = view.findViewById(R.id.imgAttachVisa)
        imgProfilePicture = view.findViewById(R.id.imgProfilePicture)
//        imgProfileCancel = view.findViewById(R.id.imgProfileCancel)
        tvStatus = view.findViewById(R.id.tvStatus)
        etComment = view.findViewById(R.id.etComment)
        btnSave = view.findViewById(R.id.btnSave)
        callService()
        imgAttachVisa.setOnClickListener(this)
        tvStatus.setOnClickListener(this)
        btnSave.setOnClickListener(this)
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.visa))
    }

    private fun callService() {
        serviceHelper.enqueueCall(webService.getvisatstatus(formid),
                WebServiceConstants.getvisatstatus, true)

        serviceHelper.enqueueCall(webService.getstatus(),
                WebServiceConstants.getstatus, true)
    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        when (Tag) {
            WebServiceConstants.getvisatstatus -> {
                mObj = result as VisaStatusEnt
                setData()
            }
            WebServiceConstants.getstatus -> {
                var visaStatusesList = result as List<VisaStatusListEnt>
                for (i in 0 until visaStatusesList.size) {
                    mVisaStatusName.add(visaStatusesList[i].status)
                    mStatusIdList.add(visaStatusesList[i].id)
                }
            }
            WebServiceConstants.updatevisatstatus -> {
                UIHelper.showShortToastInCenter(dockActivity, message)
            }
        }

    }

    private fun setData() {
        TextViewHelper.setText(tvStatus, mObj.reason)
        TextViewHelper.setText(etComment, mObj.comments)
        if (!mObj.image.isEmpty()) {
            clAttachVisa.visibility = View.VISIBLE
            ImageLoaderHelper.loadImage(mObj.image, imgProfilePicture)
        } else {
            imgAttachVisa.visibility = View.VISIBLE
            clAttachVisa.visibility = View.GONE
        }
        statusId = mObj.status

    }

    @SuppressLint("LongLogTag")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                fileUriVisa = result.uri
                fileUrlVisa = File(fileUriVisa.path)
                ImageLoaderHelper.loadImageWithPicasso(dockActivity, fileUriVisa, imgProfilePicture)

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e(TAG, "onActivityResult: " + error.localizedMessage)
            }
        }
    }

    private fun showDialog(title: String, list: Array<String>, textview: TextView) {
        // setup the alert builder
        val builder = AlertDialog.Builder(dockActivity)
        builder.setTitle(title)
        builder.setItems(list) { dialog, which ->

            TextViewHelper.setText(textview, list[which])
            statusId = mStatusIdList[which]

        }
        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }
}
