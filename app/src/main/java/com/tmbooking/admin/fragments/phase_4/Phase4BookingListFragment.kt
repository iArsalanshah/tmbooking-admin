package com.tmbooking.admin.fragments.phase_4


import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.TextView

import com.tmbooking.admin.R
import com.tmbooking.admin.expandable_booking_recyclerview.Parent
import com.tmbooking.admin.expandable_booking_recyclerview.ParentAdapter
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.ui.adapters.BookingFilterAdapter
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter
import com.tmbooking.admin.entities.ChildBookingItems
import com.tmbooking.admin.entities.phase4.MakkahMadina
import com.tmbooking.admin.interfaces.OnClick
import com.tmbooking.admin.interfaces.OnViewHolderClick
import com.tmbooking.admin.ui.adapters.CustomSpinnerAdapter
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Phase4BookingListFragment : BaseFragment(), OnClick {
    override fun onItemClick(position: Int) {
        when (position) {
            0 ->
                setExpandableRecyclerView(AppConstants.DELIVERY, currentSelectedStatus)
            1 ->
                setExpandableRecyclerView(AppConstants.TRANSPORT, currentSelectedStatus)
            2 ->
                setExpandableRecyclerView(AppConstants.HOTEL, currentSelectedStatus)
            3 ->
                setExpandableRecyclerView(AppConstants.ALL, currentSelectedStatus)
        }
    }

    lateinit var spSelectStatus: Spinner
    lateinit var rvBookingFilter: RecyclerView
    lateinit var rvBooking: RecyclerView
    lateinit var adapter: ParentAdapter
    lateinit var tvAlternateText: TextView
    lateinit var adapterFilter: RecyclerViewListAdapter<Drawable>
    var TAG: String = "Phase4BookingListFragment"
    lateinit var mListResponse: List<MakkahMadina>
    private lateinit var genres: List<Parent>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_phase4_booking_list, container, false)
        rvBooking = view.findViewById(R.id.rvBooking)
        rvBookingFilter = view.findViewById(R.id.rvBookingFilter)
        tvAlternateText = view.findViewById(R.id.tvAlternateText)
        spSelectStatus = view.findViewById(R.id.spSelectStatus)
        rvBooking.layoutManager = (LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false))

        initFilter()
        initSpinner()
        return view
    }

    private var currentSelectedStatus: String = ""
    val mStatusList = arrayOf("All", "Confirmed", "Processing", "Delivered", "Cancelled", "On hold", "Pending payments")

    private fun initSpinner() {


        val adapter = CustomSpinnerAdapter(dockActivity, R.layout.item_spinner, mStatusList, "Select Status")
        spSelectStatus.adapter = adapter
        setStatusSpinnerListener()
    }

    private fun setStatusSpinnerListener() {
        spSelectStatus.post {
            spSelectStatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (genres != null)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            currentSelectedStatus = mStatusList[position]
                            spSelectStatus.background = resources.getDrawable(R.drawable.stroke_red_solid_light_red_rounded)
                            if (mStatusList[position].equals(mStatusList[0]))
                                setAdapter(genres)
                            else
                                setAdapter(getStatusList(genres, mStatusList[position]))
                        }
                }
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setExpandableRecyclerView(AppConstants.ALL, currentSelectedStatus)
    }


    private fun setExpandableRecyclerView(type: String, mStatus: String) {
        genres = getGenres(type)
        if (!mStatus.isEmpty() && !mStatus.equals("All", true) && !mStatus.equals("Select Status", true)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setAdapter(getStatusList(genres, mStatus)) /*For Type + Status filter */
            }
        } else
            setAdapter(genres) /*For Type filter*/
    }

    private fun setAdapter(genres: List<Parent>) {
        adapter = ParentAdapter(dockActivity, genres)
        rvBooking.adapter = adapter
        /*for expand a single group at a time*/
        /*   adapter.setOnGroupExpandCollapseListener(object : GroupExpandCollapseListener {
               override fun onGroupExpanded(group: ExpandableGroup<*>) {
                   if (expandedGroup != null) {
                       adapter.toggleGroup(expandedGroup)
                   }
                   expandedGroup = group
               }

               override fun onGroupCollapsed(group: ExpandableGroup<*>) {

               }
           })*/

        val expandableGroupList = adapter.groups
        /*Expandle recycler view library donot expand groups with for oop started from i=0 , it does work reverse */
        for (i in expandableGroupList.indices.reversed()) {
            if (expandableGroupList[i] != null) {
                adapter.toggleGroup(expandableGroupList[i])
            }
        }
    }


    private fun initFilter() {
        adapterFilter = BookingFilterAdapter(dockActivity, this)
        rvBookingFilter.layoutManager = LinearLayoutManager(dockActivity, LinearLayoutManager.HORIZONTAL, false)
        rvBookingFilter.adapter = adapterFilter

        var list: ArrayList<Drawable> = ArrayList()
        list.add(resources.getDrawable(R.drawable.ic_gift))
        list.add(resources.getDrawable(R.drawable.ic_car))
        list.add(resources.getDrawable(R.drawable.ic_hotel))
        list.add(resources.getDrawable(R.drawable.ic_all))
        adapterFilter.addAll(list)
    }

    private fun getGenres(type: String): List<Parent> {
        var mParent = ArrayList<Parent>()
        var mParentHotel = ArrayList<Parent>()
        var mParentDelivery = ArrayList<Parent>()
        var mParentTransport = ArrayList<Parent>()

        if (isContainData()) {
            /*Set UI*/
            tvAlternateText.visibility = View.GONE
            rvBookingFilter.visibility = View.VISIBLE

            for (i in 0 until mListResponse.size) {
                var mChild = ArrayList<ChildBookingItems>()
                var mHotelList = ArrayList<ChildBookingItems>()
                var mDeliveryList = ArrayList<ChildBookingItems>()
                var mTransportList = ArrayList<ChildBookingItems>()

                /*to fectch hotels data*/
                for (j in 0 until mListResponse[i].hotels.size) {
                    mChild.add(ChildBookingItems(mListResponse[i].hotels[j], AppConstants.HOTEL
                            , mListResponse[i].hotels[j].time, mListResponse[i].hotels[j].status))
                    mHotelList.add(ChildBookingItems(mListResponse[i].hotels[j], AppConstants.HOTEL
                            , mListResponse[i].hotels[j].time, mListResponse[i].hotels[j].status))
                }

                /*to fectch delivery data*/
                for (k in 0 until mListResponse[i].delivery.size) {
                    mChild.add(ChildBookingItems(mListResponse[i].delivery[k], AppConstants.DELIVERY
                            , mListResponse[i].delivery[k].time, mListResponse[i].delivery[k].status))
                    mDeliveryList.add(ChildBookingItems(mListResponse[i].delivery[k], AppConstants.DELIVERY
                            , mListResponse[i].delivery[k].time, mListResponse[i].delivery[k].status))

                }
                /*to fectch transport data*/
                for (l in 0 until mListResponse[i].transport.size) {
                    mChild.add(ChildBookingItems(mListResponse[i].transport[l], AppConstants.TRANSPORT
                            , mListResponse[i].transport[l].time, mListResponse[i].transport[l].status))
                    mTransportList.add(ChildBookingItems(mListResponse[i].transport[l], AppConstants.TRANSPORT
                            , mListResponse[i].transport[l].time, mListResponse[i].transport[l].status))
                }

                /*now sort the list here*/
                mChild = sortedList(mChild)
                mHotelList = sortedList(mHotelList)
                mDeliveryList = sortedList(mDeliveryList)
                mTransportList = sortedList(mTransportList)

                if (mChild.size > 0)
                    mParent.add(Parent(mListResponse[i].selectedData, mChild))

                if (mHotelList.size > 0)
                    mParentHotel.add(Parent(mListResponse[i].selectedData, mHotelList))

                if (mDeliveryList.size > 0)
                    mParentDelivery.add(Parent(mListResponse[i].selectedData, mDeliveryList))

                if (mTransportList.size > 0)
                    mParentTransport.add(Parent(mListResponse[i].selectedData, mTransportList))
            }

            when (type) {
                AppConstants.HOTEL ->
                    return mParentHotel
                AppConstants.DELIVERY ->
                    return mParentDelivery
                AppConstants.TRANSPORT ->
                    return mParentTransport
                AppConstants.ALL ->
                    return mParent
            }
        } else {
            tvAlternateText.visibility = View.GONE
            rvBookingFilter.visibility = View.VISIBLE
        }

        return mParent
    }


    @RequiresApi(Build.VERSION_CODES.N)
    private fun getStatusList(myList: List<Parent>, s: String): List<Parent> {

        var myFilter: ArrayList<Parent> = ArrayList()

        for (i in 0 until myList.size) {
            var mChild: MutableList<ChildBookingItems>? = ArrayList()

            val childList = myList.get(i).items
            mChild = childList.stream().filter { mlist ->
                mlist.status.contains(s)
            }.collect(Collectors.toList<ChildBookingItems>())

            myFilter.add(Parent(myList[i].title, mChild))
        }

        return myFilter

    }

    private fun sortedList(list: ArrayList<ChildBookingItems>): ArrayList<ChildBookingItems> {
        Collections.sort(list, object : Comparator<ChildBookingItems> {
            override fun compare(o1: ChildBookingItems, o2: ChildBookingItems): Int {
                return o1.time.compareTo(o2.time)
            }
        })
        return list
    }

    private fun isContainData(): Boolean {

        for (i in 0 until mListResponse.size) {
            if (mListResponse[i] != null) {
                if (mListResponse[i].hotels.size > 0 || mListResponse[i].delivery.size > 0 || mListResponse[i].transport.size > 0)
                    return true
            }
        }
        /* if (mListResponse[0] != null && mListResponse[0].hotels.size == 0
                 && mListResponse[0].delivery.size == 0
                 && mListResponse[0].transport.size == 0) {
             tvAlternateText.visibility = View.VISIBLE
             rvBookingFilter.visibility = View.GONE
             return false
         } else {
             tvAlternateText.visibility = View.GONE
             rvBookingFilter.visibility = View.VISIBLE
         }*/
        return false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        adapter.onSaveInstanceState(outState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter.onRestoreInstanceState(savedInstanceState)

    }
}
