package com.tmbooking.admin.fragments.phase_4


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.tmbooking.admin.R
import com.tmbooking.admin.R.id.*
import com.tmbooking.admin.entities.phase4.Delivery
import com.tmbooking.admin.entities.phase4.DeliveryFail
import com.tmbooking.admin.entities.phase4.DeliverySucces
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.TitleBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class GiftsFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        callService()
    }
    /* Gifts include dinner , gifts and lunch etc */

    lateinit var tvGiftName: TextView
    lateinit var tvGuestName: TextView
    lateinit var tvGuestDetails: TextView
    lateinit var btnDelivered: Button
    lateinit var deliveryObj: Delivery

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_gifts, container, false)
        tvGiftName = view.findViewById(R.id.tvGiftName)
        tvGuestName = view.findViewById(R.id.tvGuestName)
        tvGuestDetails = view.findViewById(R.id.tvGuestDetails)
        btnDelivered = view.findViewById(R.id.btnDelivered)
        btnDelivered.setOnClickListener(this)
        setViews()

        setData()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.gift)
    }

    private fun setViews() {
        if (prefHelper.loginData.user_role == AppConstants.MANDOOB_DELIVERY)
            btnDelivered.visibility = View.VISIBLE
        else
            btnDelivered.visibility = View.GONE

        if (deliveryObj.status == AppConstants.PENDING_DELIVERY) {
            btnDelivered.text = resources.getString(R.string.deliver)
        } else if (deliveryObj.status == AppConstants.DELIVERED) {
            btnDelivered.text = resources.getString(R.string.delivered)
            btnDelivered.isClickable = false
        }

    }

    private fun setData() {
        if (deliveryObj != null) {
            TextViewHelper.setText(tvGiftName, deliveryObj.name)
            TextViewHelper.setText(tvGuestName, deliveryObj.guest_name)

            TextViewHelper.setText(tvGuestDetails,
                    "" + deliveryObj.hotel_name +
                            "\n Room #: " + deliveryObj.room_number +
                            "\n Saudi Contact Number: " + deliveryObj.contact_number +
                            "\n Date and Time : " + deliveryObj.date + " " + deliveryObj.time +
                            "\n Quantity : " + deliveryObj.quantity
            )
        }
    }

    private fun callService() {
//        getServiceHelperWP().enqueueCallWP(webServiceWP
//                .delivery(/*deliveryObj.delivery_id*/"11", 177), WebServiceConstants.DELIVERY, true)


        getWebServiceWP().delivery(deliveryObj.delivery_id, prefHelper.loginData.id/*177*/).enqueue(object : Callback<DeliverySucces> {
            override fun onResponse(call: Call<DeliverySucces>, response: Response<DeliverySucces>) {
                loadingFinished()
                if (response.body() != null) {
                    responseSuccess(response.body())
                } else
                    UIHelper.showShortToastInCenter(dockActivity, "This delivery already delivered or didnot exist")

            }

            override fun onFailure(call: Call<DeliverySucces>, t: Throwable) {
                UIHelper.showShortToastInCenter(dockActivity, t.localizedMessage)
            }
        })
    }

    fun responseSuccess(result: Any?) {
        var deliverySucces = result as DeliverySucces
        if (deliverySucces != null) {
            UIHelper.showShortToastInCenter(dockActivity, deliverySucces.status)
            dockActivity.popFragment()
        }
        /*else
            UIHelper.showShortToastInCenter(dockActivity, "This delivery already delivered or didnot exist")
*/
        /*  else {
              var deliveryFail = result as DeliveryFail
              if (deliveryFail != null) {
                  UIHelper.showShortToastInCenter(dockActivity, deliveryFail.data.params.delivery_id)

              }
          }*/
    }


}
