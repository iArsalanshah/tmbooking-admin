package com.tmbooking.admin.fragments


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.BookingEnt
import com.tmbooking.admin.entities.DriverEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.fragments.phase_4.DriverLogListFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.DateHelper
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.TitleBar
import com.tmbooking.admin.ui.views.Util
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */

class BookingDetailsFormFragment : BaseFragment() {

//    lateinit var etCar: AnyEditTextView
//    lateinit var etPickUp: AnyEditTextView
//    lateinit var etDropOff: AnyEditTextView
//    lateinit var etPickupDate: AnyEditTextView
//    lateinit var etPickUpTime: AnyEditTextView
//    lateinit var etExtraInfo: AnyEditTextView
//    lateinit var etStatus: AnyEditTextView
//    lateinit var etAction: AnyEditTextView


    lateinit var tvOrderNo: TextView
    lateinit var tvTransportType: TextView
    lateinit var tvPickupTime: TextView
    lateinit var tvPickup: TextView
    lateinit var tvDroppOff: TextView
    lateinit var tvExtraDetails: TextView
    lateinit var tvDriverAssigned: TextView


    lateinit var btnSubmit: Button
    lateinit var btnAssignedDriversLog: Button
    lateinit var mFragmentName: String
    lateinit var mlist: ArrayList<DriverEnt>


    lateinit var mBookingEnt: BookingEnt

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_booking_details_form, container, false)
//        etCar = view.findViewById(R.id.etCar)
//        etPickUp = view.findViewById(R.id.etPickUp)
//        etDropOff = view.findViewById(R.id.etDropOff)
//        etPickupDate = view.findViewById(R.id.etPickupDate)
//        etPickUpTime = view.findViewById(R.id.etPickUpTime)
//        etExtraInfo = view.findViewById(R.id.etExtraInfo)
//        etStatus = view.findViewById(R.id.etStatus)
//        etAction = view.findViewById(R.id.etAction)

        tvOrderNo = view.findViewById(R.id.tvOrderNo)
        tvTransportType = view.findViewById(R.id.tvTransportType)
        tvPickupTime = view.findViewById(R.id.tvPickupTime)
        tvPickup = view.findViewById(R.id.tvPickup)
        tvDroppOff = view.findViewById(R.id.tvDropOff)
        tvExtraDetails = view.findViewById(R.id.tvDetails)
        tvDriverAssigned = view.findViewById(R.id.tvDriverAssigned)

        btnSubmit = view.findViewById(R.id.btnSubmit)
        btnAssignedDriversLog = view.findViewById(R.id.btnAssignedDriversLog)

//        btnAssignedDriversLog.visibility = View.VISIBLE

       /* if (Util.getParsedInteger(mBookingEnt.statusValue) == 0) //means status is pending
        {
            btnSubmit.text = "Confirm booking"
            btnAssignedDriversLog.visibility = View.GONE
        } else if (Util.getParsedInteger(mBookingEnt.statusValue) == 6) {
            btnSubmit.text = "Assign driver" //COnfirm list
            btnAssignedDriversLog.visibility = View.GONE

        } else if (Util.getParsedInteger(mBookingEnt.statusValue) == 1) {
            btnSubmit.text = "Re-Assign driver"
        }
*/
        when (Util.getParsedInteger(mBookingEnt.statusValue)) {
            0 -> {
                btnSubmit.text = "Confirm booking"
                btnAssignedDriversLog.visibility = View.GONE
            }
            6 -> {
                btnSubmit.text = "Assign driver" //COnfirm list
                btnAssignedDriversLog.visibility = View.GONE
            }
            1 -> {
                btnSubmit.text = "Re-Assign driver"
            }
            4 -> {//COmpleted
                btnSubmit.visibility = View.GONE
            }
            7 -> {//COmpleted
                btnSubmit.visibility = View.GONE
            }
        }

        btnSubmit.setOnClickListener {
            if (Util.getParsedInteger(mBookingEnt.statusValue) == 0) //means status is pending
            {
                showYesNoDialog("Confirmation", "Are you sure you want to confirm this booking?")
                return@setOnClickListener
            } else if (Util.getParsedInteger(mBookingEnt.statusValue) == 1 || Util.getParsedInteger(mBookingEnt.statusValue) == 6)
                callDriverList()
        }

        btnAssignedDriversLog.setOnClickListener {
            //open driver list log
            val fragment = DriverLogListFragment()
            fragment.id = mBookingEnt.id
            dockActivity.replaceDockableFragment(fragment)
        }


        setData()
        return view
    }

    private fun callDriverList() {
        serviceHelper.enqueueCall(webService.unbookdriver("1"),
                WebServiceConstants.unbookdriver, true)
    }

    /*  private fun openFlycoDialog() {
          val msgList = arrayOf("Please Wait", "I'm coming in 5 minutes", "I'm Waiting")

          val dialog = ActionSheetDialog(dockActivity, msgList, null)
          dialog.title(resources.getString(R.string.select_your_message))
                  .titleTextSize_SP(14.5f)
                  .cancelText(resources.getString(android.R.string.cancel))
                  .show()
          dialog.setOnOperItemClickL { parent, view, position, id ->
              textmsg = msgList[position]
              val number = "02135644598"  // The number on which you want to send SMS
              sendSMS(number, textmsg)
              dialog.dismiss()
          }
      }
  */
    fun openSingleChoiceDialog(arrayList: Array<String?>) {
        AlertDialog.Builder(dockActivity)
                .setTitle("Assign Driver")
                .setSingleChoiceItems(arrayList, 0, null)
                .setNegativeButton(R.string.cancel) { dialog, which ->
                    dialog.dismiss()
                }
                .setPositiveButton(R.string.assign) { dialog, whichButton ->
                    dialog.dismiss()
                    val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                    // Do something useful withe the position of the selected radio button
                    assignDriver(mBookingEnt.pickuptime, mlist[selectedPosition].getId()!!, mBookingEnt.id)
                }
                .show()
    }

    private fun assignDriver(pickuptime: String, driverid: String, rideid: String) {
        serviceHelper.enqueueCall(webService.assigndriver(pickuptime, driverid, rideid),
                WebServiceConstants.assigndriver, true)
    }

    fun showYesNoDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(dockActivity)

        builder.setTitle(title)

        builder.setMessage(message)
        builder.setPositiveButton("Yes") { dialog, which ->
            //            Hit api for Yes
            serviceHelper.enqueueCall(webService.confirmationBooking(mBookingEnt.id),
                    WebServiceConstants.confirmationBooking, true)
            dialog.dismiss()
        }
        builder.setNegativeButton("No") { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        //        titleBar.showHomeButton();
        titleBar.setSubHeading(resources.getString(R.string.booking_detail)) // this will set only when fragment appear for history
    }

    fun setObjectData(bookingEnt: BookingEnt, mFragmentName: String) {
        mBookingEnt = bookingEnt
        this.mFragmentName = mFragmentName
    }


    private fun setData() {
//        TextViewHelper.setText(etCar, mBookingEnt.carName)
//        TextViewHelper.setText(etPickUp, mBookingEnt.pickup)
//        TextViewHelper.setText(etDropOff, mBookingEnt.dropoff)
//        TextViewHelper.setText(etPickupDate, mBookingEnt.pickuptime)
//        TextViewHelper.setText(etPickUpTime, mBookingEnt.pickuptime)
//        TextViewHelper.setText(etExtraInfo, mBookingEnt.extrainfo)
//        TextViewHelper.setText(etStatus, mBookingEnt.bookingStatus)

        val date = DateHelper.stringToDate(mBookingEnt.getPickuptime(), DateHelper.DATE_TIME_FORMAT)
        val mDate = DateHelper.getFormattedDate(date)
        val mTime = DateHelper.getFormattedTime(date)
        TextViewHelper.setText(tvOrderNo, "Order Number: " + mBookingEnt.orderid)
        TextViewHelper.setText(tvTransportType, "Transport Type: " + mBookingEnt.carName)
        TextViewHelper.setText(tvPickup, "Pickup: ${mBookingEnt.pickup}")
        TextViewHelper.setText(tvDroppOff, "Dropoff: ${mBookingEnt.dropoff}")
        TextViewHelper.setText(tvPickupTime, "Date: $mDate at $mTime")

        TextViewHelper.setText(tvExtraDetails, "Extra Details: " + mBookingEnt.extrainfo)
        TextViewHelper.setText(tvDriverAssigned, "Driver Assigned: " + mBookingEnt.rider_name)

        /* when (Util.getParsedInteger(mBookingEnt.statusValue)) {

             *//*status =0 , 6 --> pending ;
             status =1,2 --> Arrived ;
             *//*
            0 -> *//* status = Pending *//*
                TextViewHelper.setText(etAction, resources.getString(R.string.confirmation))
            6 -> *//* status = Confirmed *//*
                TextViewHelper.setText(etAction, resources.getString(R.string.assign_driver))
            1 -> *//* status = Assign *//*
                TextViewHelper.setText(etAction, resources.getString(R.string.reassign_driver))
            2 -> *//* status = Arrived *//*
                btnSubmit.visibility = View.GONE
        }*/

        /*  *//*FOr Cancel booking*//*
        if (mFragmentName === CANCEL_TEXT) {
            TextViewHelper.setText(etAction, resources.getString(R.string.cancel_booking))
            btnSubmit.visibility = View.GONE
        }
        *//*FOr booking History *//*
        if (mFragmentName === HISTORY_TEXT) {
            TextViewHelper.setText(etAction, resources.getString(R.string.none))
            btnSubmit.visibility = View.GONE
        }*/

    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        when (Tag) {
            WebServiceConstants.confirmationBooking -> {
                UIHelper.showShortToastInCenter(dockActivity, "Booking has been confirmed")
                dockActivity.popFragment()
            }
            WebServiceConstants.unbookdriver -> {
                mlist = result as ArrayList<DriverEnt>
                val arrayList = arrayOfNulls<String>(mlist.size)
                for (i in mlist.indices) {
                    arrayList[i] = mlist[i].getName()
                }
                openSingleChoiceDialog(arrayList)

            }
            WebServiceConstants.assigndriver -> {
                UIHelper.showShortToastInCenter(dockActivity, "Driver assigned successfully")
                dockActivity.popFragment()
            }
        }
    }
}

