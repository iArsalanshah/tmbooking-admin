package com.tmbooking.admin.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.BookingEnt;
import com.tmbooking.admin.entities.RideListEnt;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.interfaces.OnListItemClick;
import com.tmbooking.admin.ui.adapters.BookingAdapter;
import com.tmbooking.admin.ui.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingFragmentttt extends BaseFragment {


    private static final String IS_HISTORY = "IS_HISTORY";
    private static final String FRAGMENT_TITLE = "FRAGMENT_TITLE";
    @BindView(R.id.tvNoBooking)
    TextView tvNoBooking;
    @BindView(R.id.rvBooking)
    RecyclerView rvBooking;
    Unbinder unbinder;
    private BookingAdapter adapter;
    static boolean isHistory;
    private static String fragmentTitle;
    List<RideListEnt> mList = new ArrayList<>();
    List<RideListEnt> mActiveList = new ArrayList<>();
    List<RideListEnt> mUpcomingList = new ArrayList<>();


    public BookingFragmentttt() {
        // Required empty public constructor
    }

    public static BookingFragmentttt newInstance(boolean _isHistory, String _fragmentTitle) {
        BookingFragmentttt fragment = new BookingFragmentttt();
        Bundle arg = new Bundle();
        arg.putBoolean(IS_HISTORY, _isHistory);
        arg.putString(FRAGMENT_TITLE, _fragmentTitle);
        fragment.setArguments(arg);
//        isHistory = _isHistory;
//        fragmentTitle = _fragmentTitle;


        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments() != null) {
            isHistory = getArguments().getBoolean(IS_HISTORY);
            fragmentTitle = getArguments().getString(FRAGMENT_TITLE);
        }

        initAdapter();
//        if (prefHelper != null && prefHelper.getUser() != null && prefHelper.getUser().getId() != null)
//            callService(Util.getParsedInteger(prefHelper.getUser().getId()));
        return view;
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
//        titleBar.showHomeButton();
        titleBar.setSubHeading(getResources().getString(R.string.active_booking)); // this will set only when fragment appear for history
    }

    private void initAdapter() {
        adapter = new BookingAdapter(getDockActivity(), new OnListItemClick() {
            @Override
            public void onClick(int position, BookingEnt obj) {
                /*if (!obj.getStatus().equals(AppConstants.PENDING))
                  *//*  if (prefHelper.getRide() != null && (
                            Util.getParsedInteger(prefHelper.getRide().getStatus()) == AppConstants.ARRIVED ||
                                    Util.getParsedInteger(prefHelper.getRide().getStatus()) == AppConstants.START_RIDE
                    )) {
                        getDockActivity().popBackStackTillEntry(0);
                    }*//*
                getDockActivity().replaceDockableFragment(ActiveRideDetailsFragment.newInstance(isHistory, obj),
                        ActiveRideDetailsFragment.class.getSimpleName());*/
            }
        });
        rvBooking.setLayoutManager(new LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false));
        List<BookingEnt> list = new ArrayList<>();
        list.add(new BookingEnt());
        list.add(new BookingEnt());
        list.add(new BookingEnt());
        adapter.addAll(list);
        rvBooking.setAdapter(adapter);

        if(isValidate()){}
    }

    private boolean isValidate() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
/*
    private void callService(int userId) {
        if (fragmentTitle.equals(getResources().getString(R.string.history)))
            serviceHelper.enqueueCall(webService.getpastriverrides(userId),
                    WebServiceConstants.getpastriverrides, true);

        else
            //if (fragmentTitle.equals(getResources().getString(R.string.active_rides)))
            serviceHelper.enqueueCall(webService.getactivedriverrides(userId),
                    fragmentTitle, true);
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        if (BookingFragmentttt.this.isDetached()
                || BookingFragmentttt.this.isRemoving()
                || !BookingFragmentttt.this.isVisible()) return;
        switch (Tag) {
            case WebServiceConstants.getpastriverrides: //History rides
                if (result != null) {
                    mList.clear();
                    mList.addAll((List<RideListEnt>) result);

                    if (mList.size() > 0) {
//                    mList.clear();
//                    mList.add((RideListEnt) result);
                        tvNoBooking.setVisibility(View.GONE);

                        adapter.addAll(mList);
                    } else
                        tvNoBooking.setVisibility(View.VISIBLE);
                }

                break;
            case WebServiceConstants.ACTIVE: //Active and scheduled rides
                if (result != null) {
                    setData((List<RideListEnt>) result, WebServiceConstants.ACTIVE);
                }
                break;
            case WebServiceConstants.UPCOMING: //Active and scheduled rides
                if (result != null) {
                    setData((List<RideListEnt>) result, WebServiceConstants.UPCOMING);
                }
                break;

//            case WebServiceConstants.getmyrides: //History rides
//                break;
        }
    }

    private void setData(List<RideListEnt> mList, String _fragment) {
        mActiveList.clear();
        mUpcomingList.clear();
        Date mDate = null;
       *//* for (int i = 0; i < mList.size(); i++) {
            mDate = DateHelper.stringToDate(mList.get(i).getPickuptime(), DateHelper.DATE_FORMAT);
            if (fragmentTitle.equals(getResources().getString(R.string.active_rides))
                    && (DateHelper.isToday(mDate))) {
                mSortedList.add(mList.get(i));
            } else if (fragmentTitle.equals(getResources().getString(R.string.upcoming_rides))
                    && (DateHelper.isFutureDate(mDate))) {
                mSortedList.add(mList.get(i));
            }

        }*//*
        for (RideListEnt ride : mList) {
            mDate = DateHelper.stringToDate(ride.getPickuptime(), DateHelper.DATE_FORMAT);
            if (DateHelper.isToday(mDate)) {
                mActiveList.add(ride);
            } else if (DateHelper.isFutureDate(mDate)) {
                mUpcomingList.add(ride);
            }
        }

        if (_fragment.equals(WebServiceConstants.ACTIVE)) {
            if (mActiveList.size() > 0) {

                tvNoBooking.setVisibility(View.GONE);

                adapter.addAll(mActiveList);
            } else
                tvNoBooking.setVisibility(View.VISIBLE);
        } else if (_fragment.equals(WebServiceConstants.UPCOMING)) {
            if (mUpcomingList.size() > 0) {

                tvNoBooking.setVisibility(View.GONE);

                adapter.addAll(mUpcomingList);
            } else
                tvNoBooking.setVisibility(View.VISIBLE);
        }

    }*/
}
