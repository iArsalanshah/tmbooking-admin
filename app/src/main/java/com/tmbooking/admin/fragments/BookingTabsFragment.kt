package com.tmbooking.admin.fragments


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.ui.views.TitleBar
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
/*admin*/

class BookingFragment : BaseFragment() {

    lateinit var tabs: TabLayout
    lateinit var vpBookings: ViewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_booking_tabs, container, false)
        /*init view*/
        tabs = view.findViewById(R.id.tabs);
        vpBookings = view.findViewById(R.id.vpBookings);
        /*=========*/

        setupViewPager(vpBookings)
        tabs.setupWithViewPager(vpBookings)
        setTabsLayout()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.bookings))
    }

    private fun setTabsLayout() {
//        val textView1 = LayoutInflater.from(getDockActivity()).inflate(R.layout.custom_tab, null) as TextView
//        val textView2 = LayoutInflater.from(getDockActivity()).inflate(R.layout.custom_tab, null) as TextView
//
//        textView1.text = resources.getString(R.string.pending)
//        textView2.text = resources.getString(R.string.confirmed)
//        textView1.gravity = Gravity.CENTER_HORIZONTAL
//        textView2.gravity = Gravity.CENTER_HORIZONTAL
//        tabs.getTabAt(0)!!.customView = textView1
//        tabs.getTabAt(1)!!.customView = textView2 //?. --> only safe , !!. --> not null

        for (i in 0 until tabs.tabCount) {
            val tab = tabs.getTabAt(i)

            val tabTextView = LayoutInflater.from(getDockActivity()).inflate(R.layout.custom_tab2, null) as TextView
            tabTextView.text = tab!!.text
            tabTextView.layoutParams = TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            tab.customView = tabTextView
//            tab.select()
        }

    }

    private fun setupViewPager(viewpager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        val fragment0 = BookingListFragment()
        fragment0.whichFragment(AppConstants.PENDING_TEXT)
        adapter.addFragment(fragment0, resources.getString(R.string.pending))


        val fragment1 = BookingListFragment()
        fragment1.whichFragment(AppConstants.CONFIRMED_TEXT)
        adapter.addFragment(fragment1, resources.getString(R.string.confirmed))

        val fragment2 = BookingListFragment()
        fragment2.whichFragment(AppConstants.ACTIVE_TEXT)
        adapter.addFragment(fragment2, resources.getString(R.string.active))

        val fragment3 = BookingListFragment()
        fragment3.whichFragment(AppConstants.HISTORY_TEXT)
        adapter.addFragment(fragment3, resources.getString(R.string.completed))

        val fragment4 = BookingListFragment()
        fragment4.whichFragment(AppConstants.CANCEL_TEXT)
        adapter.addFragment(fragment4, resources.getString(R.string.cancelled))


        viewpager.adapter = adapter;
    }


    internal inner class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment? {
            return mFragmentList[position]
//            when (position) {
//                0 -> {
//                    val fragment0 = BookingListFragment()
//                    fragment0.whichFragment(AppConstants.PENDING_TEXT)
//                    return fragment0 // Fragment # 0 - This will show FirstFragment //bookinglistfragment replace after it..
//                }
//                1 -> {
//                    val fragment1 = BookingListFragment()
//                    fragment1.whichFragment(AppConstants.CONFIRMED_TEXT)
//                    return fragment1
//                }
//                2 -> {
//                    val fragment2 = BookingListFragment()
//                    fragment2.whichFragment(AppConstants.ACTIVE_TEXT)
////                    return fragment1
//                }
//                3 -> {
//                    val fragment1 = BookingListFragment()
//                    fragment1.whichFragment(AppConstants.HISTORY_TEXT)
//                    return fragment1
//                }
//                4 -> {
//                    val fragment1 = BookingListFragment()
//                    fragment1.whichFragment(AppConstants.CANCEL_TEXT)
//                    return fragment1
//                }
////                -> return BookingFragmentttt.newInstance(false, resources.getString(R.string.upcoming_rides))
//            }

            return null
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }
}
