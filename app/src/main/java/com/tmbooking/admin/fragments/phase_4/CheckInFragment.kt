package com.tmbooking.admin.fragments.phase_4


import android.os.Bundle
import android.provider.Contacts
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.Hotel
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.ui.adapters.CheckInRoomNumberListAdapter
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import android.widget.TextView
import com.tmbooking.admin.entities.phase4.CheckIn
import com.tmbooking.admin.entities.phase4.DeliverySucces
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.helpers.DateHelper
import com.tmbooking.admin.helpers.UIHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.StringBuilder
import com.tmbooking.admin.entities.Nothing


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CheckInFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        callService()
    }

    val TAG: String = "CheckInFragment"
    lateinit var rvRoomNumber: RecyclerView
    lateinit var etMessage: AnyEditTextView
    lateinit var btnSubmit: Button
    lateinit var adapter: RecyclerViewListAdapter<String>
    lateinit var mName: String

    lateinit var hotelObj: Hotel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_check_in, container, false)
        rvRoomNumber = view.findViewById(R.id.rvRoomNumber)
        etMessage = view.findViewById(R.id.etMessage)
        btnSubmit = view.findViewById(R.id.btnSubmit)
        btnSubmit.setOnClickListener(this)
        init()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar!!.subHeading = mName
    }

    private var totalRooms: Int = 0

    private fun init() {
        totalRooms = hotelObj.total_rooms.toInt()

        adapter = CheckInRoomNumberListAdapter(dockActivity)
        rvRoomNumber.layoutManager = LinearLayoutManager(dockActivity, LinearLayoutManager.VERTICAL, false)
        rvRoomNumber.adapter = adapter

        val mList = ArrayList<String>()

        for (i in 0 until hotelObj.total_rooms.toInt()) {
            var index = i + 1
            mList.add("Room No #" + index + ":")
        }
//        mList.add("Room No #2:")
        adapter.addAll(mList)
    }


    private fun callService() {

        if (mName == AppConstants.CHECKIN) {
            var mRooms = StringBuilder()
            var prefix = ""
            for (i in 0 until adapter.list.size) {
                val title1 = (rvRoomNumber.findViewHolderForAdapterPosition(i)
                        .itemView.findViewById(R.id.etRoomNumber) as AnyEditTextView).text.toString()

                if (!title1.isEmpty()) {
                    mRooms.append(prefix)
                    prefix = ","
                    mRooms.append(title1)
                } else {
                    UIHelper.showShortToastInCenter(dockActivity, "Please fill all room numbers")
                    return
                }


            }
            Log.d(TAG, mRooms.toString())
            getServiceHelperWP().enqueueCallWP(webServiceWP
                    .checkIn(hotelObj.booking_id, mRooms.toString(), etMessage.text.toString()),
                    WebServiceConstants.CHECKIN, true)

        }
//        else
//            checkoutAPI()

    }

    private fun checkoutAPI() {
        loadingStarted()
        getWebServiceWP().checkout(hotelObj.booking_id).enqueue(object : Callback<List<Nothing>> {
            override fun onResponse(call: Call<List<Nothing>>, response: Response<List<Nothing>>) {
                loadingFinished()
                UIHelper.showShortToastInCenter(dockActivity, mName + " Succesfully")
                dockActivity.popBackStackTillEntry(1)
            }

            override fun onFailure(call: Call<List<Nothing>>, t: Throwable) {
                loadingFinished()
                UIHelper.showShortToastInCenter(dockActivity, t.localizedMessage)
            }
        })
    }

    override fun ResponseSuccess(result: Any?, Tag: String?) {
        var mResult = result as CheckIn
        if (mResult != null) {
            UIHelper.showShortToastInCenter(dockActivity, mName + " Succesfully")
            dockActivity.popBackStackTillEntry(1)
        }
    }

}
