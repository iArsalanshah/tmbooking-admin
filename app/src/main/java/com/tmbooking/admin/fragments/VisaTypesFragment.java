package com.tmbooking.admin.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.VisaApplicationEnt;
import com.tmbooking.admin.entities.VisaTypesEnt;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.fragments.phase_4.visa_management.VisaTypeForm;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.ui.adapters.VisaTypesAdapter;
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.tmbooking.admin.ui.views.TitleBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
/*admin*/
public class VisaTypesFragment /*Same as country visa fragment*/extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.rvVisaTypes)
    RecyclerView rvVisaTypes;
    Unbinder unbinder;
    RecyclerViewListAdapter adapter;
    private boolean isVisaTypes;

    private boolean isMember;
    String userid;


    public VisaTypesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_visa_types, container, false);
        unbinder = ButterKnife.bind(this, view);

        initAdapter();
        callService();

        return view;
    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
//        titleBar.showHomeButton();
        if (isVisaTypes) {
            titleBar.setSubHeading(getResources().getString(R.string.visa_types));
            titleBar.setRightBtnHeading(getResources().getString(R.string.add));
            titleBar.setRightBtnListener(this);

        } else
            titleBar.setSubHeading(getResources().getString(R.string.visa_applications));

    }

    private void initAdapter() {
        adapter = new VisaTypesAdapter(getDockActivity(), isVisaTypes, isMember);
        rvVisaTypes.setLayoutManager(new LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false));
      /*  List<String> list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");
        adapter.addAll(list);*/
        rvVisaTypes.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public boolean isVisaTypes() {
        return isVisaTypes;
    }

    public void setVisaTypes(boolean visaTypes) {
        isVisaTypes = visaTypes;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member, String userid) {
        isMember = member;
        this.userid = userid;

    }

    private void callService() {
        if (isVisaTypes)
            serviceHelper.enqueueCall(webService.visatypelist(1),
                    WebServiceConstants.visatypelist, true);

        else if (isMember) { /* VIsa application member*/
            serviceHelper.enqueueCall(webService.getmembers(userid),
                    WebServiceConstants.getmembers, true);
        } else
            serviceHelper.enqueueCall(webService.visaapplicationlist(1),
                    WebServiceConstants.visaapplicationlist, true);
    }

    @Override
    public void ResponseSuccess(Object result, String Tag, String message) {
        switch (Tag) {
            case WebServiceConstants.visatypelist:
                List<VisaTypesEnt> mList = (List<VisaTypesEnt>) result;
                adapter.addAll(mList);
                break;
            case WebServiceConstants.visaapplicationlist:
                List<VisaApplicationEnt> mTypeList = (List<VisaApplicationEnt>) result;
                if (mTypeList != null)
                    adapter.addAll(mTypeList);
                break;
            case WebServiceConstants.getmembers:
                List<VisaApplicationEnt> mMemberList = (List<VisaApplicationEnt>) result;
                if (mMemberList != null)
                    adapter.addAll(mMemberList);
                break;
        }


    }

    /*Title bar right button click listener*/
    @Override
    public void onClick(View v) {
        VisaTypeForm fragment = new VisaTypeForm();
//        fragment.onAddVisa(true);
        getDockActivity().replaceDockableFragment(fragment, VisaTypeForm.class.getSimpleName());
    }
}
