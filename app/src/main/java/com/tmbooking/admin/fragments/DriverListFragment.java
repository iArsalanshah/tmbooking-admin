package com.tmbooking.admin.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Function;
import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.DriverEnt;
import com.tmbooking.admin.expandable_booking_recyclerview.Parent;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.interfaces.OnViewHolderClick;
import com.tmbooking.admin.ui.adapters.DriversListAdapter;
import com.tmbooking.admin.ui.adapters.abstracts.FilterableRecyclerViewListAdapter;
import com.tmbooking.admin.ui.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
/*admin*/

public class DriverListFragment extends BaseFragment implements View.OnClickListener, OnViewHolderClick, SearchView.OnQueryTextListener {


    @BindView(R.id.rvDriverList)
    RecyclerView rvDriverList;
    Unbinder unbinder;
    FilterableRecyclerViewListAdapter<DriverEnt> adapter;
    @BindView(R.id.search)
    SearchView search;

    public DriverListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_driver_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        initAdapter();
        callService();
        return view;
    }

    private void callService() {
        serviceHelper.enqueueCall(webService.driverlist("0"),
                WebServiceConstants.driverlist, true);

        List<String> expandableGroupList = new ArrayList<>();

        for (int i = expandableGroupList.size()-1; i >=0; i--) {
            Log.d("sd", expandableGroupList.get(i) + "");
        }

    }


    private void initFilter() {
       /* search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
//        titleBar.showHomeButton();
        titleBar.setSubHeading(getResources().getString(R.string.driver_list));
        titleBar.setRightBtnHeading(getResources().getString(R.string.add));
        titleBar.setRightBtnListener(this);
    }

    private void initAdapter() {
        search.setOnQueryTextListener(this);

        adapter = new DriversListAdapter(getDockActivity(), DriverListFragment.this, getFunction());
        rvDriverList.setLayoutManager(new LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false));
        rvDriverList.setAdapter(adapter);
    }

    private Function<DriverEnt, String> getFunction() {
        return new Function<DriverEnt, String>() {
            @Override
            public String apply(DriverEnt input) {
                if (input == null || input.getName() == null || input.getDriverid() == null)
                    return "";
                else
                    return input.getName() + "-" + input.getDriverid();
            }
        };
    }

    private Function<Parent, String> getFunction2() {
        return new Function<Parent, String>() {
            @Override
            public String apply(Parent input) {
                for (int i = 0; i < input.getItems().size(); i++) {
                    if (input.getItems().get(i) == null || input.getItems().get(i).getType() == null) {
                        return "";
                    } else
                        return input.getItems().get(i).getType();
                }
                return "";
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void ResponseSuccess(Object result, String Tag, String message) {
        switch (Tag) {
            case WebServiceConstants.driverlist:
                List<DriverEnt> mDriverList = (List<DriverEnt>) result;
                if (mDriverList != null && mDriverList.size() > 0) {
                    adapter.addAll(mDriverList);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {/*Title bar right button*/
        if (checkLoading()) {
            DriverDetailFormFragment fragment = new DriverDetailFormFragment();
            fragment.setIsRegistration(true);
            getDockActivity().replaceDockableFragment(fragment);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        /*no use of adapter click view*/
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            adapter.getFilter().filter("");
        } else {
            adapter.getFilter().filter(newText);
        }
        return true;
    }
}
