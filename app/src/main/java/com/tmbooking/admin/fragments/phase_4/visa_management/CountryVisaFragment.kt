package com.tmbooking.admin.fragments.phase_4.visa_management


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.abstraction.OnSwipeListener
import com.tmbooking.admin.entities.phase4.CountryListEnt
import com.tmbooking.admin.entities.phase4.CountryVisaTypeEnt
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.adapters.CountryVisaListAdapter
import com.tmbooking.admin.ui.views.TitleBar

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CountryVisaFragment : OnSwipeListener() {
    private var mPos: Int = -1

    override fun onSwipeDelete(position: Int) {
        mPos = position
        serviceHelper.enqueueCall(webService.removecountryvisatype(visaTypeList.get(position).id),
                WebServiceConstants.removecountryvisatype, true)
//        callservice
    }


    lateinit var tvRecord: TextView
    lateinit var rvCountryVisa: RecyclerView
    lateinit var adapter: CountryVisaListAdapter
    lateinit var countryObj: CountryListEnt
    lateinit var visaTypeList: List<CountryVisaTypeEnt>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_country_visa, container, false);
        tvRecord = view.findViewById(R.id.tvRecord)
        rvCountryVisa = view.findViewById(R.id.rvCountryVisa)
        initAdapter()
        setData()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.visa_management))

        titleBar.setAddButton {
            if (checkLoading()) {
                //Add visa type visa type
                val fragment = VisaTypeForm()
                fragment.mCountryId = countryObj.id
                dockActivity.replaceDockableFragment(fragment)

            }
        }
        titleBar.showAddButton()

    }

    override fun onResume() {
        super.onResume()
        Log.d("CountryVisaFragment", "OnResume")
        if (countryObj != null)
            callService()
    }

    private fun callService() {
        serviceHelper.enqueueCall(webService.countrylist(),
                WebServiceConstants.countrylist, true)
    }


    private fun setData() {
        if (countryObj.countryvisatypeinfo == null) return
        visaTypeList = countryObj.countryvisatypeinfo
        adapter.addAll(visaTypeList)
        if (visaTypeList.size > 0) {
            tvRecord.visibility = View.GONE
        } else
            tvRecord.visibility = View.VISIBLE
    }


    private fun initAdapter() {
        adapter = CountryVisaListAdapter(dockActivity, this)
        rvCountryVisa.layoutManager = LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false)
        rvCountryVisa.adapter = adapter

    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        when (Tag) {
            WebServiceConstants.removecountryvisatype -> {
                adapter.removeItem(mPos)
                UIHelper.showShortToastInCenter(dockActivity, message)
            }
            WebServiceConstants.countrylist -> {
                var newList = result as List<CountryListEnt>
                for (i in 0 until newList.size) {
                    if (countryObj.id == newList[i].id) {
                        countryObj = newList[i]
                        setData()
                        break
                    }
                }
            }
        }

    }


}

