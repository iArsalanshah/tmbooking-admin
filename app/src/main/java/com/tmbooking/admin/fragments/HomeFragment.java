package com.tmbooking.admin.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tmbooking.admin.R;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.fragments.phase_4.BookingsCalendarFragment;
import com.tmbooking.admin.fragments.phase_4.visa_application.VisaApplicationTabsFragment;
import com.tmbooking.admin.fragments.phase_4.visa_management.CountryListFragment;
import com.tmbooking.admin.ui.views.TitleBar;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

//import com.google.android.gms.location.places.Place;


public class HomeFragment extends BaseFragment {

    Unbinder unbinder;
    final String TAG = HomeFragment.class.getSimpleName();

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "onCreateView: refreshToken: " + refreshedToken);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showMenuButton();
        titleBar.showNotificationButton();
        titleBar.setSubHeading(getResources().getString(R.string.home));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tvCell1, R.id.tvCell2, R.id.tvCell3,
            R.id.tvCell4, R.id.tvCell5, R.id.tvCell6})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCell1:
                getDockActivity().replaceDockableFragment(new VisaApplicationTabsFragment());
                break;

            case R.id.tvCell2:
                getDockActivity().replaceDockableFragment(new CountryListFragment()); //visa management

                break;

            case R.id.tvCell3:
                getDockActivity().replaceDockableFragment(new BookingFragment()); //RIdes booking
                break;

            case R.id.tvCell4:
                getDockActivity().replaceDockableFragment(new BookingsCalendarFragment());
                break;
            case R.id.tvCell5:
                getDockActivity().replaceDockableFragment(new DriverListFragment());
                break;

            case R.id.tvCell6:
//                BookingListFragment fragment1 = new DriverMapFragment();
//                fragment1.whichFragment(AppConstants.ACTIVE_TEXT);
                getDockActivity().replaceDockableFragment(new DriverMapFragment());
                break;
           /* case R.id.tvCell1:
                VisaTypesFragment fragment = new VisaTypesFragment();
                fragment.setVisaTypes(true);
                getDockActivity().replaceDockableFragment(fragment);
                break;

            case R.id.tvCell2:
                VisaTypesFragment fragment2 = new VisaTypesFragment();
                fragment2.setVisaTypes(false);
                getDockActivity().replaceDockableFragment(fragment2);
                break;

            case R.id.tvCell3:
                getDockActivity().replaceDockableFragment(new DriverMapFragment());
                break;

            case R.id.tvCell4:
                getDockActivity().replaceDockableFragment(new DriverListFragment());
                break;

            case R.id.tvCell5:
                getDockActivity().replaceDockableFragment(new BookingFragment());
                break;

            case R.id.tvCell6:
                BookingListFragment fragment1 = new BookingListFragment();
                fragment1.whichFragment(AppConstants.ACTIVE_TEXT);
                getDockActivity().replaceDockableFragment(fragment1);
                break;*/
        }
    }
}

