package com.tmbooking.admin.fragments.phase_4.visa_management


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.abstraction.OnSwipeListener
import com.tmbooking.admin.entities.phase4.CountryListEnt
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.adapters.CountryListAdapter
import com.tmbooking.admin.ui.views.TitleBar

// TODO: Rename parameter arguments, choose names that match
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CountryListFragment : OnSwipeListener() {

    override fun onSwipeDelete(position: Int) {
        mPos = position

        serviceHelper.enqueueCall(webService.removecountry(mGroupList.get(position).id),
                WebServiceConstants.removecountry, true)
//        callservice
    }

    private var mPos: Int = -1
    lateinit var tvRecord: TextView
    lateinit var rvcountryList: RecyclerView
    private lateinit var mGroupList: List<CountryListEnt>

    lateinit var adapter: CountryListAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_country_list, container, false);
        tvRecord = view.findViewById(R.id.tvRecord)
        rvcountryList = view.findViewById(R.id.rvcountryList)
        initAdapter()
        callService()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.visa_management))

        titleBar.setAddButton {
            if (checkLoading()) {
                //Add COUntry
                var fragment = CountryFormFragment()
                dockActivity.replaceDockableFragment(fragment)
            }
        }
        titleBar.showAddButton()

    }

    private fun callService() {
        serviceHelper.enqueueCall(webService.countrylist(),
                WebServiceConstants.countrylist, true)
    }

    private fun initAdapter() {
        adapter = CountryListAdapter(dockActivity, this)
        rvcountryList.layoutManager = LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false)
        rvcountryList.adapter = adapter

    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        when (Tag) {
            WebServiceConstants.countrylist -> {
                mGroupList = result as List<CountryListEnt>
                adapter.addAll(mGroupList)
                if (mGroupList.size > 0) {
                    tvRecord.visibility = View.GONE
                } else
                    tvRecord.visibility = View.VISIBLE
            }
            WebServiceConstants.removecountry -> {
                adapter.removeItem(mPos)
                UIHelper.showShortToastInCenter(dockActivity, message)

            }

        }

    }


}
