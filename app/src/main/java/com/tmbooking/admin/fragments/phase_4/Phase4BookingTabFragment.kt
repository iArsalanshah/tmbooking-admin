package com.tmbooking.admin.fragments.phase_4


import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.MakkahMadina
import com.tmbooking.admin.entities.phase4.ManageBooking
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.ui.views.TitleBar
import java.util.ArrayList
import android.widget.RelativeLayout



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Phase4BookingTabFragment : BaseFragment() {

    lateinit var tabLayout: TabLayout
    lateinit var vpBookings: ViewPager
    lateinit var manageBooking: ManageBooking
    lateinit var makkahList: List<MakkahMadina>
    lateinit var madinaList: List<MakkahMadina>
    private lateinit var adapter: ViewPagerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_phase4_booking_tab, container, false)
        /*init view*/
        tabLayout = view.findViewById(R.id.tabs);
        vpBookings = view.findViewById(R.id.vpBookings);

        /*=========*/

        setupViewPager(vpBookings)
        tabLayout.setupWithViewPager(vpBookings)
        setTabsLayout()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.bookings))
    }


    private fun setTabsLayout() {
        for (i in 0 until tabLayout.tabCount) {
            val tab = tabLayout.getTabAt(i)

            val tabTextView =  LayoutInflater.from(getDockActivity()).inflate(R.layout.custom_tab, null) as TextView
            tabTextView.text = tab!!.text
            tabTextView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            tab.customView = tabTextView
            tab.select()
        }

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        for (i in 0 until tabLayout.getTabCount()) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(0, 0, 20, 0)
            tab.requestLayout()
        }
    }


    private fun setupViewPager(viewpager: ViewPager) {
        adapter = ViewPagerAdapter(childFragmentManager)
        if (prefHelper.loginData.user_role != AppConstants.MANDOOB_MADINA) {
            val fragment0 = Phase4BookingListFragment()
            fragment0.mListResponse = makkahList
            adapter.addFragment(fragment0, resources.getString(R.string.makkah))
        }
        if (prefHelper.loginData.user_role != AppConstants.MANDOOB_MAKKAH) {
            val fragment0 = Phase4BookingListFragment()
            fragment0.mListResponse = madinaList
            adapter.addFragment(fragment0, resources.getString(R.string.madina))
        }
        viewpager.adapter = adapter
    }


    internal inner class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment? {
            return mFragmentList.get(position)
            /* when (position) {
                 0 -> {
                     val fragment0 = Phase4BookingListFragment()
                     fragment0.mListResponse = makkahList
                     return fragment0 // Fragment # 0 - This will show FirstFragment //bookinglistfragment replace after it..
                 }
                 1 -> {
                     val fragment1 = Phase4BookingListFragment()
 //                    fragment1.whichFragment(AppConstants.MADINA)
                     fragment1.mListResponse = madinaList
                     return fragment1
                 }
 //                -> return BookingFragmentttt.newInstance(false, resources.getString(R.string.upcoming_rides))
             }

            return null*/
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }


}

