package com.tmbooking.admin.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.tmbooking.admin.R;
import com.tmbooking.admin.entities.phase4.AuthToken;
import com.tmbooking.admin.entities.phase4.LoginResponse;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.helpers.UIHelper;
import com.tmbooking.admin.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends BaseFragment {

    final static String TAG = LoginFragment.class.getSimpleName();
    @BindView(R.id.etUsername)
    TextInputEditText etUsername;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    Unbinder unbinder;
    private String refreshToken = "";

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        refreshToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "onCreateView: refreshToken = " + refreshToken);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnLogin)
    public void onViewClicked() {
        if (isValidate()) {
            checkAuthToken(etUsername.getText().toString(), etPassword.getText().toString());
        }
    }

    @Override
    public void ResponseSuccess(Object result, String Tag, String message) {
        switch (Tag) {
            case WebServiceConstants.login:
//                welcomeHome((UserEnt) result);
                break;
        }
    }

    /*Arsalan Code*/
    //API
    private void checkAuthToken(String username, String pwd) {
        loadingStarted();
        getAuthTokenWithAPI(username, pwd);
    }

    private void getAuthTokenWithAPI(String userName, String pwd) {
        getWebServiceWP().authToken(userName, pwd)
                .enqueue(new Callback<AuthToken>() {
                    @Override
                    public void onResponse(Call<AuthToken> call, Response<AuthToken> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            String auth = response.body().getToken();
                            Log.e(TAG, "onResponse: TOKEN = " + auth);
                            //Set User Login
                            userLogin(auth);
                        } else {
                            loadingFinished();
                            UIHelper.showLongToastInCenter(getDockActivity(), "Invalid username or password");
                            Log.e(TAG, "onFailure: " + response.raw());
                        }
                    }

                    @Override
                    public void onFailure(Call<AuthToken> call, Throwable t) {
                        loadingFinished();
                        Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
                        UIHelper.showLongToastInCenter(getDockActivity(), getString(R.string.error_failure));
                    }
                });
    }

    private void userLogin(String auth) {
        prefHelper.setToken(auth);
        getWebServiceWP().login("Bearer " + auth).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                loadingFinished();
                if (response.isSuccessful() && response.body() != null) {
                    openHomeFragment(response.body());
                } else {
                    Log.e(TAG, "onFailure: " + response.raw());
                    UIHelper.showLongToastInCenter(getDockActivity(), "Invalid username or password");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loadingFinished();
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
                UIHelper.showLongToastInCenter(getDockActivity(), getString(R.string.error_failure));
            }
        });
    }

    private void openHomeFragment(LoginResponse result) {
        prefHelper.setLoginStatus(true);
        prefHelper.setLoginData(new Gson().toJson(result));
        sendTokenToServer();
        getDockActivity().popBackStackTillEntry(0);
        getDockActivity().replaceDockableFragment(HomeFragment.newInstance(), "HomeFragment");
    }

    private void sendTokenToServer() {
        serviceHelper.enqueueCall(webService.addToken(prefHelper.getLoginData().getId(), refreshToken),
                WebServiceConstants.addToken, false);
    }

    private boolean isValidate() {
        return !etUsername.getText().toString().isEmpty() && !etPassword.getText().toString().isEmpty();
    }
}
