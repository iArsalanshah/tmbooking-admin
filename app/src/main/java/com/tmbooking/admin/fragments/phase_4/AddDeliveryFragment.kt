package com.tmbooking.admin.fragments.phase_4


import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tmbooking.admin.R
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.widget.Button
import android.widget.TextView
import com.tmbooking.admin.entities.phase4.AddDelivery
import com.tmbooking.admin.entities.phase4.Hotel
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.DateHelper
import com.tmbooking.admin.helpers.DateTimePicker
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.interfaces.ResultCallback
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar
import kotlinx.android.synthetic.main.fragment_add_delivery.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 *
 */
class AddDeliveryFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvSelectItem -> {
                showDialog("Select Item", mgiftItemsList.toTypedArray(), tvSelectItem)

            }
            R.id.tvSelectDeliveryBoy -> {
                showDialog("Select Delivery Boy", mAssigneeNameList.toTypedArray(), tvSelectDeliveryBoy)

            }
            R.id.tvSelectDate -> {
//                openCalendar(tvSelectDate)
                callDateTimePicker()
            }
            R.id.btnAdd -> {
                callPostService()
//                UIHelper.showShortToastInCenter(dockActivity, "Will be implemented later")
            }
        }

    }

    lateinit var tvSelectItem: TextView
    lateinit var tvSelectDeliveryBoy: TextView
    lateinit var tvSelectDate: TextView
    lateinit var tvRoomNo: AnyEditTextView
    lateinit var tvQuantity: AnyEditTextView
    lateinit var btnAdd: Button
    private var mdatetime: String = ""
    lateinit var hotelObj: Hotel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_add_delivery, container, false)
        tvSelectItem = view.findViewById(R.id.tvSelectItem)
        tvSelectDeliveryBoy = view.findViewById(R.id.tvSelectDeliveryBoy)
        tvSelectDate = view.findViewById(R.id.tvSelectDate)
        tvRoomNo = view.findViewById(R.id.tvRoomNo)
        tvQuantity = view.findViewById(R.id.tvQuantity)
        btnAdd = view.findViewById(R.id.btnAdd)
        callGetService()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.add_delivery)
    }


    private fun setListener() {

        tvSelectItem.setOnClickListener(this)
        tvSelectDeliveryBoy.setOnClickListener(this)
        tvSelectDate.setOnClickListener(this)
        btnAdd.setOnClickListener(this)
    }

    private lateinit var assigneeId: String
    private lateinit var itemName: String

    private fun showDialog(title: String, list: Array<String>, textview: TextView) {
        // setup the alert builder
        val builder = AlertDialog.Builder(dockActivity)
        builder.setTitle(title)
        builder.setItems(list) { dialog, which ->
            when (textview) {
                tvSelectItem -> {
                    TextViewHelper.setText(textview, list[which])
                    itemName = list[which]

                }
                tvSelectDeliveryBoy -> {
                    TextViewHelper.setText(textview, list[which])
                    assigneeId = mAssigneeIdList[which]
                }
            }

        }
        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }

    private fun callDateTimePicker() {
        val dateTimePicker = DateTimePicker()
        dateTimePicker.showDialog(dockActivity, System.currentTimeMillis())
        dateTimePicker.dateResultCallback = ResultCallback { date ->
            mdatetime = DateHelper.getFormattedDate(date, DateHelper.DD_MM_YYYY_HH_SS)
            TextViewHelper.setText(tvSelectDate, mdatetime)
        }
    }

    private fun callGetService() {
        getServiceHelperWP().enqueueCallWP(webServiceWP
                .getDeliveryItems(), WebServiceConstants.GET_DELIVERY_ITEMS, true)
    }

    private fun callPostService() {
        if (itemName.isEmpty() || assigneeId.isEmpty() || mdatetime.isEmpty() || tvRoomNo.text.toString().isEmpty() || tvQuantity.text.toString().isEmpty()) {
            UIHelper.showShortToastInCenter(dockActivity, "Please fill all fields")
        } else
            getServiceHelperWP().enqueueCallWP(webServiceWP
                    .addDeliveries(hotelObj.booking_id, itemName, assigneeId,
                            mdatetime, tvRoomNo.text.toString(), tvQuantity.text.toString()), WebServiceConstants.POST_DELIVERY_ITEMS, true)
    }


    override fun ResponseSuccess(result: Any?, Tag: String?) {
        when (Tag) {
            WebServiceConstants.GET_DELIVERY_ITEMS -> {
                var mObj = result as AddDelivery
                if (mObj != null)
                    setData(mObj)
                setListener()

            }
            WebServiceConstants.POST_DELIVERY_ITEMS -> {
                UIHelper.showShortToastInCenter(dockActivity, "Added Successfully!")
                dockActivity.popFragment()
            }
        }
    }

    private lateinit var mgiftItemsList: List<String>
    private var mAssigneeNameList: ArrayList<String> = ArrayList<String>()
    private var mAssigneeIdList: ArrayList<String> = ArrayList<String>()


    private fun setData(mObj: AddDelivery) {
        if (mObj.gift_items.size != null && mObj.gift_items.size > 0) {
            mgiftItemsList = mObj.gift_items
        }
        if (mObj.assignee.size != null && mObj.assignee.size > 0) {
            for (i in 0 until mObj.assignee.size) {
                mAssigneeNameList.add(mObj.assignee[i].name)
                mAssigneeIdList.add(mObj.assignee[i].user_id)
            }

        }
    }

}
