package com.tmbooking.admin.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.flyco.dialog.widget.ActionSheetDialog

import com.tmbooking.admin.R
import com.tmbooking.admin.entities.VisaApplicationEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.DateHelper
import com.tmbooking.admin.helpers.ImageLoaderHelper
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.helpers.UIHelper
import com.tmbooking.admin.ui.views.AnyEditTextView
import com.tmbooking.admin.ui.views.TitleBar

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class VisaApplicationFormFragment : BaseFragment(), View.OnClickListener {


    lateinit var userPicture: ImageView
    lateinit var imgPassportPhoto: ImageView
    lateinit var etDate: AnyEditTextView
    lateinit var etNationality: AnyEditTextView
    lateinit var etLivingIn: AnyEditTextView
    lateinit var etFullName: AnyEditTextView
    lateinit var etMobileNo: AnyEditTextView
    lateinit var etEmail: AnyEditTextView
    lateinit var etReferralCode: AnyEditTextView
    lateinit var tvStatus: TextView
    lateinit var etReason: AnyEditTextView
    lateinit var btnMemberEditSave: Button
    lateinit var btnEditSave: Button
    lateinit var btnMember: Button

    lateinit var mVisaBookingEnt: VisaApplicationEnt

    lateinit var status: String
    internal var visaConstantList = arrayOf("Pending", "Approved", "Rejected")
    var clickOnce: Boolean = true
    var clickOnceMember: Boolean = true
    private var isMember: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_visa_application_form, container, false)

        status = mVisaBookingEnt.status;
        userPicture = view.findViewById(R.id.userPicture)
        imgPassportPhoto = view.findViewById(R.id.imgPassportPhoto)
        etDate = view.findViewById(R.id.etDate)
        etNationality = view.findViewById(R.id.etNationality)
        etLivingIn = view.findViewById(R.id.etLivingIn)
        etFullName = view.findViewById(R.id.etFullName)
        etMobileNo = view.findViewById(R.id.etMobileNo)
        etEmail = view.findViewById(R.id.etEmail)
        etReferralCode = view.findViewById(R.id.etReferralCode)
        tvStatus = view.findViewById(R.id.tvStatus)
        etReason = view.findViewById(R.id.etReason)
        btnMemberEditSave = view.findViewById(R.id.btnMemberEditSave)
        btnEditSave = view.findViewById(R.id.btnEditSave)
        btnMember = view.findViewById(R.id.btnMember)

        btnMemberEditSave.setOnClickListener(this)
        btnEditSave.setOnClickListener(this)
        btnMember.setOnClickListener(this)
        tvStatus.setOnClickListener(this)

        setData()

        if (isMember) {
            btnMemberEditSave.visibility = View.VISIBLE
            btnEditSave.visibility = View.GONE
            btnMember.visibility = View.GONE
        } else {
            btnMemberEditSave.visibility = View.GONE
            btnEditSave.visibility = View.VISIBLE
            btnMember.visibility = View.VISIBLE
        }

        return view
    }

    fun isMember(): Boolean {
        return isMember
    }

    fun setMember(member: Boolean) {
        isMember = member

    }

    private fun setData() {
        ImageLoaderHelper.loadImage(mVisaBookingEnt.photo, userPicture)
        if (mVisaBookingEnt.passport.size > 0)
            ImageLoaderHelper.loadImage(mVisaBookingEnt.passport.get(0), imgPassportPhoto)

        if (isMember) /*For member*/
            TextViewHelper.setText(etDate,
                    DateHelper.getlocalDate(mVisaBookingEnt.datetime, DateHelper.DATE_FORMAT2, DateHelper.DATE_TIME_FORMAT))
        else
            TextViewHelper.setText(etDate, mVisaBookingEnt.date)

        TextViewHelper.setText(etNationality, mVisaBookingEnt.nationality)
        TextViewHelper.setText(etLivingIn, mVisaBookingEnt.livingin)
        TextViewHelper.setText(etFullName, mVisaBookingEnt.fullname)
        TextViewHelper.setText(etMobileNo, mVisaBookingEnt.mobile)
        TextViewHelper.setText(etEmail, mVisaBookingEnt.email)
        TextViewHelper.setText(etReferralCode, if (mVisaBookingEnt.referalcode == null) "-" else mVisaBookingEnt.referalcode)
        TextViewHelper.setText(etEmail, mVisaBookingEnt.email)

        if (mVisaBookingEnt.status == AppConstants.PENDING.toString())
            TextViewHelper.setText(tvStatus, visaConstantList[0])
        else if (mVisaBookingEnt.status == AppConstants.APPROVED.toString())
            TextViewHelper.setText(tvStatus, visaConstantList[1])
        else if (mVisaBookingEnt.status == AppConstants.REJECTED.toString())
            TextViewHelper.setText(tvStatus, visaConstantList[2])

        TextViewHelper.setText(etReason, mVisaBookingEnt.reason)

    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.setSubHeading(resources.getString(R.string.visa_type))
    }

    fun setObjectData(visaBookingEnt: VisaApplicationEnt) {
        mVisaBookingEnt = visaBookingEnt
    }


    override fun onClick(v: View?) {
        when (v!!.getId()) {
            R.id.btnMemberEditSave -> {
                if (clickOnceMember) {
                    clickOnceMember = false
                    tvStatus.isEnabled = true
                    etReason.isEnabled = true
                    btnMemberEditSave.setText(resources.getString(R.string.save))
                } else {
                    /*hit service*/
                    serviceHelper.enqueueCall(webService.updatemembervisastatus(status,
                            etReason.text.toString(),
                            mVisaBookingEnt.getId()), WebServiceConstants.updatemembervisastatus, true)
                }

            }
            R.id.btnEditSave -> {
                if (clickOnce) {
                    clickOnce = false
                    tvStatus.isEnabled = true
                    etReason.isEnabled = true
                    btnEditSave.setText(resources.getString(R.string.save))
                } else {
                    /*hit service*/
                    serviceHelper.enqueueCall(webService.updateparentvisastatus(status,
                            etReason.text.toString(),
                            mVisaBookingEnt.getId()), WebServiceConstants.updateparentvisastatus, true)
                }
            }
            R.id.btnMember -> {
                val fragment = VisaTypesFragment()
                fragment.setMember(true, mVisaBookingEnt.userid)
                dockActivity.replaceDockableFragment(fragment, VisaTypesFragment::class.java.simpleName)
            }
            R.id.tvStatus -> {
                openFlycoDialog()
            }
        }
    }


    private fun openFlycoDialog() {
        val dialog = ActionSheetDialog(dockActivity, visaConstantList, null)
        dialog.title(resources.getString(R.string.status))
                .titleTextSize_SP(14.5f)
                .cancelText(resources.getString(android.R.string.cancel))
                .show()
        dialog.setOnOperItemClickL { parent, view, position, id ->
            status = position.toString()
            TextViewHelper.setText(tvStatus, visaConstantList[position])
            dialog.dismiss()
        }
    }


    override fun ResponseSuccess(result: Any, Tag: String, message: String) {
        when (Tag) {
            WebServiceConstants.updateparentvisastatus -> {
                UIHelper.showShortToastInCenter(dockActivity, "Updated successfully")
                dockActivity.popFragment()
            }
            WebServiceConstants.updatemembervisastatus -> {
                UIHelper.showShortToastInCenter(dockActivity, "Member Updated successfully")
                dockActivity.popFragment()
            }

        }
    }

}
