package com.tmbooking.admin.fragments.phase_4

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener

import com.tmbooking.admin.R
import com.tmbooking.admin.annotation.RestAPI
import com.tmbooking.admin.entities.phase4.All
import com.tmbooking.admin.entities.phase4.BookingDates
import com.tmbooking.admin.entities.phase4.ManageBooking
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.ui.views.TitleBar
import org.threeten.bp.LocalDate
import java.lang.StringBuilder
import com.tmbooking.admin.helpers.OneDayDecorator
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class BookingsCalendarFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        callService()
    }


    private fun getDates(): String {
        var mSelectedDates = calendarView.selectedDates

        var mDates = StringBuilder()
        var prefix = ""
        for (i in 0 until mSelectedDates.size) {
            mDates.append(prefix)
            prefix = ","


            var day = String.format("%02d", mSelectedDates[i].day)
            var month = String.format("%02d", mSelectedDates[i].month)
            mDates.append(day
                    + "-" + month
                    + "-" + mSelectedDates[i].year.toString())
        }
        Log.d(TAG, mDates.toString())
        return mDates.toString()
    }

    lateinit var btnSubmit: Button
    lateinit var calendarView: MaterialCalendarView
    var TAG: String = "BookingsCalendarFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_bookings_calendar, container, false)
        btnSubmit = view.findViewById(R.id.btnSubmit)
        calendarView = view.findViewById(R.id.calendarView)
        calendarView.selectionMode = MaterialCalendarView.SELECTION_MODE_RANGE

        calendarView.setOnMonthChangedListener { materialCalendarView, calendarDay ->
            callServiceForDates(calendarView.currentDate.month.toString(), calendarView.currentDate.year.toString())
        }
        callServiceForDates(calendarView.currentDate.month.toString(), calendarView.currentDate.year.toString())

        return view
    }


    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.bookings)
    }

    @RestAPI
    private fun callService() {
        getServiceHelperWP().enqueueCallWP(webServiceWP
                .list_bookings(getDates(), prefHelper.loginData.id/*1*/), WebServiceConstants.LIST_BOOKINGS, true)
    }

    @RestAPI
    private fun callServiceForDates(currentMonth: String, currentYear: String) {
        getServiceHelperWP().enqueueCallWP(webServiceWP
                .list_bookings_count(currentMonth, currentYear, prefHelper.loginData.id/*1*/),
                WebServiceConstants.LIST_BOOKINGS_COUNT, true)
    }


    override fun ResponseSuccess(result: Any?, Tag: String?) {
        when (Tag) {
            WebServiceConstants.LIST_BOOKINGS -> {
                if (result != null) {
                    var mBooking = result as ManageBooking
                    var fragment = Phase4BookingTabFragment()
                    if (mBooking.madina != null)
                        fragment.madinaList = mBooking.madina
                    if (mBooking.makkah != null)
                        fragment.makkahList = mBooking.makkah
                    dockActivity.replaceDockableFragment(fragment)
                }
            }
            WebServiceConstants.LIST_BOOKINGS_COUNT -> {
                if (result != null) {

                    var mBookingDates = result as BookingDates

                    showEvents(mBookingDates.all)
                    btnSubmit.setOnClickListener(this)

                }
            }
        }
    }

    private fun showEvents(all: List<All>) {
        val arrayOfColorDotsToDisplay = intArrayOf(
                Color.RED)

        for (i in 0 until all.size) {
            if (all[i].count != 0) {
                val st = StringTokenizer(all.get(i).selectedData, "-")
                val day = st.nextToken().toInt()
                val month = st.nextToken().toInt()
                val year = st.nextToken().toInt()

                var localDate = LocalDate.of(year, month, day)

                calendarView.addDecorator(OneDayDecorator(CalendarDay.from(localDate), arrayOfColorDotsToDisplay, all[i].count))
            }
        }

    }


}

