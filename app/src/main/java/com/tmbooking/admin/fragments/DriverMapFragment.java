package com.tmbooking.admin.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appolica.interactiveinfowindow.InfoWindow;
import com.appolica.interactiveinfowindow.InfoWindowManager;
import com.appolica.interactiveinfowindow.fragment.MapInfoWindowFragment;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.tmbooking.admin.R;
import com.tmbooking.admin.annotation.RestAPI;
import com.tmbooking.admin.entities.DriverEnt;
import com.tmbooking.admin.fragments.abstracts.BaseFragment;
import com.tmbooking.admin.global.AppConstants;
import com.tmbooking.admin.global.WebServiceConstants;
import com.tmbooking.admin.helpers.GPSHelper;
import com.tmbooking.admin.helpers.TextViewHelper;
import com.tmbooking.admin.helpers.UIHelper;
import com.tmbooking.admin.map.abstracts.FormFragment;
import com.tmbooking.admin.ui.views.TitleBar;
import com.tmbooking.admin.ui.views.Util;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
/*admin*/

@RuntimePermissions
public class DriverMapFragment extends BaseFragment implements /*OnMapReadyCallback, */
        GoogleMap.OnMarkerClickListener, InfoWindowManager.WindowShowListener {
    View rootView;
    Unbinder unbinder;
    @BindView(R.id.search)
    TextView search;
    private boolean isMapLoaded;
    private FusedLocationProviderClient mFusedLocationClient;
    private LatLng currentLatLng;
    private GoogleMap googleMap;
    private final static long DELAY_FOR_LATE_GPS_RESPONSE = 3000;

    InfoWindowManager infoWindowManager;
    DriverEnt singleDriver;
//    InfoWindow formWindow;

//    MapInfoWindowFragment mapInfoWindowFragment;

    public DriverMapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_driver_map, container, false);
//        if (googleMap == null)
//            initMap();
        /*else{
            final InfoWindowManager.ContainerSpecification containerSpecification
                    = new InfoWindowManager.ContainerSpecification(getResources().getDrawable(R.drawable.solid_white_form_rounded));

            infoWindowManager.setContainerSpec(containerSpecification);
        }*/
//        callService();

        // Inflate the layout for this fragment
       /* if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_driver_map, container, false);
        } catch (InflateException e) {
            *//* map is already there, just return view as it is *//*
        }*/
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        if (Util.isGooglePlayServicesAvailable(getDockActivity())) {
            initMap();
            if (singleDriver == null)
                callService();
           /* else
                setMarkers(Collections.singletonList(singleDriver));
*/
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.drivers_location)); // this will set only when fragment appear for history

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    int PLACE_PICKER_REQUEST = 1;

    @OnClick(R.id.search)
    public void onViewClicked() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getDockActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getDockActivity());
//                String toastMsg = String.format("Place: %s", place.getName());
                TextViewHelper.setText(search, (String) place.getName());
            }
        }
    }

    private void initMap() {
//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.googlemap);
//        mapFragment.getMapAsync(DriverMapFragment.this);
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getDockActivity());

        /*Info map sample*/
        final MapInfoWindowFragment
                mapInfoWindowFragment =
                (MapInfoWindowFragment) getChildFragmentManager().findFragmentById(R.id.googlemap);

        infoWindowManager = mapInfoWindowFragment.infoWindowManager();
        infoWindowManager.setHideOnFling(true);

        mapInfoWindowFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap _googleMap) {
                googleMap = _googleMap;

               /* final Marker marker2 = googleMap.addMarker(new MarkerOptions().position(new LatLng(1, 1)).snippet("FORM_VIEW"));

                final int offsetX = (int) getResources().getDimension(R.dimen.marker_offset_x);
                final int offsetY = (int) getResources().getDimension(R.dimen.marker_offset_y);

                final InfoWindow.MarkerSpecification markerSpec =
                        new InfoWindow.MarkerSpecification(offsetX, offsetY);


                formWindow = new InfoWindow(marker2, markerSpec, new FormFragment());*/

                _googleMap.setOnMarkerClickListener(DriverMapFragment.this);
                if (singleDriver != null)
                    setMarkers(Collections.singletonList(singleDriver));
            }
        });

        infoWindowManager.setWindowShowListener(DriverMapFragment.this);

        final InfoWindowManager.ContainerSpecification containerSpecification
                = new InfoWindowManager.ContainerSpecification(getResources().getDrawable(R.drawable.solid_white_form_rounded));

        infoWindowManager.setContainerSpec(containerSpecification);


    }

    @SuppressWarnings("MissingPermission")
    private void getLocation() {
        if (!isMapLoaded)
            if (mFusedLocationClient != null) {
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(getDockActivity(), new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    currentLatLng = new LatLng(location.getLatitude(),
                                            location.getLongitude());
                                } else {
                                    UIHelper.showShortToastInCenter(getDockActivity(), "Location not found");
                                }
                            }
                        });
            } else {
//                UIHelper.showShortToastInCenter(getDockActivity(), getString(R.string.error_failure));
            }
    }

    private void initLocationsWithBroadCast() {
        Intent intent = new Intent();
        intent.setAction(AppConstants.SEND_LOCATION_BROADCAST);
        getDockActivity().sendBroadcast(intent);
    }

    //For location
    @Override
    public void onStart() {
        super.onStart();
        DriverMapFragmentPermissionsDispatcher
                .showLocationWithPermissionCheck(DriverMapFragment.this);
    }

    /**
     * Permission Dispatcher work
     */
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    void showLocation() {
        if (GPSHelper.isGPSEnabled(getDockActivity())) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    getLocation();
                    initLocationsWithBroadCast();
                }
            }, DELAY_FOR_LATE_GPS_RESPONSE);
        } else
            GPSHelper.showGPSDisabledAlertToUser(getDockActivity(), "GPS is disabled in your device. " +
                    "Please enable to see direction");
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(getDockActivity())
                .setMessage(R.string.permission_location_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            request.proceed();
                        } catch (IllegalStateException ex) {
                        }
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            request.cancel();
                        } catch (Exception ex) {
                        }
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_denied));
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void showNeverAskForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_neverask));
    }


    private void showSettingsSnackBar(String msg) {
        Snackbar snackbar = Snackbar.make(rootView, msg, Snackbar.LENGTH_LONG);
        snackbar.setAction("Settings", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInstalledAppDetailsActivity(getDockActivity());
            }
        });
        snackbar.show();
    }

    private void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    /*
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getDockActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getDockActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);


    }*/


    @RestAPI
    private void callService() {
        serviceHelper.enqueueCall(webService.driverlist("0"),
                WebServiceConstants.driverlist, true);
    }

    private void setMarkers(List<DriverEnt> mDriverList) {
        if (googleMap != null) {
            googleMap.clear();
            for (int i = 0; i < mDriverList.size(); i++) {

                final Marker marker2 = googleMap.addMarker(
                        new MarkerOptions().position(new LatLng(Util.getParsedDouble(mDriverList.get(i).getLatitude())
                                , Util.getParsedDouble(mDriverList.get(i).getLongitude())))
                                .snippet("FORM_VIEW")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pointeradmin)));

                final int offsetX = (int) getResources().getDimension(R.dimen.marker_offset_x);
                final int offsetY = (int) getResources().getDimension(R.dimen.marker_offset_y);

                final InfoWindow.MarkerSpecification markerSpec =
                        new InfoWindow.MarkerSpecification(offsetX, offsetY);


                FormFragment formFragment = new FormFragment();
                formFragment.setDriverEnt(mDriverList.get(i));

                InfoWindow formWindow = new InfoWindow(marker2, markerSpec, formFragment);
                marker2.setTag(formWindow);
            }
            if (singleDriver != null)
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Util.getParsedDouble(mDriverList.get(0).getLatitude())
                        , Util.getParsedDouble(mDriverList.get(0).getLongitude())), 13));

        }

    }

    public void ResponseSuccess(Object result, String Tag, String message) {
        switch (Tag) {
            case WebServiceConstants.driverlist:
                List<DriverEnt> mDriverList = (List<DriverEnt>) result;
                if (mDriverList != null && mDriverList.size() > 0) {
                    setMarkers(mDriverList);
                }
                break;
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
      /*  if (formWindow != null) {
            DriverEnt driverObj = (DriverEnt) marker.getTag();

            infoWindowManager.toggle(formWindow, true);
        }*/
        if (googleMap != null && marker != null) {
            InfoWindow _formWindow = (InfoWindow) marker.getTag();

            infoWindowManager.toggle(_formWindow, true);
        }

        return true;
    }

    @Override
    public void onWindowShowStarted(@NonNull InfoWindow infoWindow) {

    }

    @Override
    public void onWindowShown(@NonNull InfoWindow infoWindow) {

    }

    @Override
    public void onWindowHideStarted(@NonNull InfoWindow infoWindow) {

    }

    @Override
    public void onWindowHidden(@NonNull InfoWindow infoWindow) {

    }


    public void setOnlyDriverMarker(DriverEnt singleDriver) {
        this.singleDriver = singleDriver;
    }
}
