package com.tmbooking.admin.fragments.phase_4


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.tmbooking.admin.R
import com.tmbooking.admin.entities.phase4.GuestEnt
import com.tmbooking.admin.entities.phase4.Hotel
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.helpers.TextViewHelper
import com.tmbooking.admin.ui.adapters.GuestListAdapter
import com.tmbooking.admin.ui.adapters.abstracts.RecyclerViewListAdapter
import com.tmbooking.admin.ui.views.TitleBar
import java.util.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class GuestListFragment : BaseFragment() {
    lateinit var rvGuestList: RecyclerView
    lateinit var tvAlternateText: TextView
    lateinit var adapter: RecyclerViewListAdapter<GuestEnt>
    lateinit var bookingOrderId: String


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_guest_list, container, false)
        rvGuestList = view.findViewById(R.id.rvGuestList)
        tvAlternateText = view.findViewById(R.id.tvAlternateText)
        init()
        callService()
        return view
    }

    override fun setTitleBar(titleBar: TitleBar) {
        super.setTitleBar(titleBar)
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar.subHeading = resources.getString(R.string.guest_details)
    }

    private fun init() {
        adapter = GuestListAdapter(dockActivity)
        rvGuestList.layoutManager = LinearLayoutManager(dockActivity, LinearLayoutManager.VERTICAL, false)
        rvGuestList.adapter = adapter
/*
        val mList = ArrayList<String>()
        mList.add("")
        mList.add("")
        mList.add("")
        mList.add("")
        adapter.addAll(mList)*/
    }


    private fun callService() {
        getServiceHelperWP().enqueueCallWP(webServiceWP
                .all_guests(bookingOrderId), WebServiceConstants.ALL_GUEST, true)
    }

    override fun ResponseSuccess(result: Any?, Tag: String?) {
        var guestList = result as List<GuestEnt>
        if (guestList.size > 0) {
            tvAlternateText.visibility = View.GONE
            adapter.addAll(guestList)
        } else
            tvAlternateText.visibility = View.VISIBLE
    }


}
