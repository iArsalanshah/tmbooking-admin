package com.tmbooking.admin.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tmbooking.admin.R
import com.tmbooking.admin.entities.BookingEnt
import com.tmbooking.admin.fragments.abstracts.BaseFragment
import com.tmbooking.admin.global.AppConstants
import com.tmbooking.admin.global.WebServiceConstants
import com.tmbooking.admin.interfaces.OnListItemClick
import com.tmbooking.admin.ui.adapters.BookingAdapter
import com.tmbooking.admin.ui.views.TitleBar

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
/*admin*/

class BookingListFragment : BaseFragment() {
    lateinit var tvNoBooking: TextView
    lateinit var rvBooking: RecyclerView
    lateinit var adapter: BookingAdapter

    internal var mFragmentName: String = ""
    internal lateinit var titleBar: TitleBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_booking_list, container, false)
        tvNoBooking = view.findViewById(R.id.tvNoBooking);
        rvBooking = view.findViewById(R.id.rvBooking);
        initAdapter()
        callService()
        return view
    }

    private fun callService() {
        when (mFragmentName) {
            AppConstants.ACTIVE_TEXT ->
                serviceHelper.enqueueCall(webService.activerides(),
                        WebServiceConstants.activerides, true)
            AppConstants.CONFIRMED_TEXT ->
                serviceHelper.enqueueCall(webService.getRidesBooking(6),
                        WebServiceConstants.activerides, true)
            AppConstants.PENDING_TEXT ->
                serviceHelper.enqueueCall(webService.getRidesBooking(0),
                        WebServiceConstants.activerides, true)
            AppConstants.CANCEL_TEXT ->
                serviceHelper.enqueueCall(webService.getRidesBooking(7),
                        WebServiceConstants.activerides, true)
            AppConstants.HISTORY_TEXT ->
                serviceHelper.enqueueCall(webService.getRidesBooking(4),
                        WebServiceConstants.activerides, true)
        }

    }

    fun whichFragment(_mFragmentName: String) {
        mFragmentName = _mFragmentName

    }

    override fun setTitleBar(titleBar: TitleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar)
        this.titleBar = titleBar
        titleBar.hideButtons()
        titleBar.showBackButton()
        titleBar?.setSubHeading(mFragmentName) // this will set only when fragment appear

        //        titleBar.showHomeButton();
    }

    private fun initAdapter() {
        adapter = BookingAdapter(getDockActivity(),
                OnListItemClick { position, obj ->
                    val fragment = BookingDetailsFormFragment()
                    fragment.setObjectData(obj, mFragmentName)
                    dockActivity.addDockableFragment(fragment, BookingDetailsFormFragment()::class.java.canonicalName)
                })
        rvBooking.layoutManager = LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false)
        rvBooking.adapter = adapter
    }

    override fun ResponseSuccess(result: Any?, Tag: String?, message: String?) {
        val mbookingList = result as List<BookingEnt>
        if (mbookingList.size > 0) {
            tvNoBooking.visibility = View.GONE
        } else
            tvNoBooking.visibility = View.VISIBLE

        adapter.addAll(mbookingList)

    }

}
